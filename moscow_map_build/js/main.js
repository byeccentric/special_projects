var body = document.getElementsByTagName('body')[0];
var svg = document.getElementById('moscow-map');
var defaultWidth = 730; // базовая ширина блока свг
var coef = svg.clientWidth / defaultWidth; // Коэфицент сжатия растяжения, мега-важен для расположения пупапов

var data = [
    {
        title: 'ул. Профсоюзная, 148',
        before: 'сквер «Родничок»',
        after: 'апарт-отель «Коньково»',
        reaction: 'выступают против. Митинги время от времени перерастают в потасовки с полицией и охранниками объекта. По информации жителей, строительство начато без проведения публичных слушаний'
    },
    {
        title: '13-й и 14-й мкр. Ясенево',
        before: 'зеленая зона',
        after: '4-этажный торговый центр, вплотную прилегающий к окнам соседних жилых домов',
        reaction: 'публичные слушания были проведены еще в 2012 году, причем на них присутствовали 16 человек из всего 10-тысячного района'
    },
    {
        title: 'ул. Живописная, 21',
        before: 'особо охраняемая природная территория (ООПТ)',
        after: '22-этажный жилой комплекс (ЖК «Серебряный Бор»). Работы идут на фундаменте. По неофициальной информации, в этом ЖК уже продано 70% квартир',
        reaction: 'с осени 2016 года проходят митинги, сбор подписей местных граждан, обращения к органам власти'
    },
    {
        title: 'ул. Паршина',
        before: 'бывший хлебозавод «Серебряный Бор»',
        after: 'пока не известно',
        reaction: 'местные жители утверждают, что на территории хлебозавода, точнее под ним, до сих пор находится бетонный саркофаг с ядерными отходами, появившимися здесь в результате активной деятельности института им. Курчатова в середине прошлого века'
    },
    {
        title: 'Парк «Дубки»',
        before: 'детский сад, представляющий собой образец архитектуры начала XX века',
        after: '22-этажный дом с подземной парковкой',
        reaction: 'строительство начато, инициативная группа требует его остановить'
    },
    {
        title: 'ул. Лодочная',
        before: 'парк',
        after: 'стадион и храм',
        reaction: 'митинги и пикеты против застройки'
    },
    {
        title: 'ул. Таманская, вл. 1',
        before: 'выход на берег Москвы-реки в Серебряном Бору',
        after: 'без слушаний получено разрешение на строительство центра водного спорта с комплексом апартаментов на 50 тыс. кв. м',
    },
    {
        title: 'ул. 11-я Парковая, 46',
        before: 'территория, предназначенная для детского сада',
        after: 'многоэтажный жилой дом. В январе застройщик вырубил на этом месте более 500 деревьев и несколько тысяч кустарников',
        reaction: 'большинство жителей квартала считают, что будущая стройка незаконна, и называют случившееся экологической катастрофой'
    },
    {
        title: 'парк «Кусково»',
        before: 'парк',
        after: 'хорда',
        reaction: 'в парке идет вырубка деревьев под строительство. По словам экологов, она идет с нарушениями законодательства. Многочисленные конфликты активистов и полиции'
    },
    {
        title: 'ООПТ «Салтыковский лесопарк»',
        before: 'лесопарк',
        after: 'жилой микрорайон с высотой застройки 65, 80 и 100 м, что равняется 25–30-этажным домам',
        reaction: 'выступают против застройки, которая может нанести непоправимый вред ООПТ, а также имеющему мировую известность комплексу ледниковых водоемов «Косинское трехозерье». Проектируемая территория застройки составит 164 га'
    },
    {
        title: 'СВАО, между Янтарным пр. и ул. Коминтерна',
        before: 'стадион «Красная стрела»',
        after: 'жилой комплекс высотой 75 м',
    },
];

window.addEventListener('resize', function() {
    coef = svg.clientWidth / defaultWidth;
});

window.onload=function() {
    var mos = document.getElementsByClassName("map__element");
    var modal = document.querySelector('.modal');
    var header = modal.querySelector('.modal__header');
    var now = modal.querySelector('.modal__now .modal__value');
    var future = modal.querySelector('.modal__future .modal__value');
    var react = modal.querySelector('.modal__reaction');
    var reactValue = react.querySelector('.modal__value');
    for (var i=0, element; element = mos[i++];) {
        element.addEventListener('mouseover', function() {
            var num = parseInt(this.getAttribute('id').replace('point', ''));
            header.innerHTML = data[num].title;
            now.innerHTML = data[num].before;
            future.innerHTML = data[num].after;
            if (data[num].hasOwnProperty('reaction')) {
                react.style.display = 'block';
                reactValue.innerHTML = data[num].reaction;
            } else {
                react.style.display = 'none';
            }

            var top = parseInt(this.getAttribute('cy'))*coef - parseInt(this.getAttribute('r'))*coef - parseInt(modal.offsetHeight) - 10;
            if (top < 10) {
                top = parseInt(this.getAttribute('cy'))*coef + parseInt(this.getAttribute('r'))*coef + 10;
            }

            var left = parseInt(this.getAttribute('cx'))*coef - parseInt(modal.offsetWidth)/2 + 10;
            if (left < 0) {
                left = 10;
            } else if (left + modal.offsetWidth > body.clientWidth) {
                left = left - (left + modal.offsetWidth - body.clientWidth) - 10;
            }

            modal.style.top =  top + 'px';
            modal.style.left = left + 'px'
            modal.style.visibility = 'visible';
            this.setAttribute('fill', '#000000');
        });
        element.addEventListener('mouseout', function() {
            modal.style.visibility = 'hidden';
            this.setAttribute('fill', '#F9453B');
        });
    }
};