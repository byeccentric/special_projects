/* jshint camelcase: false */
$(function () {
    var dataDuma = {};

    // список партий
    $.ajax({
        url: 'data/parties.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.parties ){
                dataDuma.parties = data.parties;
                init();
            }
        }
    });

    // результаты выборов
    $.ajax({
        url: 'data/r_ov.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data ){
                dataDuma.results = data.items;
                dataDuma.ballots = data.ballots;
                dataDuma.time = data.publish_date;
                init();
            }
        },
        error: function(){
            dataDuma.ballots = 0;
            init();
        }
    });

    // результаты экзитполов
    $.ajax({
        url: 'data/ep.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.items ){
                dataDuma.exitPoll = data.items;
                dataDuma.timeExit = data.publish_date;
                init();
            }
        }
    });

    function init(){
        if( (dataDuma.ballots >= 0) && dataDuma.parties && dataDuma.exitPoll){
            // пошли данные выборов
            if( dataDuma.ballots > 0){
                renderTime( true );
                viewInfoBlock();
                renderList( true, dataDuma.results );

            // иначе экзитполы выводим
            } else {
                renderTime( false );
                renderList( false, dataDuma.exitPoll );
            }
        }
    }

    function renderTime( isClosed ){
        var date
          , hours
          , title;

        if( isClosed ){
            date = new Date( dataDuma.time );
            title = 'Результаты выборов';
        } else {
            date = new Date( dataDuma.timeExit );
            title = 'Результаты экзитполов';
        }

        hours = date.getUTCHours() + 3;
        if( hours > 23 ){
            hours = hours - 24;
        }

        $('.js-title').html( title );
        $('.js-time').html('на ' + convertTime(hours) + ':' + convertTime(date.getMinutes()) + ' мск');
    }

    function renderList( isClosed, items ){
        var html = '';
        var itesmArray = sorting(items);
        var max = itesmArray[0].result || 100;

        $('.js-limit').css('margin-left', (caclBarWidth(max, 5)/2) + '%');

        for( var i = 0, len = itesmArray.length; i < len; i++ ){
            html = html + '<div class="article__poll__answer-item';

            if( itesmArray[i].result >= 5 ){
                html = html + ' bold';
            }
            html = html + '">';

            html = html + '<div class="article__poll__answer-item__result">';
            html = html + '<div class="article__poll__answer-item__result__inner';

            if( isClosed ){
                html = html + ' big';
            }


            html = html + '">';
            html = html + '<div class="article__poll__answer-item__percent">';
            html = html + itesmArray[i].result.toString().replace('.', ',');
            html = html + '%</div>';

            if( isClosed ){
                html = html + '<div class="article__poll__answer-item__percent_exitpoll">(' + dataDuma.exitPoll[itesmArray[i].id].toString().replace('.', ',') + '%)</div>';
            }

            html = html + '</div>';
            html = html + '<div class="article__poll__answer-item__name">' + dataDuma.parties[itesmArray[i].id] + '</div></div>';



            html = html + '<div class="article__poll__answer-item__bar-wrapper">';

            if( isClosed ){
                html = html + '<div class="article__poll__answer-item__bar-real ' + getColor(itesmArray[i].result) + ' js-pollresult" data-width="' + caclBarWidth(max, itesmArray[i].result) + '"></div>';
            }

            html = html + '<div class="article__poll__answer-item__bar-exitpoll js-exitpoll" data-width="' + caclBarWidth(max, dataDuma.exitPoll[itesmArray[i].id]) + '"></div>';
            html = html + '</div></div>';
        }

        $('.js-content').html( html );

        $('.js-exitpoll').each(function(){
            $(this).animate({width : $(this).data('width') +'%'}, 1000);
        });

        $('.js-pollresult').each(function(){
            $(this).animate({width : $(this).data('width') +'%'}, 1000);
        });
    }

    function caclBarWidth(base, current){
        var percent = (100*current)/base;
        return parseFloat(percent.toFixed(2));
    }

    function getColor( result ){
        var color = '';

        if( result < 3 ){
            color = 'icon11';
        } else if( result < 5 ){
            color = 'icon12';
        } else if( result < 10 ){
            color = 'icon13';
        } else if( result < 20 ){
            color = 'icon14';
        } else if( result < 30 ){
            color = 'icon15';
        } else if( result < 50 ){
            color = 'icon16';
        } else{
            color = 'icon17';
        }

        return color;
    }

    function viewInfoBlock(){
        $('.js-result-info').show();
        $('.js-ballot-bar').animate({width : Math.floor(dataDuma.ballots) + '%'}, 1000);

        $('.js-ballot').html(dataDuma.ballots.toString().replace('.', ',') + '%');


        //$({numberValue: 0}).animate({numberValue: Math.round(dataDuma.ballots)}, {
        //    duration: 1000,
        //    easing: 'swing',
        //    step: function() {
        //        $('.js-ballot').html(Math.ceil(this.numberValue) + '%');
        //    }
        //});

    }

    function sorting( data ){
        var arr = [];

        for(var key in data ){
            arr.push({
                id: key,
                result: data[key]
            });
        }

        arr.sort(function(paramA, paramB){
            return paramB.result - paramA.result;
        });

        return arr;
    }

    function convertTime(number){
        if(number < 10){
            number = '0' + number;
        }

        return number;
    }
});