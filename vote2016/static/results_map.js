/* global Highcharts */
/* global russianRegions */
/* jshint camelcase: false */
$(function () {
    var highChartsSettings = {
        chart: {
            renderTo: 'container'
        },
        credits: {
            enabled: false
        },
        title : {
            text : ''
        },
        legend: {
            enabled: false
            //valueDecimals: 0
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            },
            enableMouseWheelZoom: false
        },
        colorAxis: {
            dataClasses: [{
                to: 5,
                color: '#fedfa6'
            }, {
                from: 5,
                to: 10,
                color: '#efc592'
            }, {
                from: 10,
                to: 15,
                color: '#e3b083'
            }, {
                from: 15,
                to: 20,
                color: '#d89b73'
            }, {
                from: 20,
                to: 30,
                color: '#cc8663'
            }, {
                from: 30,
                to: 40,
                color: '#c17054'
            }, {
                from: 40,
                to: 50,
                color: '#b55b44'
            }, {
                from: 50,
                to: 60,
                color: '#aa4735'
            }, {
                from: 60,
                to: 70,
                color: '#9e3024'
            }, {
                from: 70,
                to: 80,
                color: '#921b15'
            }, {
                from: 80,
                color: '#6a0203'
            }]
        },
        exporting: {
            enabled: false
        },
        tooltip : {
            backgroundColor: '#fff',
            borderWidth: 0,
            useHTML: true,
            formatter: _tooltipFormatter
        },
        series : [{
            data : [],
            mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
            joinBy: 'hc-key',
            name: '',
            borderWidth: 1,
            borderColor: '#fff',
            states: {
                hover: {
                    color: '#FCD78D'
                }
            }
        }]
    };

    Highcharts.setOptions({
        global: {
            timezoneOffset: 3 * -60 // minutes
        },
        lang: {
            contextButtonTitle: 'Контекстное меню',
            decimalPoint: '.',
            downloadJPEG: 'Экспорт в формате JPEG',
            downloadPDF: 'Экспорт в формате PDF',
            downloadPNG: 'Экспорт в формате PNG',
            downloadSVG: 'Экспорт в формате SVG',
            loading: 'Загрузка...',
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            shortMonths: ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'],
            weekdays: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
            numericSymbols: [ 'тыс' , 'млн' , 'млр' , 'трл' , 'P' , 'E'],
            printChart: 'На печать',
            rangeSelectorFrom: 'От',
            rangeSelectorTo: 'До',
            rangeSelectorZoom: 'Масштаб',
            resetZoom: 'Масштаб по-умолчанию',
            resetZoomTitle: 'Масштаб 1:1',
        }
    });

    var dataDuma = {};
    var dataDumaRegion = {};
    var dataRegions = {};
    var currentParty = 0;
    var map;

    var $popuList = $('.js-list')
      , $selectedParty = $('.js-selected-party');

    $.ajax({
        url: 'data/parties.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.parties ){
                dataDuma.parties = data.parties;
                initPopup();
            }
        }
    });

    $.ajax({
        url: 'data/r_ov.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.items ){
                dataDuma.percent = data.items;
                dataDuma.time = data.publish_date;

                initPopup();
            }
        }
    });

    $.ajax({
        url: 'data/to_br.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.items ){
                dataDumaRegion.appearance = data.items;
                initMap();
            }
        }
    });

    $.ajax({
        url: 'data/r_br.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.items ){
                dataDumaRegion.partiesPercent = data.items;
                initMap();
            }
        }
    });

    function initPopup(){
        if( dataDuma.percent && dataDuma.parties ){
            renderList();
            bindEvents();
            initMap();
        }
    }

    function initMap(){
        if( dataDumaRegion.partiesPercent && dataDumaRegion.appearance && dataDuma.parties && dataDuma.percent){
            dataRegions = convertData();

            highChartsSettings.series[0].data = russianRegions;
            map = new Highcharts.Map(highChartsSettings);
        }
    }

    function renderList(){
        var html = '';

        for( var key in dataDuma.parties ){
            if( dataDuma.percent[key] >= 5){
                html = html + '<div class="map__district__party-list__item';
                if( key === '4'){
                    html = html + ' selected';
                    $selectedParty.html(dataDuma.parties[key] + ' ( ' + dataDuma.percent[key].toString().replace('.', ',') + '% )');
                    currentParty = parseInt(key,10);
                }
                html = html + '" data-id="' + key + '"><div class="map__district__party-list__value">';
                html = html + dataDuma.percent[key].toString().replace('.', ',');
                html = html + '%</div><div class="map__district__party-list__title">';
                html = html + dataDuma.parties[key];
                html = html + '</div></div>';
            }
        }

        $('.js-list').html( html );
    }

    function convertData(){
        var obj = {};

        for(var i = 0, len = russianRegions.length; i < len; i++ ){
            obj[russianRegions[i]['hc-key'] ] = $.extend({}, russianRegions[i]) ;
            obj[russianRegions[i]['hc-key'] ].number = dataDumaRegion.appearance[russianRegions[i].nick];
            obj[russianRegions[i]['hc-key'] ].result = dataDumaRegion.partiesPercent[russianRegions[i].nick];

            var partiesList = dataDumaRegion.partiesPercent[russianRegions[i].nick];
            for(var j = 0, lenReg = partiesList.length; j < lenReg; j++){
               if( partiesList[j].partyId === currentParty ){
                    russianRegions[i].value = partiesList[j].partyValue;
               }
            }
        }

        return obj;
    }

    function _tooltipFormatter(){
        var pointData = dataRegions[this.point['hc-key']];
        var pointNumber = '';

        var html = '<div class="map__district__name">' + this.point.name +'</div>';
        html = html + '<div class="map__district__value">Явка: <b>' + pointData.number.toString().replace('.', ',') + '% ' + pointNumber + '</b></div>';

        if( pointData.result && pointData.result.length > 0 ){
            html = html + '<div class="map__district__result">';
            var topFivePresent = false;

            for(var i = 0; i < 4; i++ ){
                if( i < 4 ){
                    if( pointData.result[i].partyId === currentParty ){
                        html = html + renederPartyPopup(i, true, pointData);
                        topFivePresent = true;
                    } else {
                        html = html + renederPartyPopup(i, false, pointData);
                    }
                } else {
                    if( topFivePresent ){
                        if( i < 5 ){
                            html = html + renederPartyPopup(i, false, pointData);
                        }
                    } else if( pointData.result[i].partyId === currentParty ){
                        html = html + renederPartyPopup(i, true, pointData);
                        topFivePresent = true;
                    }
                }

            }
            html = html + '</div>';
        }

        return html;
    }

    function renederPartyPopup(i, bold, pointData){
        var html = '';

        if( bold) {
            html = html + '<div class="map__district__result__item bold">';
        } else {
            html = html + '<div class="map__district__result__item">';
        }

        html = html + '<div class="map__district__result__value">' + pointData.result[i].partyValue.toString().replace('.', ',') +  '%</div>';
        html = html + '<div class="map__district__result__title">' + dataDuma.parties[pointData.result[i].partyId] +  '</div>';
        html = html + '</div>';

        return html;
    }

    function bindEvents(){
        $('.map__district__party').click(function (event) {
            event.stopPropagation();
            $popuList.toggle();
        });

        $('.map__district__party-list__item').click(function ( event ) {
            event.stopPropagation();
            if (!$(this).hasClass('selected')) {
                var title = $(this).find('.map__district__party-list__title').html();
                var value = $(this).find('.map__district__party-list__value').html();

                $selectedParty.html(title + ' ( ' + value + ' )');
                $('.map__district__party-list__item').removeClass('selected');

                $(this).addClass('selected');
                currentParty = parseInt($(this).data('id'), 10);
                redrawMap();
            }

            $popuList.hide();
        });

        $(document).on('click', function () {
            $popuList.hide();
        });
    }

    function redrawMap(){
        map.series[0].update({
            data: convertData()
        });
    }
});