/* global Highcharts */
/* global russianRegions */
/* jshint camelcase: false */
$(function () {
    var highChartsSettings = {
        chart: {
            renderTo: 'container'
        },
        credits: {
            enabled: false
        },
        title : {
            text : ''
        },
        legend: {
            enabled: false
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            },
            enableMouseWheelZoom: false
        },
        colorAxis: {
            dataClasses: [{
                to: 10,
                color: '#daebd4'
            }, {
                from: 10,
                to: 20,
                color: '#b3dbab'
            }, {
                from: 20,
                to: 30,
                color: '#8dc782'
            }, {
                from: 30,
                to: 40,
                color: '#66b559'
            }, {
                from: 40,
                to: 50,
                color: '#40a429'
            }, {
                from: 50,
                to: 60,
                color: '#179200'
            }, {
                from: 60,
                to: 70,
                color: '#007e00'
            }, {
                from: 70,
                to: 80,
                color: '#005900'
            }, {
                from: 80,
                color: '#004700'
            }]
        },
        exporting: {
            enabled: false
        },
        tooltip : {
            backgroundColor: '#fff',
            borderWidth: 0,
            useHTML: true,
            formatter: _tooltipFormatter
        },
        series : [{
            data : russianRegions,
            mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
            joinBy: 'hc-key',
            name: '',
            borderWidth: 1,
            borderColor: '#fff',
            states: {
                hover: {
                    color: '#BADA55'
                }
            }
        }]
    };

    Highcharts.setOptions({
        global: {
            timezoneOffset: 3 * -60 // minutes
        },
        lang: {
            contextButtonTitle: 'Контекстное меню',
            decimalPoint: '.',
            downloadJPEG: 'Экспорт в формате JPEG',
            downloadPDF: 'Экспорт в формате PDF',
            downloadPNG: 'Экспорт в формате PNG',
            downloadSVG: 'Экспорт в формате SVG',
            loading: 'Загрузка...',
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            shortMonths: ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'],
            weekdays: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
            numericSymbols: [ 'тыс' , 'млн' , 'млр' , 'трл' , 'P' , 'E'],
            printChart: 'На печать',
            rangeSelectorFrom: 'От',
            rangeSelectorTo: 'До',
            rangeSelectorZoom: 'Масштаб',
            resetZoom: 'Масштаб по-умолчанию',
            resetZoomTitle: 'Масштаб 1:1',
        }
    });

    var dataObject = {};

    $.ajax({
        url: 'data/to_ov.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.turnout ){
                $('.js-percent').html(data.turnout.toString().replace('.', ',') + '%').css('display', 'inline-block');
                $('.js-percent-parent').css('display', 'block');

                var date = new Date(data.publish_date);
                var hours = date.getUTCHours() + 3;
                if( hours > 23 ){
                    hours = hours - 24;
                }
                $('.js-time').html('на ' + convertTime(hours) + ':' + convertTime(date.getMinutes()) + ' мск');
            }
        }
    });

    $.ajax({
        url: 'data/to_br.json',
        dataType: 'json',
        cache: false,
        success: function( data ) {
            if( data && data.items ){
                dataObject = convertData(data.items);
                highChartsSettings.series[0].data = russianRegions;
                new Highcharts.Map(highChartsSettings);

                $('#container').append('<div class="overlay">Кликните по карте для активации</div>');

                renderList();
            }
        }
    });

    function convertData( items ){
        var obj = {};

        for(var i = 0, len = russianRegions.length; i < len; i++ ){
            obj[russianRegions[i]['hc-key'] ] = $.extend({}, russianRegions[i]);
            obj[russianRegions[i]['hc-key'] ].value = items[russianRegions[i].nick];

            russianRegions[i].value = items[russianRegions[i].nick];
        }

        return obj;
    }

    function _tooltipFormatter(){
        var pointData = dataObject[this.point['hc-key']];
        var pointNumber = '';

        if( pointData.number ){
            pointNumber = '(' + pointData.number.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ')';
        }

        return '<span class="map__district__name">' + this.point.name +'</span><span class="map__district__value">Явка: <b>' + this.point.value.toString().replace('.', ',') + '% ' + pointNumber + '</b></span>';
    }

    function convertTime(number){
        if(number < 10){
            number = '0' + number;
        }

        return number;
    }

    function renderList(){
        var html = '';
        var list = russianRegions;

        list.sort(function(a, b){
            return b.value - a.value;
        });

        html = html + '<div class="poll__block">';
        for(var i = 0; i < 11; i++){
            if (list[i].value != null) {
                html = html + '<div class="poll__item clearfix">';
                html = html + '<div class="poll__item__percent">' + list[i].value.toString().replace('.', ',') + '%</div>';
                html = html + '<div class="poll__item__name">' + findName(list[i]['hc-key']) + '</div>';
                html = html + '</div>';
            }
        }
        html = html + '</div><div class="poll__block loose">';

        for(i = list.length - 11; i < list.length; i++){
            if (list[i].value != null) {
                html = html + '<div class="poll__item clearfix">';
                html = html + '<div class="poll__item__percent">' + list[i].value.toString().replace('.', ',') + '%</div>';
                html = html + '<div class="poll__item__name">' + findName(list[i]['hc-key']) + '</div>';
                html = html + '</div>';
            }
        }
        html = html + '</div>';

        $('.js-content').html(html);
    }

    function findName(nick){
        var arr = Highcharts.maps['countries/ru/custom/ru-all-rbc'].features;
        var title = '';

        for(var i = 0; i < arr.length; i++){
            if( nick === arr[i].id.toLowerCase().replace('.','-')){
                title = arr[i].properties.name;
            }
        }

        return title;
    }
});