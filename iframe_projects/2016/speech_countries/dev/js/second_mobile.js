var colors = {"Семья":"#8ecfae","Религия":"#69bfae","Пожелания":"#005f65","Деньги":"#165374","Борьба":"#3793be","Ценности":"#14447e","Будущее":"#41679e","Страна":"#7c173d","Общество и политика":"#f05157","Проблемы":"#f39e3d","Долг":"#f69ea4"};
var categories = ["Ценности","Борьба","Будущее","Страна","Общество и политика","Долг","Пожелания","Религия","Семья","Проблемы","Деньги"];

var defaultChoise = "russian";
var thisChoise = defaultChoise;
var newData = {};

$(document).ready(function() {
    $.ajax({
        url: 'data/themes.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            var data_i = JSON.parse(items.responseText);
            for(var i=0, item; item=data_i[i++];){
                if(!newData.hasOwnProperty(item.country)) {
                    newData[item.country] = {
                        max: 0,
                        items: []
                    }
                }
                newData[item.country].items.push({
                    name: item.groups,
                    value: Math.round(item['percent of speech']*10000)/100
                });

                if(Math.round(item['percent of speech']*10000)/100 > newData[item.country].max)
                    newData[item.country].max = Math.round(item['percent of speech']*10000)/100;
            }

            var tempData = {};
            _.each(newData, function(item, key) {
                tempData[key] = {};
                tempData[key]['max'] = item.max;
                tempData[key]['items'] = item.items.sort(function(a,b) {
                    return categories.indexOf(a.name) - categories.indexOf(b.name);
                });
            });

            newData = tempData;

            makeBars(thisChoise);
        }
    })
});

function makeBars(choise) {
    var html = $("#bars_items").html();
    html = _.template(html);
    html = html({'data' : newData[choise], 'colors': colors});
    $('.bars').html(html);
}

$('.choosers').on('click', '.choosers__item', function() {
    if(!$(this).hasClass('choosers__item_active')) {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        $('.choosers__value').html('<span class="choosers__value-name">'+$(this).find('.choosers__item-name').html()+'</span><span class="choosers__value-country">'+$(this).find('.choosers__item-country').html()+'</span>');
        $('.chooser__item').removeClass('chooser__item_active');
        thisChoise = $(this).data('link');
        makeBars(thisChoise)
    }
    return false;
});

$('.choosers__value').click(function() {
    if($('.choosers__list').css('display') == "block") {
        $('.choosers').css('border', '1px solid #CDCDCD;');
    } else {
        $('.choosers').css('border', '1px solid #FFFFFF');
    }
    $('.choosers__list').slideToggle();
});

$(document).on('click', function(evt) {
    if(evt.target.className != "choosers__select" && evt.target.className != "choosers__value" && evt.target.className != "choosers__value-name" && evt.target.className != "choosers__value-country" &&evt.target.className != "choosers__item_active" ) {
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
    }
});