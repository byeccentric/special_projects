var allData = {
    "rus": [
        {"name": "зависеть", "count": 6, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0416666667,"count":1},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0476190476,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "семья", "count": 7, "years": {"2009":{"percent":0.0952380952,"count":2},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "дети","count": 9, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.125,"count":3},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.04,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "вместе", "count": 9, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.1052631579,"count":2}}},
        {"name": "близкий", "count": 11, "years": {"2009":{"percent":0.1428571429,"count":3},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.1111111111,"count":2}}},
        {"name": "любовь", "count": 12, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.0416666667,"count":1},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.04,"count":1},"2014":{"percent":0.1052631579,"count":2},"2015":{"percent":0.1111111111,"count":2}}},
        {"name": "благополучие", "count": 15, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.1578947368,"count":3},"2015":{"percent":0.1666666667,"count":3}}},
        {"name": "здоровье", "count": 16, "years": {"2009":{"percent":0.1428571429,"count":3},"2010":{"percent":0.1666666667,"count":4},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.12,"count":3},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "друзья", "count": 21, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.2,"count":5},"2014":{"percent":0.2105263158,"count":4},"2015":{"percent":0.1666666667,"count":3}}},
        {"name": "Россия", "count": 39, "years": {"2009":{"percent":0.1904761905,"count":4},"2010":{"percent":0.375,"count":9},"2011":{"percent":0.3529411765,"count":6},"2012":{"percent":0.1904761905,"count":4},"2013":{"percent":0.28,"count":7},"2014":{"percent":0.2631578947,"count":5},"2015":{"percent":0.2222222222,"count":4}}}
    ],
    "gb1": [
        {"name": "война", "count": 10, "years": {"2014":{"percent":0.2051282051,"count":8},"2015":{"percent":0.0512820513,"count":2}}},
        {"name": "спорт", "count": 11, "years": {"2010":{"percent":0.2666666667,"count":8},"2012":{"percent":0.0769230769,"count":2},"2014":{"percent":0.0256410256,"count":1}}},
        {"name": "общество","count": 12, "years": {"2010":{"percent":0.1,"count":3},"2011":{"percent":0.1818181818,"count":8},"2015":{"percent":0.0256410256,"count":1}}},
        {"name": "надежда", "count": 13, "years": {"2010":{"percent":0.0333333333,"count":1},"2011":{"percent":0.1136363636,"count":5},"2013":{"percent":0.0740740741,"count":2},"2014":{"percent":0.0512820513,"count":2},"2015":{"percent":0.0769230769,"count":3}}},
        {"name": "игра", "count": 13, "years": {"2010":{"percent":0.2,"count":6},"2012":{"percent":0.0384615385,"count":1},"2014":{"percent":0.1538461538,"count":6}}},
        {"name": "любовь", "count": 18, "years": {"2010":{"percent":0.0666666667,"count":2},"2011":{"percent":0.0909090909,"count":4},"2012":{"percent":0.1153846154,"count":3},"2013":{"percent":0.1111111111,"count":3},"2014":{"percent":0.0512820513,"count":2},"2015":{"percent":0.1025641026,"count":4}}},
        {"name": "мир", "count": 20, "years":  {"2010":{"percent":0.0666666667,"count":2},"2011":{"percent":0.1136363636,"count":5},"2013":{"percent":0.1111111111,"count":3},"2014":{"percent":0.1538461538,"count":6},"2015":{"percent":0.1025641026,"count":4}}},
        {"name": "семейный", "count": 24, "years":  {"2010":{"percent":0.0333333333,"count":1},"2011":{"percent":0.25,"count":11},"2012":{"percent":0.1153846154,"count":3},"2013":{"percent":0.1481481482,"count":4},"2015":{"percent":0.1282051282,"count":5}}},
        {"name": "рождество", "count": 35, "years": {"2011":{"percent":0.1136363636,"count":5},"2012":{"percent":0.2307692308,"count":6},"2013":{"percent":0.2222222222,"count":6},"2014":{"percent":0.1794871795,"count":7},"2015":{"percent":0.282051282,"count":11}}},
        {"name": "человечность", "count": 49, "years": {"2010":{"percent":0.2333333333,"count":7},"2011":{"percent":0.1363636364,"count":6},"2012":{"percent":0.4230769231,"count":11},"2013":{"percent":0.3333333333,"count":9},"2014":{"percent":0.1794871795,"count":7},"2015":{"percent":0.2307692308,"count":9}}}
    ],
    "ch": [
        {"name": "победа", "count": 6, "years": {"2014":{"percent":0.0253164557,"count":2},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "друзья", "count": 6, "years": {"2014":{"percent":0.0253164557,"count":2},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "новый","count": 9, "years": {"2014":{"percent":0.0632911392,"count":5},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "жизнь", "count": 14, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.0853658537,"count":7}}},
        {"name": "реформы", "count": 15, "years": {"2014":{"percent":0.1139240506,"count":9},"2015":{"percent":0.0731707317,"count":6}}},
        {"name": "мир", "count": 15, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.0975609756,"count":8}}},
        {"name": "формат", "count": 16, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.1097560976,"count":9}}},
        {"name": "должны", "count": 17, "years": {"2014":{"percent":0.1265822785,"count":10},"2015":{"percent":0.0853658537,"count":7}}},
        {"name": "народ", "count": 29, "years": {"2014":{"percent":0.2151898734,"count":17},"2015":{"percent":0.1463414634,"count":12}}},
        {"name": "Китай", "count": 34, "years": {"2014":{"percent":0.164556962,"count":13},"2015":{"percent":0.256097561,"count":21}}}
    ],
    "ger": [
        {"name": "семейный", "count": 6, "years": {"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.0769230769,"count":3}}},
        {"name": "целостность", "count": 7, "years": {"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.1212121212,"count":4}}},
        {"name": "сплоченность","count": 9, "years":  {"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.0512820513,"count":2},"2014":{"percent":0.0909090909,"count":3},"2015":{"percent":0.03125,"count":1}}},
        {"name": "согражданин", "count": 9, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.0606060606,"count":2},"2015":{"percent":0.0625,"count":2}}},
        {"name": "благодарность", "count": 9, "years": {"2012":{"percent":0.0476190476,"count":1},"2014":{"percent":0.0303030303,"count":1},"2015":{"percent":0.21875,"count":7}}},
        {"name": "любовь", "count": 10, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1025641026,"count":4},"2014":{"percent":0.0606060606,"count":2},"2015":{"percent":0.0625,"count":2}}},
        {"name": "Европа", "count": 11, "years": {"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.1818181818,"count":6},"2015":{"percent":0.0625,"count":2}}},
        {"name": "человеческий", "count": 16, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1025641026,"count":4},"2014":{"percent":0.1818181818,"count":6},"2015":{"percent":0.125,"count":4}}},
        {"name": "жизнь", "count": 27, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1538461538,"count":6},"2014":{"percent":0.1515151515,"count":5},"2015":{"percent":0.125,"count":4}}},
        {"name": "Германия", "count": 31, "years": {"2012":{"percent":0.2857142857,"count":6},"2013":{"percent":0.282051282,"count":11},"2014":{"percent":0.1212121212,"count":4},"2015":{"percent":0.3125,"count":10}}}
    ],
    "vat": [
        {"name": "насилие", "count": 9, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.025974026,"count":2},"2015":{"percent":0.03125,"count":3}}},
        {"name": "страдание", "count": 10, "years" :{"2014":{"percent":0.0649350649,"count":5},"2015":{"percent":0.0520833333,"count":5}}},
        {"name": "надежда","count": 12, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.025974026,"count":2},"2015":{"percent":0.0625,"count":6}}},
        {"name": "братья", "count": 18, "years" :{"2013":{"percent":0.064516129,"count":6},"2014":{"percent":0.1038961039,"count":8},"2015":{"percent":0.0416666667,"count":4}}},
        {"name": "дети", "count": 21, "years" :{"2013":{"percent":0.0752688172,"count":7},"2014":{"percent":0.1168831169,"count":9},"2015":{"percent":0.0520833333,"count":5}}},
        {"name": "любовь", "count": 22, "years" :{"2013":{"percent":0.0967741936,"count":9},"2014":{"percent":0.0909090909,"count":7},"2015":{"percent":0.0625,"count":6}}},
        {"name": "Рождество", "count": 28, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.1168831169,"count":9},"2015":{"percent":0.15625,"count":15}}},
        {"name": "человечность", "count": 42, "years" :{"2013":{"percent":0.1505376344,"count":14},"2014":{"percent":0.1688311688,"count":13},"2015":{"percent":0.15625,"count":15}}},
        {"name": "мир", "count": 43, "years" :{"2013":{"percent":0.2580645161,"count":24},"2014":{"percent":0.0909090909,"count":7},"2015":{"percent":0.125,"count":12}}},
        {"name": "бог", "count": 61, "years" :{"2013":{"percent":0.2258064516,"count":21},"2014":{"percent":0.1948051948,"count":15},"2015":{"percent":0.2604166667,"count":25}}}
    ],
    "fra": [
        {"name": "мир", "count": 9, "years": {"2013":{"percent":0.05,"count":3},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.0425531915,"count":2}}},
        {"name": "доверие", "count": 9, "years": {"2012":{"percent":0.0909090909,"count":3},"2014":{"percent":0.1,"count":6}}},
        {"name": "европа","count": 10, "years": {"2012":{"percent":0.0303030303,"count":1},"2013":{"percent":0.0666666667,"count":4},"2014":{"percent":0.0333333333,"count":2},"2015":{"percent":0.0638297872,"count":3}}},
        {"name": "великий", "count": 13, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0333333333,"count":2},"2014":{"percent":0.0833333333,"count":5},"2015":{"percent":0.085106383,"count":4}}},
        {"name": "социальный", "count": 14, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0833333333,"count":5},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.0638297872,"count":3}}},
        {"name": "соотечественник", "count": 14, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0666666667,"count":4},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.085106383,"count":4}}},
        {"name": "должен", "count": 15, "years": {"2013":{"percent":0.1333333333,"count":8},"2014":{"percent":0.0333333333,"count":2},"2015":{"percent":0.1063829787,"count":5}}},
        {"name": "против", "count": 17, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.05,"count":3},"2014":{"percent":0.1,"count":6},"2015":{"percent":0.1276595745,"count":6}}},
        {"name": "борьба", "count": 22, "years": {"2012":{"percent":0.1515151515,"count":5},"2013":{"percent":0.1166666667,"count":7},"2014":{"percent":0.0833333333,"count":5},"2015":{"percent":0.1063829787,"count":5}}},
        {"name": "Франция", "count": 77, "years": {"2012":{"percent":0.4848484848,"count":16},"2013":{"percent":0.4,"count":24},"2014":{"percent":0.3666666667,"count":22},"2015":{"percent":0.3191489362,"count":15}}}
    ],
    "usa": [
        {"name": "создавать", "count": 12},
        {"name": "человечность", "count": 13},
        {"name": "страны","count": 19},
        {"name": "любовь", "count": 20},
        {"name": "американский", "count": 26},
        {"name": "дом", "count": 28},
        {"name": "первый", "count": 33},
        {"name": "войска", "count": 45},
        {"name": "семейный", "count": 68},
        {"name": "Рождество", "count": 83}
    ]
};

var version = window.navigator.userAgent.toLowerCase();
if(version.indexOf('iphone os') !== -1) {
    var pos = version.indexOf('iphone os')+10;
    if(parseInt(version[pos]) >= 9) {
        var script = document.createElement('script');
        script.src = '../../../_vendors_/js/d3/3.4.11/d3.min.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
        script = document.createElement('script');
        script.src = 'js/main_mobile.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
    } else {
        script = document.createElement('script');
        script.src = 'js/main_mobile_static.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
    }
}

if(version.indexOf('android') !== -1) {
    var pos = version.indexOf('android')+8;
    if(parseInt(version[pos]) == 4) {
        if(parseInt(version[pos+2]) >= 4) {
            var script = document.createElement('script');
            script.src = '../../../_vendors_/js/d3/3.4.11/d3.min.js';
            script.async = false; // чтобы гарантировать порядок
            document.head.appendChild(script);
            script = document.createElement('script');
            script.src = 'js/main_mobile.js';
            script.async = false; // чтобы гарантировать порядок
            document.head.appendChild(script);
        } else {
            script = document.createElement('script');
            script.src = 'js/main_mobile_static.js';
            script.async = false; // чтобы гарантировать порядок
            document.head.appendChild(script);
        }
    } else if (parseInt(version[pos]) > 4) {
        var script = document.createElement('script');
        script.src = '../../../_vendors_/js/d3/3.4.11/d3.min.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
        script = document.createElement('script');
        script.src = 'js/main_mobile.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
    } else {
        script = document.createElement('script');
        script.src = 'js/main_mobile_static.js';
        script.async = false; // чтобы гарантировать порядок
        document.head.appendChild(script);
    }
}