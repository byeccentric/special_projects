var allData = {
    "rus": [
        {"name": "зависеть", "count": 6, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0416666667,"count":1},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0476190476,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "семья", "count": 7, "years": {"2009":{"percent":0.0952380952,"count":2},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "дети","count": 9, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.125,"count":3},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.04,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "вместе", "count": 9, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.1052631579,"count":2}}},
        {"name": "близкий", "count": 11, "years": {"2009":{"percent":0.1428571429,"count":3},"2011":{"percent":0.0588235294,"count":1},"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.1111111111,"count":2}}},
        {"name": "любовь", "count": 12, "years": {"2009":{"percent":0.0476190476,"count":1},"2010":{"percent":0.0416666667,"count":1},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.04,"count":1},"2014":{"percent":0.1052631579,"count":2},"2015":{"percent":0.1111111111,"count":2}}},
        {"name": "благополучие", "count": 15, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.0476190476,"count":1},"2013":{"percent":0.08,"count":2},"2014":{"percent":0.1578947368,"count":3},"2015":{"percent":0.1666666667,"count":3}}},
        {"name": "здоровье", "count": 16, "years": {"2009":{"percent":0.1428571429,"count":3},"2010":{"percent":0.1666666667,"count":4},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.12,"count":3},"2014":{"percent":0.052631579,"count":1},"2015":{"percent":0.0555555556,"count":1}}},
        {"name": "друзья", "count": 21, "years": {"2009":{"percent":0.0952380952,"count":2},"2010":{"percent":0.0833333333,"count":2},"2011":{"percent":0.1176470588,"count":2},"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.2,"count":5},"2014":{"percent":0.2105263158,"count":4},"2015":{"percent":0.1666666667,"count":3}}},
        {"name": "Россия", "count": 39, "years": {"2009":{"percent":0.1904761905,"count":4},"2010":{"percent":0.375,"count":9},"2011":{"percent":0.3529411765,"count":6},"2012":{"percent":0.1904761905,"count":4},"2013":{"percent":0.28,"count":7},"2014":{"percent":0.2631578947,"count":5},"2015":{"percent":0.2222222222,"count":4}}}
    ],
    "gb2": [
        {"name": "общественный", "count": 12, "years": {"2010":{"percent":0.1111111111,"count":10},"2011":{"percent":0.0952380952,"count":2}}},
        {"name": "обслуживание", "count": 12, "years": {"2010":{"percent":0.1111111111,"count":10},"2011":{"percent":0.0952380952,"count":2}}},
        {"name": "обезопасить","count": 13, "years": {"2010":{"percent":0.0666666667,"count":6},"2011":{"percent":0.0476190476,"count":1},"2013":{"percent":0.04,"count":1},"2015":{"percent":0.2173913044,"count":5}}},
        {"name": "экономика", "count": 16, "years": {"2010":{"percent":0.1,"count":9},"2011":{"percent":0.0952380952,"count":2},"2013":{"percent":0.2,"count":5}}},
        {"name": "правительство", "count": 18, "years": {"2010":{"percent":0.1333333333,"count":12},"2011":{"percent":0.0952380952,"count":2},"2012":{"percent":0.125,"count":3},"2015":{"percent":0.0434782609,"count":1}}},
        {"name": "создавать", "count": 19, "years": {"2010":{"percent":0.0777777778,"count":7},"2011":{"percent":0.0476190476,"count":1},"2012":{"percent":0.1666666667,"count":4},"2013":{"percent":0.12,"count":3},"2015":{"percent":0.1739130435,"count":4}}},
        {"name": "страны", "count": 28, "years": {"2010":{"percent":0.1444444444,"count":13},"2011":{"percent":0.0952380952,"count":2},"2012":{"percent":0.25,"count":6},"2013":{"percent":0.12,"count":3},"2015":{"percent":0.1739130435,"count":4}}},
        {"name": "Великобритания", "count": 31, "years": {"2010":{"percent":0.1555555556,"count":14},"2011":{"percent":0.1428571429,"count":3},"2012":{"percent":0.1666666667,"count":4},"2013":{"percent":0.12,"count":3},"2015":{"percent":0.3043478261,"count":7}}},
        {"name": "работа", "count": 34, "years": {"2010":{"percent":0.1,"count":9},"2011":{"percent":0.2857142857,"count":6},"2012":{"percent":0.2916666667,"count":7},"2013":{"percent":0.4,"count":10},"2015":{"percent":0.0869565217,"count":2}}},
    ],
    "gb1": [
        {"name": "война", "count": 10, "years": {"2014":{"percent":0.2051282051,"count":8},"2015":{"percent":0.0512820513,"count":2}}},
        {"name": "спорт", "count": 11, "years": {"2010":{"percent":0.2666666667,"count":8},"2012":{"percent":0.0769230769,"count":2},"2014":{"percent":0.0256410256,"count":1}}},
        {"name": "общество","count": 12, "years": {"2010":{"percent":0.1,"count":3},"2011":{"percent":0.1818181818,"count":8},"2015":{"percent":0.0256410256,"count":1}}},
        {"name": "надежда", "count": 13, "years": {"2010":{"percent":0.0333333333,"count":1},"2011":{"percent":0.1136363636,"count":5},"2013":{"percent":0.0740740741,"count":2},"2014":{"percent":0.0512820513,"count":2},"2015":{"percent":0.0769230769,"count":3}}},
        {"name": "игра", "count": 13, "years": {"2010":{"percent":0.2,"count":6},"2012":{"percent":0.0384615385,"count":1},"2014":{"percent":0.1538461538,"count":6}}},
        {"name": "любовь", "count": 18, "years": {"2010":{"percent":0.0666666667,"count":2},"2011":{"percent":0.0909090909,"count":4},"2012":{"percent":0.1153846154,"count":3},"2013":{"percent":0.1111111111,"count":3},"2014":{"percent":0.0512820513,"count":2},"2015":{"percent":0.1025641026,"count":4}}},
        {"name": "мир", "count": 20, "years":  {"2010":{"percent":0.0666666667,"count":2},"2011":{"percent":0.1136363636,"count":5},"2013":{"percent":0.1111111111,"count":3},"2014":{"percent":0.1538461538,"count":6},"2015":{"percent":0.1025641026,"count":4}}},
        {"name": "семейный", "count": 24, "years":  {"2010":{"percent":0.0333333333,"count":1},"2011":{"percent":0.25,"count":11},"2012":{"percent":0.1153846154,"count":3},"2013":{"percent":0.1481481482,"count":4},"2015":{"percent":0.1282051282,"count":5}}},
        {"name": "рождество", "count": 35, "years": {"2011":{"percent":0.1136363636,"count":5},"2012":{"percent":0.2307692308,"count":6},"2013":{"percent":0.2222222222,"count":6},"2014":{"percent":0.1794871795,"count":7},"2015":{"percent":0.282051282,"count":11}}},
        {"name": "человечность", "count": 49, "years": {"2010":{"percent":0.2333333333,"count":7},"2011":{"percent":0.1363636364,"count":6},"2012":{"percent":0.4230769231,"count":11},"2013":{"percent":0.3333333333,"count":9},"2014":{"percent":0.1794871795,"count":7},"2015":{"percent":0.2307692308,"count":9}}}
    ],
    "ch": [
        {"name": "победа", "count": 6, "years": {"2014":{"percent":0.0253164557,"count":2},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "друзья", "count": 6, "years": {"2014":{"percent":0.0253164557,"count":2},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "новый","count": 9, "years": {"2014":{"percent":0.0632911392,"count":5},"2015":{"percent":0.0487804878,"count":4}}},
        {"name": "жизнь", "count": 14, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.0853658537,"count":7}}},
        {"name": "реформы", "count": 15, "years": {"2014":{"percent":0.1139240506,"count":9},"2015":{"percent":0.0731707317,"count":6}}},
        {"name": "мир", "count": 15, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.0975609756,"count":8}}},
        {"name": "формат", "count": 16, "years": {"2014":{"percent":0.0886075949,"count":7},"2015":{"percent":0.1097560976,"count":9}}},
        {"name": "должны", "count": 17, "years": {"2014":{"percent":0.1265822785,"count":10},"2015":{"percent":0.0853658537,"count":7}}},
        {"name": "народ", "count": 29, "years": {"2014":{"percent":0.2151898734,"count":17},"2015":{"percent":0.1463414634,"count":12}}},
        {"name": "Китай", "count": 34, "years": {"2014":{"percent":0.164556962,"count":13},"2015":{"percent":0.256097561,"count":21}}}
    ],
    "ger": [
        {"name": "семейный", "count": 6, "years": {"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.0769230769,"count":3}}},
        {"name": "целостность", "count": 7, "years": {"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.1212121212,"count":4}}},
        {"name": "сплоченность","count": 9, "years":  {"2012":{"percent":0.1428571429,"count":3},"2013":{"percent":0.0512820513,"count":2},"2014":{"percent":0.0909090909,"count":3},"2015":{"percent":0.03125,"count":1}}},
        {"name": "согражданин", "count": 9, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.0606060606,"count":2},"2015":{"percent":0.0625,"count":2}}},
        {"name": "благодарность", "count": 9, "years": {"2012":{"percent":0.0476190476,"count":1},"2014":{"percent":0.0303030303,"count":1},"2015":{"percent":0.21875,"count":7}}},
        {"name": "любовь", "count": 10, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1025641026,"count":4},"2014":{"percent":0.0606060606,"count":2},"2015":{"percent":0.0625,"count":2}}},
        {"name": "Европа", "count": 11, "years": {"2013":{"percent":0.0769230769,"count":3},"2014":{"percent":0.1818181818,"count":6},"2015":{"percent":0.0625,"count":2}}},
        {"name": "человеческий", "count": 16, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1025641026,"count":4},"2014":{"percent":0.1818181818,"count":6},"2015":{"percent":0.125,"count":4}}},
        {"name": "жизнь", "count": 27, "years": {"2012":{"percent":0.0952380952,"count":2},"2013":{"percent":0.1538461538,"count":6},"2014":{"percent":0.1515151515,"count":5},"2015":{"percent":0.125,"count":4}}},
        {"name": "Германия", "count": 31, "years": {"2012":{"percent":0.2857142857,"count":6},"2013":{"percent":0.282051282,"count":11},"2014":{"percent":0.1212121212,"count":4},"2015":{"percent":0.3125,"count":10}}}
    ],
    "vat": [
        {"name": "насилие", "count": 9, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.025974026,"count":2},"2015":{"percent":0.03125,"count":3}}},
        {"name": "страдание", "count": 10, "years" :{"2014":{"percent":0.0649350649,"count":5},"2015":{"percent":0.0520833333,"count":5}}},
        {"name": "надежда","count": 12, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.025974026,"count":2},"2015":{"percent":0.0625,"count":6}}},
        {"name": "братья", "count": 18, "years" :{"2013":{"percent":0.064516129,"count":6},"2014":{"percent":0.1038961039,"count":8},"2015":{"percent":0.0416666667,"count":4}}},
        {"name": "дети", "count": 21, "years" :{"2013":{"percent":0.0752688172,"count":7},"2014":{"percent":0.1168831169,"count":9},"2015":{"percent":0.0520833333,"count":5}}},
        {"name": "любовь", "count": 22, "years" :{"2013":{"percent":0.0967741936,"count":9},"2014":{"percent":0.0909090909,"count":7},"2015":{"percent":0.0625,"count":6}}},
        {"name": "Рождество", "count": 28, "years" :{"2013":{"percent":0.0430107527,"count":4},"2014":{"percent":0.1168831169,"count":9},"2015":{"percent":0.15625,"count":15}}},
        {"name": "человечность", "count": 42, "years" :{"2013":{"percent":0.1505376344,"count":14},"2014":{"percent":0.1688311688,"count":13},"2015":{"percent":0.15625,"count":15}}},
        {"name": "мир", "count": 43, "years" :{"2013":{"percent":0.2580645161,"count":24},"2014":{"percent":0.0909090909,"count":7},"2015":{"percent":0.125,"count":12}}},
        {"name": "бог", "count": 61, "years" :{"2013":{"percent":0.2258064516,"count":21},"2014":{"percent":0.1948051948,"count":15},"2015":{"percent":0.2604166667,"count":25}}}
    ],
    "fra": [
        {"name": "мир", "count": 9, "years": {"2013":{"percent":0.05,"count":3},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.0425531915,"count":2}}},
        {"name": "доверие", "count": 9, "years": {"2012":{"percent":0.0909090909,"count":3},"2014":{"percent":0.1,"count":6}}},
        {"name": "европа","count": 10, "years": {"2012":{"percent":0.0303030303,"count":1},"2013":{"percent":0.0666666667,"count":4},"2014":{"percent":0.0333333333,"count":2},"2015":{"percent":0.0638297872,"count":3}}},
        {"name": "великий", "count": 13, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0333333333,"count":2},"2014":{"percent":0.0833333333,"count":5},"2015":{"percent":0.085106383,"count":4}}},
        {"name": "социальный", "count": 14, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0833333333,"count":5},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.0638297872,"count":3}}},
        {"name": "соотечественник", "count": 14, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.0666666667,"count":4},"2014":{"percent":0.0666666667,"count":4},"2015":{"percent":0.085106383,"count":4}}},
        {"name": "должен", "count": 15, "years": {"2013":{"percent":0.1333333333,"count":8},"2014":{"percent":0.0333333333,"count":2},"2015":{"percent":0.1063829787,"count":5}}},
        {"name": "против", "count": 17, "years": {"2012":{"percent":0.0606060606,"count":2},"2013":{"percent":0.05,"count":3},"2014":{"percent":0.1,"count":6},"2015":{"percent":0.1276595745,"count":6}}},
        {"name": "борьба", "count": 22, "years": {"2012":{"percent":0.1515151515,"count":5},"2013":{"percent":0.1166666667,"count":7},"2014":{"percent":0.0833333333,"count":5},"2015":{"percent":0.1063829787,"count":5}}},
        {"name": "Франция", "count": 77, "years": {"2012":{"percent":0.4848484848,"count":16},"2013":{"percent":0.4,"count":24},"2014":{"percent":0.3666666667,"count":22},"2015":{"percent":0.3191489362,"count":15}}}
    ],
    "usa": [
        {"name": "создавать", "count": 12, "years": {"2009":{"percent":0.0735294118,"count":5},"2010":{"percent":0.0434782609,"count":2},"2011":{"percent":0.0227272727,"count":1},"2012":{"percent":0.0677966102,"count":4}}},
        {"name": "человечность", "count": 13, "years": {"2009":{"percent":0.0294117647,"count":2},"2010":{"percent":0.0434782609,"count":2},"2011":{"percent":0.0454545454,"count":2},"2012":{"percent":0.0677966102,"count":4},"2013":{"percent":0.0508474576,"count":3}}},
        {"name": "страны","count": 19, "years": {"2009":{"percent":0.0441176471,"count":3},"2010":{"percent":0.0434782609,"count":2},"2011":{"percent":0.0454545454,"count":2},"2012":{"percent":0.0508474576,"count":3},"2013":{"percent":0.0677966102,"count":4},"2014":{"percent":0.0555555556,"count":2},"2015":{"percent":0.0857142857,"count":3}}},
        {"name": "любовь", "count": 20, "years": {"2009":{"percent":0.0294117647,"count":2},"2010":{"percent":0.0869565217,"count":4},"2011":{"percent":0.0909090909,"count":4},"2012":{"percent":0.0508474576,"count":3},"2013":{"percent":0.0508474576,"count":3},"2014":{"percent":0.0833333333,"count":3},"2015":{"percent":0.0285714286,"count":1}}},
        {"name": "американский", "count": 26, "years": {"2009":{"percent":0.0882352941,"count":6},"2010":{"percent":0.0652173913,"count":3},"2011":{"percent":0.0909090909,"count":4},"2012":{"percent":0.0847457627,"count":5},"2013":{"percent":0.0677966102,"count":4},"2014":{"percent":0.0277777778,"count":1},"2015":{"percent":0.0857142857,"count":3}}},
        {"name": "дом", "count": 28, "years": {"2009":{"percent":0.1176470588,"count":8},"2010":{"percent":0.0652173913,"count":3},"2011":{"percent":0.0454545454,"count":2},"2012":{"percent":0.0677966102,"count":4},"2013":{"percent":0.1016949152,"count":6},"2014":{"percent":0.0555555556,"count":2},"2015":{"percent":0.0857142857,"count":3}}},
        {"name": "первый", "count": 33, "years": {"2009":{"percent":0.0735294118,"count":5},"2010":{"percent":0.1304347826,"count":6},"2011":{"percent":0.0681818182,"count":3},"2012":{"percent":0.1016949152,"count":6},"2013":{"percent":0.1186440678,"count":7},"2014":{"percent":0.0833333333,"count":3},"2015":{"percent":0.0857142857,"count":3}}},
        {"name": "войска", "count": 45, "years": {"2009":{"percent":0.1176470588,"count":8},"2010":{"percent":0.1304347826,"count":6},"2011":{"percent":0.1363636364,"count":6},"2012":{"percent":0.1186440678,"count":7},"2013":{"percent":0.1355932203,"count":8},"2014":{"percent":0.1388888889,"count":5},"2015":{"percent":0.1428571429,"count":5}}},
        {"name": "семейный", "count": 68, "years": {"2009":{"percent":0.1911764706,"count":13},"2010":{"percent":0.1739130435,"count":8},"2011":{"percent":0.2272727273,"count":10},"2012":{"percent":0.186440678,"count":11},"2013":{"percent":0.1525423729,"count":9},"2014":{"percent":0.25,"count":9},"2015":{"percent":0.2285714286,"count":8}}},
        {"name": "Рождество", "count": 83, "years": {"2009":{"percent":0.2352941176,"count":16},"2010":{"percent":0.2173913044,"count":10},"2011":{"percent":0.2272727273,"count":10},"2012":{"percent":0.2033898305,"count":12},"2013":{"percent":0.2542372881,"count":15},"2014":{"percent":0.3055555556,"count":11},"2015":{"percent":0.2571428571,"count":9}}}
    ]
};

var root;

var default_choise = "rus";
var this_choise = default_choise;

root = typeof exports !== "undefined" && exports !== null ? exports : this;

var tooltip = d3.select("body")
    .append("div")
    .attr('class', 'tooltip')
    .text("tooltip");

var chart, collide, collisionPadding, connectEvents, data, force, gravity, height, idValue, jitter, label, margin, maxRadius, minCollisionRadius, mouseout, mouseover, node, rScale, rValue, textValue, tick, transformData, update, updateLabels, updateNodes, width;
width = $('#vis').width();
height = 383;
data = [];
node = null;
label = null;
margin = {
    top: 80,
    right: 0,
    bottom: 0,
    left: -30
};
maxRadius = 70;
rScale = d3.scale.sqrt().range([10, maxRadius]);
rValue = function (d) {
    return parseInt(d.count);
};
idValue = function (d) {
    return d.name;
};
textValue = function (d) {
    return d.name;
};
collisionPadding = 2;
minCollisionRadius = 20;
jitter = 0.2;
transformData = function(rawData) {
    rawData.forEach(function (d) {
        d.count = parseInt(d.count);
        return rawData.sort(function (a, b) {
            //return 0.5 - Math.random();
            return a.count - b.count;
        });
    });
    return rawData;
};
tick = function (e) {
    var dampenedAlpha;
    dampenedAlpha = e.alpha * 0.1;
    node.each(gravity(dampenedAlpha)).each(collide(jitter)).attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
    });
    return label.style("left", function (d) {
        return ((margin.left + d.x) - d.dx / 2) + "px";
    }).style("top", function (d) {
        return ((margin.top + d.y) - d.dy / 2) + "px";
    });
};
chart = function (selection) {
    return selection.each(function (rawData) {
        var maxDomainValue, svg, svgEnter;
        data = transformData(rawData);
        maxDomainValue = d3.max(data, function (d) {
            return rValue(d);
        });
        rScale.domain([0, maxDomainValue]);
        svg = d3.select(this).selectAll("svg").data([data]);
        svgEnter = svg.enter().append("svg");
        svg.attr("width", width);
        svg.attr("height", height + margin.top + margin.bottom);
        svgEnter.select("g").remove();
        node = svgEnter.append("g").attr("id", "bubble-nodes").attr("transform", "translate(" + margin.left + "," + margin.top + ")").attr("width", width).attr("height", height).attr("vector-effect", "non-scaling-stroke");
        label = d3.select(this).selectAll("#bubble-labels").data([data]).enter().append("div").attr("id", "bubble-labels");
        update();
    });
};
update = function () {
    data.forEach(function (d, i) {
        return d.forceR = Math.max(minCollisionRadius, rScale(rValue(d)));
    });
    force.nodes(data).start();
    updateNodes();
    return updateLabels();
};
updateNodes = function () {
    node = node.selectAll(".bubble-node").data(data, function (d) {
        return idValue(d);
    });
    node.exit().remove();
    return node.enter().append("g").attr("class", "bubble-node").call(force.drag).call(connectEvents).append("circle").attr("r", function (d) {
        return rScale(rValue(d));
    });
};
updateLabels = function () {
    var labelEnter;
    label = label.selectAll(".bubble-label").data(data, function (d) {
        return idValue(d);
    });
    label.exit().remove();
    labelEnter = label.enter();
    labelEnter.append("span")
        .attr("class", "bubble-label").text(function (d) {
        return textValue(d);
    })
        .style("font-size", function (d) {
            if(d.name.length < 15)
                return Math.max(8, rScale(rValue(d) / (d.name.length*15))) + "px";
            else
                return 8+'px';
        })
        .call(force.drag)
        .call(connectEvents);

    label.style("width", function (d) {
        return 2 * rScale(rValue(d)) + "px";
    });
    label.append("span").text(function (d) {
        return textValue(d);
    }).each(function (d) {
        return d.dx = Math.max(2.5 * rScale(rValue(d)), this.getBoundingClientRect().width);
    }).remove();
    label.style("width", function (d) {
        return d.dx + "px";
    });
    return label.each(function (d) {
        return d.dy = this.getBoundingClientRect().height;
    });
};
gravity = function (alpha) {
    var ax, ay, cx, cy;
    cx = width;
    cy = height;
    ax = alpha / 20;
    ay = alpha / 120;
    return function (d) {
        d.x += (cx - d.x) * ax;
        return d.y += (cy - d.y) * ay;
    };
};
collide = function (jitter) {
    return function (d) {
        return data.forEach(function (d2) {
            var distance, minDistance, moveX, moveY, x, y;
            if (d !== d2) {
                x = d.x - d2.x;
                y = d.y - d2.y;
                distance = Math.sqrt(x * x + y * y);
                minDistance = d.forceR + d2.forceR + collisionPadding;
                if (distance < minDistance) {
                    distance = (distance - minDistance) / distance * jitter;
                    moveX = x / (width > 470 ? 0.5 : 2) * distance;
                    moveY = y / 5 * distance;
                    d.x -= moveX;
                    d.y -= moveY;
                    d2.x += moveX;
                    return d2.y += moveY;
                }
            }
        });
    };
};
connectEvents = function (d) {
    return;
};
chart.jitter = function (_) {
    if (!arguments.length) {
        return jitter;
    }
    jitter = _;
    force.start();
    return chart;
};
chart.height = function (_) {
    if (!arguments.length) {
        return height;
    }
    height = _;
    return chart;
};
chart.width = function (_) {
    if (!arguments.length) {
        return width;
    }
    width = _;
    return chart;
};
chart.r = function (_) {
    if (!arguments.length) {
        return rValue;
    }
    rValue = _;
    return chart;
};

force = d3.layout.force().gravity(0.03).charge(-30).size([width, height]).on("tick", tick);
chart(d3.select("#vis").datum(allData[this_choise]));

function reDraw(boom) {
    $('#vis').empty();
    force.gravity(boom);
    setTimeout(function () {
        force.gravity(0.03);
    }, 100);
    d3.select(node).remove();
    newData = allData[this_choise];
    chart(d3.select("#vis").datum(newData));
}

var timeOut;
$(window).bind('orientationchange', function() {
    width = $('#vis').width();
    if(width > 470) {
        margin.left = width / 10;
    } else {
        margin.left = -30;
    }
    if(timeOut != "underfined") {
        clearTimeout(timeOut);
        timeOut = setTimeout(function () {
            reDraw(-0.3);
        }, 50);
    }else {
        reDraw(0);
    }
});

$('.choosers').on('click', '.choosers__item', function() {
    if(!$(this).hasClass('choosers__item_active')) {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        $('.choosers__value').html('<span class="choosers__value-name">'+$(this).find('.choosers__item-name').html()+'</span><span class="choosers__value-country">'+$(this).find('.choosers__item-country').html()+'</span>');
        $('.chooser__item').removeClass('chooser__item_active');
        this_choise = $(this).data('link');
        $('.words__image-file').hide();
        $('.words__image-file.' + this_choise).show();
        reDraw(-0.5);
    }
    return false;
});

$('.choosers__value').click(function() {
    if($('.choosers__list').css('display') == "block") {
        $('.choosers').css('border', '1px solid #CDCDCD;');
    } else {
        $('.choosers').css('border', '1px solid #FFFFFF');
    }
    $('.choosers__list').slideToggle();
});

$(document).on('click', function(evt) {
    if(evt.target.className != "choosers__select" && evt.target.className != "choosers__value" && evt.target.className != "choosers__value-name" && evt.target.className != "choosers__value-country" &&evt.target.className != "choosers__item_active" ) {
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
    }
});


