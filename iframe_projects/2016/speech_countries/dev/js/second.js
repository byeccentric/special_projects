var params = {
    "russian": {
        "name": "Владимир Путин",
        "country": "Россия",
        "image": "images/rus.png"
    },
    "usa": {
        "name": "Барак Обама",
        "country": "США",
        "image": "images/usa.png"
    },
    "france": {
        "name": "Франсуа Олланд",
        "country": "Франция",
        "image": "images/fra.png"
    },
    "china": {
        "name": "Си Цзиньпин",
        "country": "Китай",
        "image": "images/ch.png"
    },
    "england": {
        "name": "Елизавета II",
        "country": "Великобритания",
        "image": "images/gb1.png"
    },
    "german": {
        "name": "Ангела Меркель",
        "country": "Германия",
        "image": "images/ger.png"
    },
    "pope": {
        "name": "Папа Римский Франциск",
        "country": "Ватикан",
        "image": "images/vat.png"
    }
};

var colors = {"Семья":"#8ecfae","Религия":"#69bfae","Пожелания":"#005f65","Деньги":"#165374","Борьба":"#3793be","Ценности":"#14447e","Будущее":"#41679e","Страна":"#7c173d","Общество и политика":"#f05157","Проблемы":"#f39e3d","Долг":"#f69ea4"};
var categories = ["Ценности","Борьба","Будущее","Страна","Общество и политика","Долг","Пожелания","Религия","Семья","Проблемы","Деньги"];

$(document).ready(function() {
    $.ajax({
        url: 'data/themes.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            var data_i = JSON.parse(items.responseText);
            var newData = {};
            for(var i=0, item; item=data_i[i++];){
                if(!newData.hasOwnProperty(item.country)) {
                    newData[item.country] = [];

                }
                newData[item.country].push({
                    name: item.groups,
                    value: item['percent of speech']
                });
            }

            var tempData = {};
            _.each(newData, function(item, key) {
                tempData[key] = item.sort(function(a,b) {
                    return categories.indexOf(a.name) - categories.indexOf(b.name);
                });
            });

            newData = tempData;

            var html = $("#bars_items").html();
            html = _.template(html);
            html = html({'data' : newData, 'countries': params, 'colors': colors, 'cats': categories});
            $('.bars').html(html);
        }
    })
});

$('.bars').on('mouseover', '.bars__item-bar-value',function(e) {
    $('.modal__name').html($(this).data('name'));
    $('.modal__round').css('background', $(this).css('background-color'));
    $('.modal__percent').html($(this).data('value'));
    $('.modal').css('top', $(this).offset().top - 45);
    var fromLeft = e.pageX - $('.modal').width()/1.5;
    if(fromLeft < 40)
        fromLeft = 40;
    if(fromLeft+$('.modal').width() > $('.content').width()+10)
        fromLeft = $('.content').width() +10 - $('.modal').width();
    $('.modal').css('left', fromLeft);
    $('.modal').show();
    $('.bars__item-bar-value').stop();
    $('.bars__item-bar-value[data-name!="'+$(this).data('name')+'"]').animate({'opacity': 0.1}, 200);
    $('.bars__item-bar-value[data-name="'+$(this).data('name')+'"]').css('opacity', 1);
});

$('.bars').on('mouseout', '.bars__item-bar-value',function() {
    $('.bars__item-bar-value').stop();
    $('.bars__item-bar-value').animate({'opacity': 1}, 200);
    $('.modal').hide();
});