"use scrict";

function get_GET () {
    var result = [];
    var search = decodeURIComponent(location.search.substr(1)).split('&');
    for (var i in search) {
        search[i] = search[i].split('=');
        result[ search[i][0] ] = search[i][1];
    }
    return result;
}

var GET = get_GET();

var data = {};
var data_big = {};
var all_count = 0;
var max = 0;
var max_big = {};
var colors = ['#56cc90', '#d7ca22', '#22c7fd', '#df5a3c', '#1c36a5', '#ed8908', 'grey', 'violet', 'brown'];
var default_item = 'Доходы от внешнеэкономической деятельности';

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.value == this.axis.min) ? "left" : ((this.value == this.axis.max) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
        url: '2_detail.json',
        dataType: 'json',
        cache: false,
        complete: function (items){
            var data_i = JSON.parse(items.responseText);

            console.log(data_i);

            for(var j=0,it; it=data_i[j++];) {
                data[it.key] = {};
                for(var i=0,item; item=it.value[i++];) {
                    if(data[it.key].hasOwnProperty(item.subgroup_in)) {
                        data[it.key][item.subgroup_in].push({
                            y: item.data_2016,
                            year: item.year
                        });
                    } else {
                        data[it.key][item.subgroup_in] = [];
                        data[it.key][item.subgroup_in].push({
                            y: item.data_2016,
                            year: item.year
                        });
                    }
                }
                var data_temp = {};
                for(var key in data[it.key]) {
                    data_temp[key] = data[it.key][key].sort(function(a,b) { return a.year - b.year});
                }
                data[it.key] = data_temp;
                var cl = '';
                if(it.key == default_item) {
                    cl = ' choosers__item_active';
                    $('.choosers__value').html(it.key);
                }
                $('.choosers__list').append('<div class="choosers__item'+cl+'">'+it.key+'</div>');
            }

            initDetail(default_item);
        }
    });

    $.ajax({
        url: '2.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            "use strict";

            var data_i = JSON.parse(items.responseText);

            all_count = 0;
            for(var i=0, item; item=data_i[i++];) {
                if(!data_big.hasOwnProperty(item.group_in)) {
                    all_count++;
                    max_big[item.group_in] = 0;
                    data_big[item.group_in] = {
                        name: item.group_in,
                        fillColor: item.group_in == 'Другое' ? '#d8d8d8' : '#56cc90',
                        color: item.group_in == 'Другое' ? '#d8d8d8' : '#56cc90',
                        data: []
                    };
                }
                var mark = {
                    enabled: false
                };
                if(item.pin == 1) {
                    mark = {
                        enabled: true,
                        fillColor: "#397557",
                        symbol: 'square',
                        width: 6,
                        height: 6
                    }
                }
                data_big[item.group_in]['data'].push({
                    y: item.percent,
                    year: item.year,
                    data_2016: item.data_2016,
                    pin: item.pin,
                    comment: item.comment,
                    marker: mark
                });
                if(item.percent > max)
                    max = item.percent;
                if(item.percent > max_big[item.group_in])
                    max_big[item.group_in] = item.percent;
            }
        }
    })
});

function init() {
    var cnt = 1;
    for(var key in data_big) {
        var from_top = 0;
        if(max_big[key] == max) {
            from_top = max_big[key]-5;
        } else {
            from_top = max - max_big[key]+25;
        }
        $('.graphics').append('<div class="graphic" id="container'+cnt+'"></div>');
        Highcharts.chart('container'+cnt, {
            chart: {
                type: 'area',
                spacingBottom: 0,
                spacingTop: 0,
                spacingLeft: 0,
                spacingRight: 0,
                height: 100,
                style: {
                    'fontFamily' : 'ALSStory'
                }
            },
            title: {
                text: key,
                floating: true,
                x: 20,
                y: from_top,
                align: 'left',
                style: {'fontSize': '14px', 'fontWeight': 'bold'}
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                visible: false,
                padding: 0,
                'max': max
            },
            tooltip: {
                useHTML: true,
                formatter: _makeTooltip,
                borderRadius: 0,
                borderWidth: 0,
                padding: 0,
                hideDelay: 10
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        fillColor: '#397557',
                        radius: 3,
                        states: {
                            hover: {
                                symbol: 'circle',
                                enabled: true,
                                radius: 4,
                            }
                        }
                    },
                    fillOpacity: 0.9,
                    lineWidth: 1,
                    states: {
                        hover: {
                            enabled: true,
                            lineWidth: 1,
                            lineWidthPlus: 0,
                        }
                    }
                }
            },
            xAxis: {
                categories: [
                    2005,
                    2006,
                    2007,
                    2008,
                    2009,
                    2010,
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: cnt==1 ? true : false,
                labels: {
                    enabled: cnt==1 || cnt >= all_count ? true : false,
                    align: 'center',
                    useHTML: true,
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickLength: 10,
                endOnTick: false,
                startOnTick: false,
            },
            series: [data_big[key]]
        });
        cnt++;
    }

}
function initDetail(this_val) {
    $('.underGraphic').show();
    $('.legend').show();
    $('.graphic').hide();
    var value = {
        series: []
    };

    var cnt = 0;
    var max = 0;
    $('.legend').html('');
    for(var key in data[this_val]) {
        value.series.push({
            name: key,
            color: colors[cnt],
            data: data[this_val][key]
        });

        for(var i=0, item; item = data[this_val][key][i++];){
            if(item.y > max)
                max = item.y;
        }

        var html = $("#legend_item").html();
        html = _.template(html);
        html = html({"item": { class_add: cnt, name: key, short_name:  makeShortString(key)}});
        $('.legend').append(html);
        cnt++;
    }

    Highcharts.chart('maked', {
            chart: {
                type: 'line',
                spacingBottom: 0,
                spacingTop: 0,
                spacingLeft: 0,
                spacingRight: 20,
                marginRight: 0,
                height: 500,
                style: {
                    'fontFamily' : 'ALSStory'
                }
            },
            title: {
                text: '',
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                visible: true,
                padding: 0,
                lineWidth: 0,
                tickLength: 0,
                lineColor: "#FFF",
                minorGridLineWidth: 0,
                tickWidth: 0,
                offset: 0,
                title: {
                    enabled: false,
                    margin: 0
                },
                labels: {
                    formatter: function() {
                        return formatSumm(this.value);
                    },
                    distance: 0,
                    padding: 0,
                    y: 3,
                    x: -15,
                    zIndex: 10,
                    align: 'right',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                showFirstLabel: false,
                showLastLabel: false,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickAmount: 5,
                'max': max,
            },
            tooltip: {
                useHTML: true,
                formatter: _makeToolptipDetail,
                borderRadius: 0,
                borderWidth: 0,
                padding: 0
            },
            plotOptions: {
                line: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 2
                        }
                    },
                    marker: {
                        symbol: 'circle',
                        enabled: false
                    },
                },

            },
            series: value.series,
            xAxis: [{
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                visible: true,
                opposite: false,
                labels: {
                    overflow: 'justify',
                    align: "center",
                    x: 10,
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                startOnTick: false,
                endOnTick: false,
                minPadding: 0,
                maxPadding: 0,
            },{
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                linkedTo: 0,
                visible: true,
                opposite: true,
                labels: {
                    overflow: 'justify',
                    align: "center",
                    x: 10,
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                startOnTick: false,
                endOnTick: false,
                minPadding: 0,
                maxPadding: 0,
            }]
        }
    );

}
$('.choosers').on('click', '.choosers__item', function() {
    if($(this).data('choose') != "all") {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        $('.choosers__value').html($(this).html());
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        $('.chooser__item').removeClass('chooser__item_active');
        initDetail($(this).html());
    } else {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        $('.choosers__value').html($(this).html());
        $('.underGraphic').hide();
        $('.legend').hide();
        $('.graphic').show();
        init();
    }
    return false;
});

$('.chooser__item').click(function() {
    $('.choosers__item').removeClass('choosers__item_active');
    $(this).addClass('chooser__item_active');
    $('.choosers__list').slideUp();
    $('.choosers').css('border', '1px solid #CDCDCD');
    $('.choosers__value').html($(this).html());
    $('.underGraphic').hide();
    $('.legend').hide();
    $('.graphic').show();
    init();
});

$('.choosers__value').click(function() {
    if($('.choosers__list').css('display') == "block") {
        $('.choosers').css('border', '1px solid #CDCDCD;');
    } else {
        $('.choosers').css('border', '1px solid #FFFFFF');
    }
    $('.choosers__list').slideToggle();
});

$(document).on('click', function(evt) {
    if(evt.target.className != "choosers__select" && evt.target.className != "choosers__value") {
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
    }
});

function _makeTooltip() {
    $('.graphic').css('z-index', '0');
    $('#container'+this.series.chart.index).css('z-index', '1');
    var style = '';
    if(parseInt(this.point.year) > 2011) {
        if(this.series.chart.index > 2)
            style = 'style="right: 0; left: auto;"';
        else
            style = 'style="right: 0; left: auto; top: 20px; bottom: auto;"';
    }
    else if(this.series.chart.index <= 2)
        style = 'style="top: 20px; bottom: auto; z-index: 1000;"';
    var item = {
        year: this.point.year,
        perc: Math.round(this.point.y),
        val: formatSumm(this.point.data_2016),
        name: this.series.name,
        pin: this.point.pin,
        comment: this.point.comment,
        styles: style
    };

    var html = $("#modal-tooltip").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function _makeToolptipDetail() {
    var style = '';
    var style2 = '';
    if(parseInt(this.point.year) > 2014) {
        if(GET['type'] == 'l') {
            style = 'style="right: 0; left: auto; width: 150px;"';
            style2 = 'style="font-size: 14px"';
        }
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(GET['type'] == 'l') {
            style = 'style="width: 150px"';
            style2 = 'style="font-size: 14px"';
        }
    }
    var item = {
        year: this.point.year,
        val: formatSumm(this.point.y),
        name: this.series.name,
        styles: style,
        styles2: style2,
        attr: GET['type']
    };

    var html = $("#modal-tooltip-detail").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 >= 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 >= 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 >= 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 >= 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}

function makeShortString(str) {
    var pieces = str.split(' ');
    var allLength = 0;
    var max = 50;
    var thisStr = '';
    if(pieces.length > 0) {
        for(var i=0,item; item=pieces[i++];) {
            allLength+=item.length;
            if(allLength > max) {
                return thisStr+'...';
            }
            thisStr+=' '+item;
        }
    }
    return thisStr;
}