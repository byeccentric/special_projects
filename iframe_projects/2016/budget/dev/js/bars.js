"use strict";

var data = {};
var values_array = [];

var max_height = 150;

$(document).ready(function() {
    $.ajax({
        url: '1.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            "use strict";

            var data_i = JSON.parse(items.responseText);

            for(var i=0, item; item=data_i[i++];) {
                data[item.year] = {
                    in_2016: parseFloat(String(item.in_2016).replace(/,/g, '')),
                    in_2016_num: String(makeNumberTrln(parseFloat(String(item.in_2016).replace(/,/g, '')))).replace('.', ','),
                    out_2016: parseFloat(String(item.out_2016).replace(/,/g, '')),
                    out_2016_num: String(makeNumberTrln(parseFloat(String(item.out_2016).replace(/,/g, '')))).replace('.', ','),
                    in_fact: makeNumberTrln(parseFloat(String(item.in_fact).replace(/,/g, ''))),
                    out_fact: makeNumberTrln(parseFloat(String(item.out_fact).replace(/,/g, ''))),
                    deficit_perc: item['deficit_%_GDP'],
                    deficit: item['deficit,_mlrd']
                };
                values_array.push(parseFloat(String(item.in_2016).replace(/,/g, '')));
                values_array.push(parseFloat(String(item.out_2016).replace(/,/g, '')));
            }

            var thisMax = Math.max.apply(null, values_array);

            for(var key in data) {
                data[key].in_height = Math.round(parseFloat(data[key].in_2016) / thisMax * max_height);
                data[key].out_height = Math.round(parseFloat(data[key].out_2016) / thisMax * max_height);
            }

            var html = $("#bars").html();
            html = _.template(html);
            html = html({'data' : data});
            $('.bars__items').html(html);
        }
    })
});

function makeNumberTrln(num) {
    return Math.round(num / (1000*1000*1000*100))/10;
}

/*
$('.bars__items').on('mouseover', '.bars__item-in-bar', function () {
    var year = $(this).parent().parent().find('.bars__item-year').html();
    $('.modal__name').html(year);
    if(parseInt(year) < 2015) {
        $('.modal').css({'right': 0, 'left': 'auto'});
    } else {
        $('.modal').css({'left': 0, 'right': 'auto'});
    }

    $('.modal__more-wrapper').html('' +
        '<div class="modal__more-item">Фактические доходы'+
        '<div class="modal__more-item-value">'+data[year]['in_fact']+' трлн руб</div>'+
        '</div></div>'+
        '<div class="modal__more-item">Дефицит бюджета'+
        '<div class="modal__more-item-value">'+data[year]['deficit']+' млрд руб</div>'+
        '</div></div>'+
        '<div class="modal__more-item">Дефицит бюджета %'+
        '<div class="modal__more-item-value">'+data[year]['deficit_perc'] +'%</div>'+
        '</div></div>'
    );

    $('.modal').show();
});

$('.bars__items').on('mouseout', '.bars__item-in-bar', function () {
    $('.modal').hide();
});

$('.bars__items').on('mouseover', '.bars__item-out-bar', function () {
    var year = $(this).parent().parent().find('.bars__item-year').html();
    $('.modal__name').html(year);
    if(parseInt(year) < 2015) {
        $('.modal').css({'right': 0, 'left': 'auto'});
    } else {
        $('.modal').css({'left': 0, 'right': 'auto'});
    }

    $('.modal__more-wrapper').html('' +
        '<div class="modal__more-item">Фактические расходы'+
        '<div class="modal__more-item-value">'+data[year].out_fact+' трлн руб.</div>'+
        '</div></div>'+
        '<div class="modal__more-item">Дефицит бюджета'+
        '<div class="modal__more-item-value">'+data[year].deficit+' млрд руб.</div>'+
        '</div></div>'+
        '<div class="modal__more-item">Дефицит бюджета %'+
        '<div class="modal__more-item-value">'+data[year].deficit_perc+'%</div>'+
        '</div></div>'
    );

    $('.modal').css('display', 'block');
});

$('.bars__items').on('mouseout', '.bars__item-out-bar', function () {
    $('.modal').hide();
});*/
