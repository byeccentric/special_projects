"use strict";

var data = {
    series : []
};

var data_perc = {
    series: []
};

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.value == this.axis.min) ? "left" : ((this.value == this.axis.max) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
        url: '4.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {

            var data_i = JSON.parse(items.responseText);
            var data1 = [];
            var data2 = [];

            for(var i=0,item; item=data_i[i++];) {
                data1.push({
                    y: item.domestic_debt_mlrd*1000000000,
                    year: item.year
                });

                data2.push({
                     y: item.foreign_debt_mlrd*1000000000,
                     year: item.year
                });
            }

            data.series[0] = {
                name: 'Внешний долг',
                fillColor: "#ed8908",
                fillOpacity: 0.8,
                color: "#ed8908",
                data: data1
            };

            data.series[1] = {
                name: 'Внутренний долг',
                fillColor: "#56cc90",
                fillOpacity: 0.8,
                color: "#56cc90",
                data: data2
            };

            initSumm();
        }
    });

    $.ajax({
        url: '4_2.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {

            var data_i = JSON.parse(items.responseText);
            var data1 = [];
            var data2 = [];

            for(var i=0,item; item=data_i[i++];) {
                data1.push({
                    y: item['debt_GDP_%'],
                    vvp: item.GDP,
                    sum: item.debt_summ_fact*1000000000,
                    year: item.year
                });

                data2.push({
                    y: item['debt_service_GDP_%'],
                    vvp: item.GDP,
                    sum: item.service_summ_fact,
                    year: item.year
                });
            }

            data_perc.series[0] = {
                name: 'Внешний долг',
                fillColor: "#ed8908",
                fillOpacity: 0.8,
                color: "#ed8908",
                data: data1
            };

            data_perc.series[1] = {
                name: 'Внутренний долг',
                fillColor: "#56cc90",
                fillOpacity: 0.8,
                color: "#56cc90",
                data: data2
            };
        }
    })
});

$('.choosers__item').click(function () {
    $('.choosers__item').removeClass('choosers__item_active');
    $(this).addClass('choosers__item_active');
    if($(this).data('val') == 'summ') {
        initSumm();
    } else {
        initPerc();
    }
    return false;
});

function initSumm() {
    Highcharts.chart('container', {
        chart: {
            type: 'area',
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            height: 400,
            style: {
                'fontFamily' : 'ALSStory'
            }
        },
        title: {
            text: '<ul class="legend"><li class="legend__item in">Внутренний долг</li><li class="legend__item out">Внешний долг</li></ul>',
            floating: true,
            x: 25,
            y: 40,
            align: 'left',
            style: {'fontSize': '14px', 'fontWeight': 'bold'},
            useHTML: true
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        yAxis: {
            visible: false,
            padding: 0
        },
        tooltip: {
            useHTML: true,
            formatter: _makeTooltip,
            borderRadius: 0,
            borderWidth: 0,
            padding: 0,
            hideDelay: 10,
            zIndex: 5
        },
        plotOptions: {
            area: {
                className: 'some',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    fillColor: '#666666',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 4
                        }
                    }
                },
                fillOpacity: 1,
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        },
        xAxis: [
            {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: false,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickLength: 0,
                endOnTick: false,
                startOnTick: false,
            }, {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: true,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                linkedTo: 0,
                endOnTick: false,
                startOnTick: false,
            }
        ],
        series: data.series
    });
}

function initPerc() {
    Highcharts.chart('container', {
        chart: {
            type: 'area',
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            height: 400,
            style: {
                'fontFamily' : 'ALSStory'
            }
        },
        title: {
            text: '<ul class="legend"><li class="legend__item in">Обслуживание долга</li><li class="legend__item out">Общий долг</li></ul>',
            floating: true,
            x: 25,
            y: 40,
            align: 'left',
            style: {'fontSize': '14px', 'fontWeight': 'bold'},
            useHTML: true
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        yAxis: {
            visible: false,
            padding: 0
        },
        tooltip: {
            useHTML: true,
            formatter: _makeTooltipPerc,
            borderRadius: 0,
            borderWidth: 0,
            padding: 0,
            hideDelay: 10,
            zIndex: 5
        },
        plotOptions: {
            area: {
                className: 'some',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    fillColor: '#666666',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 4
                        }
                    }
                },
                fillOpacity: 1,
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        },
        xAxis: [
            {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: false,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                endOnTick: false,
                startOnTick: false,
            }, {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: true,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickLength: 0,
                linkedTo: 0,
                endOnTick: false,
                startOnTick: false,
            }
        ],
        series: data_perc.series
    });
}

function _makeTooltip() {
    var style = '';
    if(parseInt(this.point.year) > 2014) {
        if(this.y > 7500000000000)
            style = 'style="right: 20px; left: auto; top: 20px; bottom: auto"';
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(this.y > 7500000000000)
            style = 'style="top: 20px; bottom: auto"';
    }
    var item = {
        year: this.point.year,
        val: formatSumm(this.point.y),
        name: this.series.name,
        styles: style
    };

    var html = $("#modal-tooltip").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function _makeTooltipPerc() {
    var style = '';
    if(parseInt(this.point.year) > 2014) {
        if(this.y > 9.6)
            style = 'style="right: 20px; left: auto; top: 20px; bottom: auto"';
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(this.y > 9.6)
            style = 'style="top: 20px; bottom: auto"';
    }
    var item = {
        year: this.point.year,
        perc: Math.round(this.point.y*100)/100,
        vvp: formatSumm(this.point.vvp),
        summ: formatSumm(this.point.sum),
        name: this.series.name,
        styles: style
    };

    var html = $("#modal-tooltip-perc").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 > 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 > 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 > 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 > 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}