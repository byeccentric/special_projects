"use scrict";

var data_big = [];
var data = {};
var default_item = 'Национальная оборона';
var menu_items = ['Все статьи'];

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.value == this.axis.min) ? "left" : ((this.value == this.axis.max) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
        url: '5_2.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            var data_i = JSON.parse(items.responseText);

            var maxHere = {};
            for(var i=0,item; item=data_i[i++];) {
                if(data.hasOwnProperty(item.section)) {
                    data[item.section].series[0].data.push({
                        y: item.data_2016,
                        hidden: item.hidden_data_2016,
                        perc: item['hidden_data_%'],
                        year: item.year
                    });

                    data[item.section].series[1].data.push({
                        data_2016: item.data_2016,
                        y: item.hidden_data_2016,
                        perc: item['hidden_data_%'],
                        year: item.year
                    });
                } else {
                    maxHere[item.section] = 0;
                    data[item.section] = {};
                    data[item.section].series = [
                        {
                            name: 'Все расходы',
                            color: '#df5a3c',
                            maxHere: 0,
                            data: []
                        },
                        {
                            name: 'Скрытые расходы',
                            color: '#b5b5b5',
                            maxHere: 0,
                            data: []
                        }
                    ];

                    data[item.section].series[0].data.push({
                        y: item.data_2016,
                        hidden: item.hidden_data_2016,
                        perc: item['hidden_data_%'],
                        year: item.year
                    });

                    data[item.section].series[1].data.push({
                        data_2016: item.data_2016,
                        y: item.hidden_data_2016,
                        perc: item['hidden_data_%'],
                        year: item.year
                    });
                }

                if(item.data_2016 > maxHere[item.section]) {
                    maxHere[item.section] = item.data_2016;
                    data[item.section].series[0].maxHere = item.data_2016;
                    data[item.section].series[1].maxHere = item.data_2016;
                }
            }

            for(var key in data) {
                data[key].series[0].data = data[key].series[0].data.sort(function(a,b){ return a.year - b.year});
                data[key].series[1].data = data[key].series[1].data.sort(function(a,b){ return a.year - b.year});
                menu_items.push(key);
            }
            buildMenu(default_item);
            initData(default_item);
        }
    });

    $.ajax({
        url: '5.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            var data_i = JSON.parse(items.responseText);
            var data1 = [];
            var data2 = [];

            for(var i=0,item; item=data_i[i++];) {
                data1.push({
                    y: item.hidden_2016,
                    perc: item['hidden_full_%'],
                    year: item.year
                });
                data2.push({
                    y: item.data_2016,
                    perc: item['hidden_full_%'],
                    year: item.year
                });
            }

            data_big[0] = {
                name: 'Все расходы',
                fillColor: "#df5a3c",
                fillOpacity: 0.8,
                color: "#df5a3c",
                data: data2
            };


            data_big[1] = {
                name: 'Скрытые расходы',
                fillColor: "#b5b5b5",
                fillOpacity: 0.8,
                color: "#b5b5b5",
                data: data1
            };
        }
    });
});

$('.choosers').on('click', '.choosers__item', function() {
    if($(this).html() != "Все статьи") {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        $('.choosers__value').html($(this).html());
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        initData($(this).html());
        buildMenu($(this).html());
    } else {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('chooser__item_active');
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
        $('.choosers__value').html($(this).html());
        buildMenu($(this).html());
        $('.underGraphic').hide();
        $('.legend').hide();
        $('.graphic').show();
        init();
    }
    return false;
});

$('.choosers__value').click(function() {
    if($('.choosers__list').css('display') == "block") {
        $('.choosers').css('border', '1px solid #CDCDCD;');
    } else {
        $('.choosers').css('border', '1px solid #FFFFFF');
    }
    $('.choosers__list').slideToggle();
});

$(document).on('click', function(evt) {
    if(evt.target.className != "choosers__select" && evt.target.className != "choosers__value") {
        $('.choosers__list').slideUp();
        $('.choosers').css('border', '1px solid #CDCDCD');
    }
});

function buildMenu(chosen_item) {
    $('.choosers__list').html('');
    for(var i=0,item;item=menu_items[i++];) {
        var cl = '';
        if (item == chosen_item) {
            cl = ' choosers__item_active';
            $('.choosers__value').html(item);
            $('.choosers__list').prepend('<div class="choosers__item' + cl + '">' + item + '</div>');
        } else {
            $('.choosers__list').append('<div class="choosers__item' + cl + '">' + item + '</div>');
        }
    }
}

function init() {
    Highcharts.chart('container', {
        chart: {
            type: 'area',
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            height: 400,
            style: {
                'fontFamily' : 'ALSStory'
            }
        },
        title: {
            text: '<ul class="legend"><li class="legend__item know">Все расходы</li><li class="legend__item unknow">Скрытые расходы</li></ul>',
            floating: true,
            x: 25,
            y: 40,
            align: 'left',
            style: {'fontSize': '14px', 'fontWeight': 'bold'},
            useHTML: true
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        yAxis: {
            visible: false,
            padding: 0
        },
        tooltip: {
            useHTML: true,
            formatter: _makeTooltip,
            borderRadius: 0,
            borderWidth: 0,
            padding: 0,
            hideDelay: 10,
            zIndex: 5
        },
        plotOptions: {
            area: {
                className: 'some',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    fillColor: '#666666',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 4
                        }
                    }
                },
                fillOpacity: 1,
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        },
        xAxis: [
            {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: false,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickLength: 0,
                endOnTick: false,
                startOnTick: false,
            }, {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: true,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                linkedTo: 0,
                endOnTick: false,
                startOnTick: false,
            }
        ],
        series: data_big
    });
}

function initData(type) {
    Highcharts.chart('container', {
        chart: {
            type: 'area',
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            height: 400,
            style: {
                'fontFamily' : 'ALSStory'
            }
        },
        title: {
            text: '<ul class="legend"><li class="legend__item know">Все расходы</li><li class="legend__item unknow">Скрытые расходы</li></ul>',
            floating: true,
            x: 25,
            y: 30,
            align: 'left',
            style: {'fontSize': '14px', 'fontWeight': 'bold'},
            useHTML: true
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        yAxis: {
            visible: false,
            padding: 0
        },
        tooltip: {
            useHTML: true,
            formatter: _makeTooltipData,
            borderRadius: 0,
            borderWidth: 0,
            padding: 0,
            hideDelay: 10,
            zIndex: 5,
            shared: true,
        },
        plotOptions: {
            area: {
                className: 'some',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    fillColor: '#666666',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true,
                            radius: 4
                        }
                    }
                },
                fillOpacity: 1,
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        },
        xAxis: [
            {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: false,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                tickLength: 0,
                endOnTick: false,
                startOnTick: false,
            }, {
                categories: [
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                opposite: true,
                labels: {
                    enabled: true,
                    align: 'center',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                lineWidth: 0,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickLength: 0,
                linkedTo: 0,
                endOnTick: false,
                startOnTick: false,
            }
        ],
        series: data[type].series
    });
}

function _makeTooltip() {
    var style = '';
    if(parseInt(this.point.year) > 2014) {
        if(this.point.y > 11000000000000)
            style = 'style="right: 20px; left: auto; top: 20px; bottom: auto"';
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(this.point.y > 11000000000000)
            style = 'style="top: 20px; bottom: auto"';
    }
    var item = {
        year: this.point.year,
        val: formatSumm(this.point.y),
        perc: Math.round(this.point.perc * 10) / 10,
        name: this.series.name,
        styles: style
    };

    var html = $("#modal-tooltip").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function _makeTooltipData() {
    console.log(this);
    var style = '';
    if(parseInt(this.x) > 3) {
        if(this.y/this.points[0].series.options.maxHere > 0.8)
            style = 'style="right: 20px; left: auto; top: -50px; bottom: auto"';
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(this.y/this.points[0].series.options.maxHere > 0.8)
            style = 'style="top: -50px; bottom: auto"';
    }
    var item = {
        year: this.points[0].point.year,
       /* val: formatSumm(this.point.y),*/
        /*perc: Math.round(this.point.perc * 10) / 10,*/
        /*name: this.series.name,*/
        points: this.points,
        styles: style
    };

    var html = $("#modal-tooltip").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 > 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 > 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 > 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 > 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}
