'use strict';

const gulp = require('gulp'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    inject = require('gulp-inject'),
    babel = require('gulp-babel'),
    minify = require('gulp-minify');

let path_src = './src/blocks/';
let path_dest_dev = './dev/';
let path_dest_pub = './pub/';
let path_vendor = '../../_vendors_/';

var pages = {
    income: {
        blocks: [
            'header',
            'plus_graphic',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    income_test: {
        blocks: [
            'header',
            'plus_graphic_test',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    expenses: {
        blocks: [
            'header',
            'minus_graphic',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    debts: {
        blocks: [
            'header',
            'debts',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    hidden: {
        blocks: [
            'header',
            'hidden',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    bars: {
        blocks: [
            'header',
            'bars_up',
            'footer'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
    income_mobile: {
        blocks: [
            'header_mob',
            'plus_mob',
            'footer_mob'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js'
        ]
    },
    expenses_mobile: {
        blocks: [
            'header_mob',
            'minus_mob',
            'footer_mob'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/highcharts/v5.0.0/highcharts.js'
        ]
    },
    bars_mobile: {
        blocks: [
            'header_mob',
            'bars_mob',
            'footer_mob'
        ],
        scripts: [
            path_vendor+'js/jquery/3.1.1/jquery.min.js',
            path_vendor+'js/underscore/1.8.3/underscore.js'
        ]
    },
};

gulp.task('css:build', function() {
    let count_max = Object.keys(pages).length;
    let counter = 1;
    for(var i in pages) {
        let page = pages[i];
        var less_src = [path_vendor + 'css/clear.less', path_vendor + 'css/fonts.less'];
        page.blocks.forEach(item => less_src.push(path_src + item + '/*.less'));

        if (counter < count_max)
            gulp.src(less_src)
                .pipe(less())
                .pipe(concat(i + '.css'))
                .pipe(gulp.dest(path_dest_dev + 'css/'));
        else
            return gulp.src(less_src)
                .pipe(less())
                .pipe(concat(i + '.css'))
                .pipe(gulp.dest(path_dest_dev + 'css/'));
        counter++;
    }
});

gulp.task('js:build', function() {
    var count_max = Object.keys(pages).length;
    var counter = 1;
    for(var i in pages) {
        var js_src = [];
        for(var j=0; j<pages[i].blocks.length; j++) {
            js_src.push(path_src+pages[i].blocks[j]+'/*.js');
        }
        if(counter < count_max)
            gulp.src(js_src)
                .pipe(concat(i+'.js'))
                .pipe(gulp.dest(path_dest_dev+'js/'));
        else
            return gulp.src(js_src)
                .pipe(concat(i+'.js'))
                .pipe(babel({presets: ['es2015']}))
                .pipe(gulp.dest(path_dest_dev+'js/'));
        counter++;
    }
});

gulp.task('build', ['css:build', 'js:build'], function (callback) {
    for(var i in pages) {
        var html_src = [];
        for(var j=0; j<pages[i].blocks.length; j++) {
            html_src[j] = path_src+pages[i].blocks[j]+'/*.html';
        }

        let src_files = pages[i].scripts;
        src_files.push(path_dest_dev+'css/'+i+'.css', path_dest_dev+'js/'+i+'.js');

        gulp.src(html_src)
            .pipe(concat(i+'.html'))
            .pipe(inject(
                gulp.src(
                    src_files,
                    {read: false}
                ),
                {
                    transform: function (filepath) {
                        var realpath = filepath.replace('/dev/', './');
                        realpath = realpath.replace('/../../', new Array(4).join('../'));
                        if (filepath.slice(-4) == '.css') {
                            return '<link rel="stylesheet" href="'+realpath+'" />';
                        } else {
                            return '<script type="text/javascript" src="'+realpath+'"></script>';
                        }
                    }
                }
            ))
            .pipe(gulp.dest(path_dest_dev));
    }

    callback();
});

gulp.watch(path_src+'**/*.*', ['build']);