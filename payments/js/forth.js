$('.products-costs__select-value').click(function() {
    $('.products-costs__select-list').slideToggle();
});

$('.products-costs__select-item').click(function() {
    $('.products-costs__select-item').removeClass('products-costs__select-item_active');
    $(this).addClass('products-costs__select-item_active');
    $('.products-costs__select-value').html($(this).html());
    $('.products-costs__select-list').slideUp();
});

$(document).on('click', function(evt) {
    if(evt.target.className != "products-costs__select" && evt.target.className != "products-costs__select-value") {
        $('.products-costs__select-list').slideUp();
    }
});