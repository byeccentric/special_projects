var i_data=[];
var map_data=[];
var data_people=[];

$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);

            console.log(i_data);
            for(var i = 0; i<i_data.length; i++) {
                var summ = parseInt(i_data[i]['total_amount_of_spending_1_month_middle_salary']);
                map_data[i] = {
                    'lat': i_data[i].lat,
                    'lon': i_data[i].lng,
                    'name': i_data[i].city,
                    'z': parseInt(i_data[i]['avg_zp']),
                    'values': {
                        'summ': summ,
                        'zp': parseInt(i_data[i]['avg_zp']),
                        'pensiya': parseInt(i_data[i]['avg_penciya']),
                        'region': i_data[i].region
                    }
                }
            }

            /*map_data[17] = {
                'lat': 56.148352,
                'lon': 72.932835,
                'name': "Красноярка",
                'z': 123123123,
                'values': {
                    'summ': 152515,
                    'zp': 123123,
                    'pensiya': 123123,
                    'region': '234123123'
                }
            }*/

            console.log(map_data);

            $(function () {
                var highChartsSettings = {
                    chart: {
                        renderTo: 'container'
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        enabled: false
                        //valueDecimals: 0
                    },
                    mapNavigation: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [
                        {
                            data: [],
                            mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
                            joinBy: 'hc-key',
                            name: '',
                            borderWidth: 1,
                            borderColor: '#fff',
                            nullColor: "#EDEDED"
                        },
                        {
                            type: "mapbubble",
                            color: 'transparent',
                            data: map_data,
                            dataLabels: {
                                enabled: true,
                                borderRadius: 500,
                                useHTML: true,
                                formatter: _labelFormatter
                            },
                            events: {
                                mouseOver: "return false;"
                            },
                            maxSize: '10%',
                            minSize: '4%'
                        }
                    ]
                };

                Highcharts.setOptions({
                    global: {
                        timezoneOffset: 3 * -60 // minutes
                    },
                    lang: {
                        contextButtonTitle: 'Контекстное меню',
                        decimalPoint: '.',
                        downloadJPEG: 'Экспорт в формате JPEG',
                        downloadPDF: 'Экспорт в формате PDF',
                        downloadPNG: 'Экспорт в формате PNG',
                        downloadSVG: 'Экспорт в формате SVG',
                        loading: 'Загрузка...',
                        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        shortMonths: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                        weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                        numericSymbols: ['тыс', 'млн', 'млр', 'трл', 'P', 'E'],
                        printChart: 'На печать',
                        rangeSelectorFrom: 'От',
                        rangeSelectorTo: 'До',
                        rangeSelectorZoom: 'Масштаб',
                        resetZoom: 'Масштаб по-умолчанию',
                        resetZoomTitle: 'Масштаб 1:1',
                    }
                });

                map = new Highcharts.Map(highChartsSettings);

            });
        }
    })
});



function _labelFormatter() {
    var html = '<div class="wrapper" id="'+this.key+'" ' +
        'style="z-index: 100; width: '+(this.point.dlBox.width+2)+'px; height: '+(this.point.dlBox.width+2)+'px; "' +
        'data-name="'+this.point.name+'"' +
        'data-region="'+this.point.values.region+'"' +
        'data-value="'+this.point.values.summ+'"' +
        'data-zp="'+this.point.values.zp+'"' +
        '>' +
        '<div class="pie spinner"></div>' +
        '<div class="pie filler"></div>' +
        '<div class="mask"></div>' +
        '</div>';
    return html;
}

$(document).on('mouseover', '.wrapper', function() {
    var coords = $(this).offset();
    $('.costs-modal__name').html($(this).data('name'));
    $('.costs-modal__region').html($(this).data('region'));
    $('.costs-modal').css({'top': coords.top, 'left': coords.left+$(this).width()}).show();
});

$(document).on('mouseout', '.wrapper', function() {
    $('.costs-modal').hide();
});