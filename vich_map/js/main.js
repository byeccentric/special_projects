var i_data = [];
var data = [];

var min = 1;
var max = 0;


$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);

            for(var i = 0; i<i_data.length; i++) {
                data.push({
                    'name': i_data[i].region_name,
                    'hc-key': i_data[i].region_id,
                    'value': i_data[i].part_of_infected_people_count,
                    'to_59_count': i_data[i].people_count_0_59_years,
                    'vich_count': i_data[i].people_count_vich
                });

                if(parseFloat(i_data[i].part_of_infected_people_count) > max) {
                    max = parseFloat(i_data[i].part_of_infected_people_count);
                }

                if(parseFloat(i_data[i].part_of_infected_people_count) < min) {
                    min = parseFloat(i_data[i].part_of_infected_people_count);
                }
            }

            Highcharts.setOptions({
                global: {
                    timezoneOffset: 3 * -60 // minutes
                },
                lang: {
                    contextButtonTitle: 'Контекстное меню',
                    decimalPoint: '.',
                    downloadJPEG: 'Экспорт в формате JPEG',
                    downloadPDF: 'Экспорт в формате PDF',
                    downloadPNG: 'Экспорт в формате PNG',
                    downloadSVG: 'Экспорт в формате SVG',
                    loading: 'Загрузка...',
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    shortMonths: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                    weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    numericSymbols: ['тыс', 'млн', 'млр', 'трл', 'P', 'E'],
                    printChart: 'На печать',
                    rangeSelectorFrom: 'От',
                    rangeSelectorTo: 'До',
                    rangeSelectorZoom: 'Масштаб',
                    resetZoom: 'Масштаб по-умолчанию',
                    resetZoomTitle: 'Масштаб 1:1'
                }
            });

            var g = new Highcharts.Map({
                chart: {
                    backgroundColor: "transparent",
                    renderTo: 'container'
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                legend: {
                    enabled: false
                },
                mapNavigation: {
                    enabled: true,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    },
            		enableMouseWheelZoom: false
                },
                colorAxis: {
                    min: min,
                    max: max,
                    dataClasses: [{
                        color: "#50CD8E",
                        to: 0.5
                    }, {
                        color: "#E7B44E",
                        from: 0.5,
                        to: 1
                    }, {
                        color: "#E15935",
                        from: 1,
                        to: 1.5
                    }, {
                        color: "#C33412",
                        from: 1.5,
                        to: 2
                    }, {
                        color: "#A40A00",
                        from: 2
                    }]
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    formatter: _tooltipFormatter
                },
                series: [{
                    data: data,
                    borderColor: "#FFFFFF",
                    mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
                    joinBy: 'hc-key',
                    name: ''
                }]
            });
        }
    })
});

function _tooltipFormatter(){
    var html = ''+
    '<div class="vich-modal">'+
        '<div class="vich-modal__text-wrapper">'+
            '<div class="vich-modal__name">'+this.point.name+'</div>'+
        '</div>'+
        '<div class="vich-modal__more-wrapper">'+
            '<div class="vich-modal__more">'+
                '<div class="vich-modal__more-value">Население &ndash; '+formatSumm(this.point.to_59_count)+' человек</div>';
    if(this.point['hc-key'] != 'ru-cr' && this.point['hc-key'] != 'ru-sev')
        html += ''+
                'Население в возрасте от 0 до 59 лет';
    html += ''+
            '</div>'+
            '<div class="vich-modal__more">'+
                '<div class="vich-modal__more-value">Носителей ВИЧ &ndash; '+formatSumm(this.point.vich_count)+' человек</div>';
    if(this.point['hc-key'] != 'ru-cr' && this.point['hc-key'] != 'ru-sev')
        html += ''+ String(this.point.value).replace('.', ',')+'% от населения в возрасте от 0 до 59 лет';
    else
        html += ''+ String(this.point.value).replace('.', ',')+'% от всего населения';
    html += ''+
            '</div>'+
        '</div>'+
        '<a href="#" class="vich-modal__close">&#215;</a>'+
    '</div>';
    return html;
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000 > 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 > 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}