var i_data=[];
var map_data_zp=[];
var map_data_pen=[];
var data_people=[];

var global_type = 'ZP';

$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);


            for(var i = 0; i<i_data.length; i++) {
                var summ = parseInt(i_data[i]['total_amount_of_spending_1_month_middle_salary']);
                if(i_data[i].is_pie_map == "1") {
                    map_data_zp[map_data_zp.length] = {
                        'lat': i_data[i].lat,
                        'lon': i_data[i].lng,
                        'name': i_data[i].city,
                        'z': parseInt(i_data[i]['avg_zp']),
                        'values': {
                            'summ': summ,
                            'zp': parseInt(i_data[i]['avg_zp']),
                            'pensiya': parseInt(i_data[i]['avg_penciya']),
                            'region': i_data[i].region,
                            'population': i_data[i].people_count
                        }
                    };

                    map_data_pen[map_data_pen.length] = {
                        'lat': i_data[i].lat,
                        'lon': i_data[i].lng,
                        'name': i_data[i].city,
                        'z': parseInt(i_data[i]['avg_penciya']),
                        'values': {
                            'summ': summ,
                            'zp': parseInt(i_data[i]['avg_zp']),
                            'pensiya': parseInt(i_data[i]['avg_penciya']),
                            'region': i_data[i].region,
                            'population': i_data[i].people_count
                        }
                    };
                }

                data_people[i] = {
                    'region': i_data[i].region,
                    'name': i_data[i].city,
                    'summ': summ,
                    'avg_zp': i_data[i].avg_zp,
                    'avg_pensiya': i_data[i].avg_penciya,
                    'population': i_data[i].people_count
                }
            }

            data_people = data_people.sort(function(first, second) {
                if(parseInt(first.population) < parseInt(second.population))
                    return 1;
                if(parseInt(second.population) < parseInt(first.population))
                    return -1;
                return 0;
            });

            $('.min-costs__graphic').remove();
            var max_width = $('.min-costs__graphics').innerWidth() - 245;
            var max_count = parseInt(data_people[0].population);
            for(var i = 0; i < data_people.length; i ++) {
                var this_width = max_width * (parseInt(data_people[i].population) / max_count);
                $('.min-costs__graphics').append('' +
                    '<div class="min-costs__graphic">' +
                        '<div class="min-costs__graphic-name">'+data_people[i].name+'</div>' +
                        '<div class="min-costs__graphic-value" ' +
                        'data-percent="'+(parseInt(data_people[i].population) / max_count)+'" '+
                        'style="width: '+this_width+'px;"' +
                        'data-name="'+data_people[i].name+'"' +
                        'data-region="'+data_people[i].region+'"' +
                        'data-value="'+data_people[i].summ+'"' +
                        'data-zp="'+data_people[i].avg_zp+'"' +
                        'data-pen="'+data_people[i].avg_pensiya+'"' +
                        'data-population="'+data_people[i].population+'"' +
                        '></div>' +
                        '<div class="min-costs__graphic-count">'+parseInt(data_people[i].population).formatMoney(0, ',', ' ')+'</div>' +
                    '</div>');
            }

            $(function () {
                var highChartsSettings = {
                    chart: {
                        renderTo: 'container'
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        enabled: false
                        //valueDecimals: 0
                    },
                    mapNavigation: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [
                        {
                            data: [],
                            mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
                            joinBy: 'hc-key',
                            name: '',
                            borderWidth: 1,
                            borderColor: '#fff',
                            nullColor: "#EDEDED"
                        },
                        {
                            type: "mapbubble",
                            color: 'transparent',
                            data: map_data_zp,
                            dataLabels: {
                                enabled: true,
                                borderRadius: 500,
                                useHTML: true,
                                formatter: _labelFormatter
                            },
                            events: {
                                mouseOver: "return false;"
                            },
                            maxSize: '10%',
                            minSize: '4%'
                        }
                    ]
                };

                Highcharts.setOptions({
                    global: {
                        timezoneOffset: 3 * -60 // minutes
                    },
                    lang: {
                        contextButtonTitle: 'Контекстное меню',
                        decimalPoint: '.',
                        downloadJPEG: 'Экспорт в формате JPEG',
                        downloadPDF: 'Экспорт в формате PDF',
                        downloadPNG: 'Экспорт в формате PNG',
                        downloadSVG: 'Экспорт в формате SVG',
                        loading: 'Загрузка...',
                        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        shortMonths: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                        weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                        numericSymbols: ['тыс', 'млн', 'млр', 'трл', 'P', 'E'],
                        printChart: 'На печать',
                        rangeSelectorFrom: 'От',
                        rangeSelectorTo: 'До',
                        rangeSelectorZoom: 'Масштаб',
                        resetZoom: 'Масштаб по-умолчанию',
                        resetZoomTitle: 'Масштаб 1:1',
                    }
                });

                map = new Highcharts.Map(highChartsSettings);

            });
        }
    })
});

$(window).resize(function() {
    if($('.min-costs__graphics').width() > 640 && $('.min-costs__graphics').width() < 750) {
        var max_width = parseInt($('.min-costs__graphics').innerWidth()) - 245;
        $('.min-costs__graphic').each(function() {
            $(this).find('.min-costs__graphic-value').css('width', $(this).data('percent')*max_width+'px');
        });
    }
});

$('.choosers__item').click(function() {
    if(!$(this).hasClass('choosers__item_active')) {
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
        if($(this).data('sort') == 'zp') {
            var param_data = map_data_zp;
            $('.min-costs__legend-name-salary').html('зарплата (за исключением минимальных расходов)');
            global_type = 'ZP';
        } else {
            var param_data = map_data_pen;
            $('.min-costs__legend-name-salary').html('пенсия (за исключением минимальных расходов)');
            global_type = 'PEN';
        }

        var highChartsSettings = {
            chart: {
                renderTo: 'container'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            legend: {
                enabled: false
                //valueDecimals: 0
            },
            mapNavigation: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [
                {
                    data: [],
                    mapData: Highcharts.maps['countries/ru/custom/ru-all-rbc'],
                    joinBy: 'hc-key',
                    name: '',
                    borderWidth: 1,
                    borderColor: '#fff',
                    nullColor: "#EDEDED"
                },
                {
                    type: "mapbubble",
                    color: 'transparent',
                    data: param_data,
                    dataLabels: {
                        enabled: true,
                        borderRadius: 500,
                        useHTML: true,
                        formatter: _labelFormatter
                    },
                    events: {
                        mouseOver: "return false;"
                    },
                    maxSize: '10%',
                    minSize: '4%'
                }
            ]
        };

        map = new Highcharts.Map(highChartsSettings);
    }
    return false;
});

function _labelFormatter() {
    var percent = this.point.values.summ / this.point.z;
    var gradus = Math.round(360 * percent);
    if(gradus > 360)
        gradus = 360;
    if(percent > 0.5) {
        opacity_filler = 1;
        opacity_mask = 0;
    } else {
        opacity_filler = 0;
        opacity_mask = 1;
    }
    var html = '<div class="wrapper" id="'+this.key+'" ' +
        'style="z-index: 100; width: '+(this.point.dlBox.width+2)+'px; height: '+(this.point.dlBox.width+2)+'px; "' +
        'data-name="'+this.point.name+'"' +
        'data-region="'+this.point.values.region+'"' +
        'data-value="'+this.point.values.summ+'"' +
        'data-zp="'+this.point.values.zp+'"' +
        'data-pen="'+this.point.values.pensiya+'"' +
        'data-population="'+this.point.values.population+'"' +
        '>' +
        '<div class="pie spinner" style="transform: rotate('+gradus+'deg);"></div>' +
        '<div class="pie filler" style="opacity: '+opacity_filler+';"></div>' +
        '<div class="mask" style="opacity: '+opacity_mask+';"></div>' +
        '</div>';
    return html;
}

$(document).on('mouseover', '.wrapper, .min-costs__graphic-value', function() {
    /*var coords1 = $(this).parent().parent().position();*/
    var coords = $(this).offset();
    $('.costs-modal__name').html($(this).data('name'));
    $('.costs-modal__region').html($(this).data('region'));
    $('.costs-modal__more-wrapper').empty();
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Численность населения <div class="costs-modal__more-item-value">'+$(this).data('population').formatMoney(0, ',', ' ')+' человек</div></div>');
    if(global_type == "ZP") {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя заработная плата <div class="costs-modal__more-item-value">' + $(this).data('zp').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        var glob_zar = $(this).data('zp');
        var word = 'зарплате';
    }
    else if(global_type == "PEN") {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя пенсия <div class="costs-modal__more-item-value">' + $(this).data('pen').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        var glob_zar = $(this).data('pen');
        var word = 'пенсии';
    }
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Сумма минимальных расходов в месяц <div class="costs-modal__more-item-value">'+$(this).data('value').formatMoney(0, ',', ' ')+' руб.</div></div>');
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Отношение минимальных расходов к средней '+word+' в месяц <div class="costs-modal__more-item-value">'+(parseFloat($(this).data('value'))/parseFloat(glob_zar) * 100).formatMoney(2, ',', ' ')+' %</div></div>');
    if(coords.left + $('.costs-modal').width() > $('.min-costs__graphics').width()) {

        $('.costs-modal').css({'top': coords.top, 'left': coords.left - $('.costs-modal').width() - 20}).show();
    } else if(coords.top + $('.costs-modal').height() > $('body').height()) {
        $('.costs-modal').css({'top': coords.top - $('.costs-modal').height(), 'left': coords.left+40}).show();
    } else {
        $('.costs-modal').css({'top': coords.top, 'left': coords.left+40}).show();
    }
});

$(document).on('mouseout', '.wrapper, .min-costs__graphic-value', function() {
    $('.costs-modal').hide();
});

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};