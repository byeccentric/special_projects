var i_data = [];
var data = [];

$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);

            console.log(i_data);

            for(var i = 0; i<i_data.length; i++) {
                data[i] = {
                    'avg_zp': i_data[i]['avg_zp'],
                    'below_zp': i_data[i]['salary_below_the_avg'],
                    'two_zp': i_data[i]['salary_two_people'],
                    'pensiya': i_data[i]['avg_penciya'],
                    'spend_avg': i_data[i].total_amount_of_spending_1_month_middle_salary,
                    'spend_below': i_data[i].total_amount_of_spending_1_month_below_the_avg_salary,
                    'spend_pens': i_data[i].total_family_income_of_pensioner,
                    'spend_family': i_data[i].total_family_income_of_two_people_and_children,

                    'region': i_data[i].region,
                    'population': i_data[i].people_count,
                    'name': i_data[i].city
                };
            }

            $('.month-costs__graphic').remove();
            for(var i = 0; i < data.length; i ++) {
                var this_max = 0;
                if(parseInt(data[i].avg_zp) > parseInt(data[i].two_zp)) {
                    if(parseInt(data[i].avg_zp) > parseInt(data[i].pensiya)) {
                        this_max = data[i].avg_zp;
                    } else {
                        this_max = data[i].pensiya;
                    }
                } else {
                    this_max = data[i].two_zp;
                }
                $('.month-costs__graphics').append('<div class="month-costs__graphic">' +
                    '<div class="month-costs__graphic-item" ' +
                    'style="height: '+Math.round(parseInt(data[i].below_zp) / this_max * 50)+'px;">' +
                    '<div class="month-costs__graphic-upline month-costs__graphic-under" ' +
                    'style="height: '+Math.round((parseInt(data[i].below_zp) - parseInt(data[i].spend_below)) / this_max * 50)+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-below="'+data[i].below_zp+'"' +
                    'data-spend_below="'+data[i].spend_below+'"' +
                    'data-population="'+data[i].population+'"' +
                    '></div>' +
                    '<div class="month-costs__graphic-downline" ' +
                    'style="height: '+Math.round(parseInt(data[i].spend_below) / this_max * 50)+'px;' +
                    '"></div>' +
                    '</div>' +
                    '<div class="month-costs__graphic-item" ' +
                    'style="height: '+Math.round(parseInt(data[i].avg_zp) / this_max * 50)+'px;">' +
                    '<div class="month-costs__graphic-upline month-costs__graphic-average" ' +
                    'style="height: '+Math.round((parseInt(data[i].avg_zp) - parseInt(data[i].spend_avg)) / this_max * 50)+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg="'+data[i].avg_zp+'"' +
                    'data-spend_avg="'+data[i].spend_avg+'"' +
                    'data-population="'+data[i].population+'"' +
                    '></div>' +
                    '<div class="month-costs__graphic-downline" ' +
                    'style="height: '+Math.round(parseInt(data[i].spend_avg) / this_max * 50)+'px;' +
                    '"></div>' +
                    '</div>' +
                    '<div class="month-costs__graphic-item" ' +
                    'style="height: '+Math.round(parseInt(data[i].pensiya) / this_max * 50)+'px;">' +
                    '<div class="month-costs__graphic-upline month-costs__graphic-pensioner" ' +
                    'style="height: '+Math.round((parseInt(data[i].pensiya) - parseInt(data[i].spend_avg)) / this_max * 50)+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-pensiya="'+data[i].pensiya+'"' +
                    'data-spend_avg="'+data[i].spend_pens+'"' +
                    'data-population="'+data[i].population+'"' +
                    '></div>' +
                    '<div class="month-costs__graphic-downline" ' +
                    'style="height: '+Math.round(parseInt(data[i].spend_pens) / this_max * 50)+'px;' +
                    '"></div>' +
                    '</div>' +
                    '<div class="month-costs__graphic-item" ' +
                    'style="height: '+Math.round(parseInt(data[i].two_zp) / this_max * 50)+'px;">' +
                    '<div class="month-costs__graphic-upline month-costs__graphic-family" ' +
                    'style="height: '+Math.round((parseInt(data[i].two_zp) - parseInt(data[i].spend_family)) / this_max * 50)+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-two="'+data[i].two_zp+'"' +
                    'data-spend_avg="'+data[i].spend_family+'"' +
                    'data-population="'+data[i].population+'"' +
                    '></div>' +
                    '<div class="month-costs__graphic-downline" ' +
                    'style="height: '+Math.round(parseInt(data[i].spend_family) / this_max * 50)+'px;' +
                    '"></div>' +
                    '</div>' +
                    '<div class="month-costs__graphic-name">'+data[i].name+'</div>' +
                    '</div>');
            }
        }
    })
});

$(document).on('mouseover', '.month-costs__graphic-upline', function() {
    var coords = $(this).offset();

    $('.costs-modal__region').html($(this).data('name')+', '+$(this).data('region'));
    $('.costs-modal__more-wrapper').empty();
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Численность населения <div class="costs-modal__more-item-value">'+$(this).data('population').formatMoney(0, ',', ' ')+' человек</div></div>');
    if($(this).hasClass('month-costs__graphic-family')) {
        $('.costs-modal__name').html('Семья из двух человек с ребенком');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Средняя заработная плата <div class="costs-modal__more-item-value">' + $(this).data('two').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></div>');
    } else if ($(this).hasClass('month-costs__graphic-pensioner')) {
        $('.costs-modal__name').html('Пенсионер');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Средняя пенсия <div class="costs-modal__more-item-value">' + $(this).data('pensiya').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></div>');
    } else if ($(this).hasClass('month-costs__graphic-average')) {
        $('.costs-modal__name').html('Человек со средней зарплатой (по Росстату)');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Средняя заработная плата <div class="costs-modal__more-item-value">' + $(this).data('avg').formatMoney(0, ',', ' ') + ' Руб.</div></b></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></div>');
    } else {
        $('.costs-modal__name').html('Человек с зарплатой ниже средней');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Заработная плата ниже средней <div class="costs-modal__more-item-value">' + $(this).data('below').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).data('spend_below').formatMoney(0, ',', ' ') + ' руб.</div></div>');
    }
    if(coords.left + $('.costs-modal').width() > $('body').width()) {
        if(coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.left - $('.costs-modal').width() < 0) {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
            }
        } else {
            if (coords.left - $('.costs-modal').width() < 0) {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top + $('.costs-modal').height() / 4,
                        'left': coords.left - $('.costs-modal').width() / 2
                    }).show();
                } else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height() / 2,
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
            }
        }
    } else {
        if (coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.top - $('.costs-modal').height() < 0) {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height() / 2,
                    'left': coords.left + $(this).width()
                }).show();
            } else {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height(),
                    'left': coords.left + $(this).width()
                }).show();
            }
        } else {
            $('.costs-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left + $(this).width()
            }).show();
        }
    }
});

$(document).on('mouseover', '.month-costs__graphic-downline', function() {
    var coords = $(this).offset();
    $('.costs-modal__region').html($(this).prev().data('name')+', '+$(this).prev().data('region'));
    $('.costs-modal__more-wrapper').empty();
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Численность населения <div class="costs-modal__more-item-value">'+$(this).prev().data('population').formatMoney(0, ',', ' ')+' человек</div></div>');
    if($(this).prev().hasClass('month-costs__graphic-family')) {
        $('.costs-modal__name').html('Семья из двух человек с ребенком');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя заработная плата <div class="costs-modal__more-item-value">' + $(this).prev().data('two').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).prev().data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
    } else if ($(this).prev().hasClass('month-costs__graphic-pensioner')) {
        $('.costs-modal__name').html('Пенсионер');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя пенсия <div class="costs-modal__more-item-value">' + $(this).prev().data('pensiya').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).prev().data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
    } else if ($(this).prev().hasClass('month-costs__graphic-average')) {
        $('.costs-modal__name').html('Человек со средней зарплатой (по Росстату)');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя заработная плата <div class="costs-modal__more-item-value">' + $(this).prev().data('avg').formatMoney(0, ',', ' ') + ' Руб.</div></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).prev().data('spend_avg').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
    } else {
        $('.costs-modal__name').html('Человек с зарплатой ниже средней');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Заработная плата ниже средней <div class="costs-modal__more-item-value">' + $(this).prev().data('below').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><b>Сумма минимальных расходов <div class="costs-modal__more-item-value">' + $(this).prev().data('spend_below').formatMoney(0, ',', ' ') + ' руб.</div></b></div>');
    }
    if(coords.left + $('.costs-modal').width() > $('body').width()) {
        if(coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.left - $('.costs-modal').width() < 0) {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
            }
        } else {
            if (coords.left - $('.costs-modal').width() < 0) {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top + $('.costs-modal').height() / 4,
                        'left': coords.left - $('.costs-modal').width() / 2
                    }).show();
                } else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height() / 2,
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
            }
        }
    } else {
        if (coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.top - $('.costs-modal').height() < 0) {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height() / 2,
                    'left': coords.left + $(this).width()
                }).show();
            } else {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height(),
                    'left': coords.left + $(this).width()
                }).show();
            }
        } else {
            $('.costs-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left + $(this).width()
            }).show();
        }
    }
});

$(document).on('mouseout', '.month-costs__graphic-upline, .month-costs__graphic-downline', function() {
    $('.costs-modal').hide();
});

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};