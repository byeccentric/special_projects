var i_data = [];
var data = {};
var maximum = {};

$(document).ready(function() {
    $.ajax({
        url: 'data_prod.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);

            for (var i = 0; i < i_data.length; i++) {
                if(!data.hasOwnProperty(i_data[i].product_id)) {
                    data[i_data[i].product_id] = {
                        "name": i_data[i].product_name,
                        "name2": i_data[i].product_name_2,
                        "cities": []
                    }
                    maximum[i_data[i].product_id] = 0;
                }
                data[i_data[i].product_id].cities[data[i_data[i].product_id].cities.length] = {
                    "price_rosstat": i_data[i].price_rosstat,
                    "price_research": i_data[i].price_research,
                    "city": i_data[i].city,
                    "region": i_data[i].region
                };
                if(parseFloat(i_data[i].price_research) > parseFloat(maximum[i_data[i].product_id])) {
                    maximum[i_data[i].product_id] = i_data[i].price_research;
                }
                if(parseFloat(i_data[i].price_rosstat) > parseFloat(maximum[i_data[i].product_id])) {
                    maximum[i_data[i].product_id] = i_data[i].price_rosstat;
                }
            }

            var counter = 0;
            $('.products-costs__select-list').empty();
            for(var i in data) {
                $('.products-costs__select-list').append('' +
                    '<div class="products-costs__select-item'+(counter==0 ? " products-costs__select-item" : "")+'"' +
                    'data-id="'+i+'"'+
                    '>'+data[i].name2+'</div>'
                );
                if(counter == 0) {
                    $('.products-costs__select-value').html(data[i].name2);
                    buildDataById(i);
                }
                counter++;
            }
        }
    })
});

function buildDataById(id) {
    $('.products-costs__graphics').empty();
    for (var i = 0; i < data[id].cities.length; i ++) {
        var rosstat_width = 0;
        var research_width = 0;
        var rosstat_style = '';
        var real_style = '';
        if(data[id].cities[i].price_rosstat == "-" || data[id].cities[i].price_rosstat == "Нет данных") {
            data[id].cities[i].price_rosstat = 'Нет данных';
            rosstat_style = ' style="width: 130px; margin-left: 0;"';
            rosstat_width = 0;
            research_width = Math.round(80 * (parseInt(data[id].cities[i].price_research) / parseInt(maximum[id])));
        }
        else if(data[id].cities[i].price_research == "-" || data[id].cities[i].price_research == "Нет данных") {
            data[id].cities[i].price_research = 'Нет данных';
            real_style = ' style="width: 130px; margin-left: 0;"';
            research_width = 0;
            rosstat_width = Math.round(80 * (parseInt(data[id].cities[i].price_rosstat) / parseInt(maximum[id])));
        } else {
            research_width = Math.round(80 * (parseInt(data[id].cities[i].price_research) / parseInt(maximum[id])));
            rosstat_width = Math.round(80 * (parseInt(data[id].cities[i].price_rosstat) / parseInt(maximum[id])));
        }

        $('.products-costs__graphics').append(''+
            '<div class="products-costs__graphic">' +
                '<div class="products-costs__graphic-rosstat" ' +
                'data-name="'+data[id].cities[i].city+'"'+
                'data-region="'+data[id].cities[i].region+'"'+
                'data-label="'+data[id].name+'"'+
                'data-rosstat="'+data[id].cities[i].price_rosstat+'"'+
                'data-research="'+data[id].cities[i].price_research+'"'+
                'style="width: '+rosstat_width+'px;"></div>'+
                '<div class="products-costs__graphic-rosstat-count"'+rosstat_style+'>'+data[id].cities[i].price_rosstat+'</div><br/>' +
                '<div class="products-costs__graphic-real" ' +
                'data-name="'+data[id].cities[i].city+'"'+
                'data-region="'+data[id].cities[i].region+'"'+
                'data-label="'+data[id].name+'"'+
                'data-rosstat="'+data[id].cities[i].price_rosstat+'"'+
                'data-research="'+data[id].cities[i].price_research+'"'+
                'style="width: '+research_width+'px;"></div>'+
                '<div class="products-costs__graphic-real-count"'+real_style+'>'+data[id].cities[i].price_research+'</div><br/>'+
                '<div class="products-costs__graphic-name">'+data[id].cities[i].city+'</div>'+
            '</div>'
        );
    }
}

$(document).on('click', '.products-costs__select-value', function() {
    $('.products-costs__select-list').slideToggle();
});

$(document).on('click', '.products-costs__select-item', function() {
    $('.products-costs__select-item').removeClass('products-costs__select-item_active');
    $(this).addClass('products-costs__select-item_active');
    $('.products-costs__select-value').html($(this).html());
    $('.products-costs__select-list').slideUp();
    buildDataById($(this).data('id'));
});

$(document).on('click', function(evt) {
    if(evt.target.className != "products-costs__select" && evt.target.className != "products-costs__select-value") {
        $('.products-costs__select-list').slideUp();
    }
});

$(document).on('mouseover', '.products-costs__graphic-rosstat, .products-costs__graphic-real', function() {
    var coords = $(this).offset();
    $('.costs-modal__name').html($(this).data('name'));
    $('.costs-modal__region').html($(this).data('region'));
    $('.costs-modal__more-wrapper').empty();
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">'+ $(this).data('label') +
        '<div class="costs-modal__more-item-value">данные Росстата: ' + $(this).data('rosstat') + ' руб.<br/>' +
        ' данные ЦЭПР: ' +$(this).data('research')+' '+($(this).data('research') != "Нет данных" ? "руб." : "")+'</div></div>');
    $('.costs-modal').css({'top': coords.top, 'left': coords.left+$(this).width()}).show();
    if(coords.left + $('.costs-modal').width() > $('body').width()) {
        if(coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.left - $('.costs-modal').width() < 0) {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
            }
        } else {
            if (coords.left - $('.costs-modal').width() < 0) {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top + $('.costs-modal').height() / 4,
                        'left': coords.left - $('.costs-modal').width() / 2
                    }).show();
                } else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height() / 2,
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
            }
        }
    } else {
        if (coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.top - $('.costs-modal').height() < 0) {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height() / 2,
                    'left': coords.left + $(this).width()
                }).show();
            } else {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height(),
                    'left': coords.left + $(this).width()
                }).show();
            }
        } else {
            $('.costs-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left + $(this).width()
            }).show();
        }
    }
});

$(document).on('mouseout', '.products-costs__graphic-rosstat, .products-costs__graphic-real', function() {
    $('.costs-modal').hide();
});
