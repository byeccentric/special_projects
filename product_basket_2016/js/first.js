var i_data = [];
var data = [];

$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            i_data = JSON.parse(items.responseText);

            max_food = 0;
            max_drugs = 0;
            max_nonfood = 0;
            max_ticket = 0;
            max_fuel = 0;
            for(var i = 0; i<i_data.length; i++) {
                data[i] = {
                    'avg_zp': i_data[i]['avg_zp'],
                    'food': i_data[i]['cheapest_set_the_price_products'],
                    'drugs': i_data[i]['1_month_price_medical'],
                    'nonfood': i_data[i]['1_month_price_non_food_products'],
                    'ticket': i_data[i]['1_month_price_transport'],
                    'fuel': i_data[i]['1_month_price_petrol'],
                    'pertol': i_data[i]['price_petrol_92_comments'],
                    'food_comment': i_data[i]['cheapest_set_the_price_products_comments'],

                    'region': i_data[i].region,
                    'population': i_data[i].people_count,
                    'name': i_data[i].city
                };
                if(parseInt(i_data[i]['cheapest_set_the_price_products']) > max_food) {
                    max_food = parseInt(i_data[i]['cheapest_set_the_price_products']);
                }
                if(parseInt(i_data[i]['1_month_price_medical']) > max_drugs) {
                    max_drugs = parseInt(i_data[i]['1_month_price_medical']);
                }
                if(parseInt(i_data[i]['1_month_price_non_food_products']) > max_nonfood) {
                    max_nonfood = parseInt(i_data[i]['1_month_price_non_food_products']);
                }
                if(parseInt(i_data[i]['1_month_price_transport']) > max_ticket) {
                    max_ticket = parseInt(i_data[i]['1_month_price_transport']);
                }
                if(parseInt(i_data[i]['1_month_price_petrol']) > max_fuel) {
                    max_fuel = parseInt(i_data[i]['1_month_price_petrol']);
                }
            }

            var max_width = $('.base-goods__graphics').innerWidth() - 225;
            var food_width = max_width * 0.29;
            var drugs_width = max_width * 0.25;
            var nonfood_width = max_width * 0.18;
            var ticket_width = max_width * 0.15;
            var fuel_width = max_width * 0.13;
            $('.base-goods__graphic').remove();
            $('.base-goods__graphics').append('<div class="base-goods__graphic">' +
                '<div class="base-goods__graphic-name"></div>' +
                '<div class="base-goods__graphic-item" style="width: '+food_width+'px;">' +
                '<div class="base-goods__graphic-header" data-tip="Базовый продовольственный набор - список товаров, включенных в стандартную закупку продуктов по минимальным ценам">' +
                'Базовый продовольственный набор'+
            '</div>' +
            '</div>' +
            '<div class="base-goods__graphic-item"  style="width: '+drugs_width+'px;">' +
                '<div class="base-goods__graphic-header" data-tip="Базовый набор лекарств - корзина из 10 популярных среди россиян лекарств, например, «Арбидол», «Кагоцел», «Эссенциале», «Нурофен», «Валидол» и др.">' +
                'Базовый набор лекарств' +
            '</div>' +
            '</div>' +
            '<div class="base-goods__graphic-item" style="width: '+nonfood_width+'px;">' +
                '<div class="base-goods__graphic-header" data-tip="Базовый непродовольственный набор - список таких товаров как туалетное мыло, зубная паста, шампунь для волос, стиральный порошок">' +
                'Базовый непродоволь- ственный набор'+
            '</div>' +
            '</div>' +
            '<div class="base-goods__graphic-item" style="width: '+ticket_width+'px;">' +
                '<div class="base-goods__graphic-header" data-tip="">' +
                'Проезд' +
                '</div>' +
                '</div>' +
                '<div class="base-goods__graphic-item" style="width: '+fuel_width+'px;">' +
                '<div class="base-goods__graphic-header" data-tip="">'+
                'Бензин' +
                '</div>' +
                '</div>' +
                '</div>');
            for(var i = 0; i < data.length; i ++) {
                var tickett_width;
                var ticket_styles;
                if(data[i].ticket == "-") {
                    tickett_width = 0;
                    ticket_styles = ' style="width: '+ticket_width+'px; margin-left: 0;"';
                    data[i].ticket = 'Нет данных';
                } else {
                    tickett_width = (ticket_width-40)*(parseInt(data[i].ticket) / max_ticket);
                    ticket_styles = '';
                }
                $('.base-goods__graphics').append('<div class="base-goods__graphic-name">'+data[i].name+'</div>' +
                    '<div class="base-goods__graphic-item" style="width: '+food_width+'px;">' +
                    '<div class="base-goods__graphic-value base-goods__graphic-food" ' +
                    'style="width: '+((food_width-40)*(parseInt(data[i].food) / max_food)) +'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-population="'+data[i].population+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg_zp="'+data[i].avg_zp+'"' +
                    'data-comment="'+data[i].food_comment+'"' +
                    'data-value="'+data[i].food+'"' +
                    '></div>' +
                    '<div class="base-goods__graphic-count">'+data[i].food+'</div>' +
                    '</div>' +
                    '<div class="base-goods__graphic-item" style="width: '+drugs_width+'px;">' +
                    '<div class="base-goods__graphic-value base-goods__graphic-drugs" ' +
                    'style="width: '+((drugs_width-40)*(parseInt(data[i].drugs) / max_drugs)) +'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-population="'+data[i].population+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg_zp="'+data[i].avg_zp+'"' +
                    'data-value="'+data[i].drugs+'"' +
                    '></div>' +
                    '<div class="base-goods__graphic-count">'+data[i].drugs+'</div>' +
                    '</div>' +
                    '<div class="base-goods__graphic-item" style="width: '+nonfood_width+'px;">' +
                    '<div class="base-goods__graphic-value base-goods__graphic-nonfood" ' +
                    'style="width: '+((nonfood_width-40)*(parseInt(data[i].nonfood) / max_nonfood)) +'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-population="'+data[i].population+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg_zp="'+data[i].avg_zp+'"' +
                    'data-value="'+data[i].nonfood+'"' +
                    '></div>' +
                    '<div class="base-goods__graphic-count">'+data[i].nonfood+'</div>' +
                    '</div>' +
                    '<div class="base-goods__graphic-item" style="width: '+ticket_width+'px;">' +
                    '<div class="base-goods__graphic-value base-goods__graphic-ticket" ' +
                    'style="width: '+tickett_width+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-population="'+data[i].population+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg_zp="'+data[i].avg_zp+'"' +
                    'data-value="'+data[i].ticket+'"' +
                    '></div>' +
                    '<div class="base-goods__graphic-count"'+ticket_styles+'>'+data[i].ticket+'</div>' +
                    '</div>' +
                    '<div class="base-goods__graphic-item" style="width: '+fuel_width+'px;">' +
                    '<div class="base-goods__graphic-value base-goods__graphic-fuel" ' +
                    'style="width: '+((fuel_width-40)*(parseInt(data[i].fuel) / max_fuel))+'px;"' +
                    'data-name="'+data[i].name+'"' +
                    'data-population="'+data[i].population+'"' +
                    'data-region="'+data[i].region+'"' +
                    'data-avg_zp="'+data[i].avg_zp+'"' +
                    'data-value="'+data[i].fuel+'"' +
                    'data-comment="'+data[i].pertol+'"' +
                    '></div>' +
                    '<div class="base-goods__graphic-count">'+data[i].fuel+'</div>' +
                    '</div>'
                );
            }
        }
    })
});

$(document).on('mouseover', '.base-goods__graphic-value', function() {
    var coords = $(this).offset();
    $('.costs-modal__name').html($(this).data('name'));
    $('.costs-modal__region').html($(this).data('region'));
    $('.costs-modal__more-wrapper').empty();
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Численность населения <div class="costs-modal__more-item-value">'+$(this).data('population').formatMoney(0, ',', ' ')+' человек</div></div>');
    $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Средняя заработная плата <div class="costs-modal__more-item-value">'+$(this).data('avg_zp').formatMoney(0, ',', ' ')+' руб.</div></div>');
    if($(this).hasClass('base-goods__graphic-food')) {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Базовый продовольственный набор ' +
            '<div class="costs-modal__more-item-value">' + $(this).data('value').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        if($(this).data('comment') != '') {
            $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">'+$(this).data('comment')+'</div>');
        }
        /*$('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><br/>*Базовый продовольственный набор - список товаров, включенных в стандартную закупку продуктов по минимальным ценам');*/
    } else if ($(this).hasClass('base-goods__graphic-drugs')) {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Базовый набор лекарств ' +
            '<div class="costs-modal__more-item-value">' + $(this).data('value').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        /*$('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><br/>*Базовый набор лекарств - корзина из 10 популярных среди россиян лекарств, например, «Арбидол», «Кагоцел», «Эссенциале», «Нурофен», «Валидол» и др.');*/
    } else if ($(this).hasClass('base-goods__graphic-nonfood')) {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Базовый непродовольственный набор ' +
            '<div class="costs-modal__more-item-value">' + $(this).data('value').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        /*$('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item"><br/>*Базовый непродовольственный набор - список таких товаров как туалетное мыло, зубная паста, шампунь для волос, стиральный порошок ');*/
    } else if ($(this).hasClass('base-goods__graphic-ticket')) {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Проезд ' +
            '<div class="costs-modal__more-item-value">' + $(this).data('value').formatMoney(0, ',', ' ') + ' руб.</div></div>');
    } else {
        $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">Бензин ' +
            '<div class="costs-modal__more-item-value">' + $(this).data('value').formatMoney(0, ',', ' ') + ' руб.</div></div>');
        if($(this).data('comment') != '') {
            $('.costs-modal__more-wrapper').append('<div class="costs-modal__more-item">'+$(this).data('comment')+'</div>');
        }

    }
    if(coords.left + $('.costs-modal').width() > $('body').width()) {
        if(coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.left - $('.costs-modal').width() < 0) {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if(coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height()/2,
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()
                    }).show();
                }
            }
        } else {
            if (coords.left - $('.costs-modal').width() < 0) {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top + $('.costs-modal').height() / 4,
                        'left': coords.left - $('.costs-modal').width() / 2
                    }).show();
                } else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width()/2
                    }).show();
                }
            } else {
                if (coords.top - $('.costs-modal').height() < 0) {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height() / 2,
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
                else {
                    $('.costs-modal').css({
                        'top': coords.top - $('.costs-modal').height(),
                        'left': coords.left - $('.costs-modal').width() - 10
                    }).show();
                }
            }
        }
    } else {
        if (coords.top + $('.costs-modal').height() > $('body').height()) {
            if(coords.top - $('.costs-modal').height() < 0) {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height() / 2,
                    'left': coords.left + $(this).width()
                }).show();
            } else {
                $('.costs-modal').css({
                    'top': coords.top - $('.costs-modal').height(),
                    'left': coords.left + $(this).width()
                }).show();
            }
        } else {
            $('.costs-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left + $(this).width()
            }).show();
        }
    }
});

$(document).on('mouseout', '.base-goods__graphic-value', function() {
    $('.costs-modal').hide();
});

$(document).on('mouseover', '.base-goods__graphic-header', function() {
    $('.tips-modal__more-wrapper').empty();
    if($(this).data('tip') != '') {
        var coords = $(this).offset();
        $('.tips-modal__more-wrapper').append('<div class="costs-modal__more-item">'+$(this).data('tip')+'</div>');
        if (coords.left + $('.costs-modal').width() > $('body').width()) {
            $('.tips-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left - $(this).width() / 2
            }).show();
        } else {
            $('.tips-modal').css({
                'top': coords.top + $(this).height(),
                'left': coords.left - $(this).width() / 2
            }).show();
        }
    }
});

$(document).on('mouseout', '.base-goods__graphic-header', function() {
    $('.tips-modal').hide();
});


Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};