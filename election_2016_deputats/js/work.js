var color = {
    '14': '#226bc4',
    '1': '#ef463d',
    '7': '#509efa',
    '2': '#f39200',
    '12': '#B61818',
    '4': '#bd55bd',
    '16': '#cccccc'
};
var data = [];
var parts = {
    14: {
        'name': '«Единая Россия»',
        'count': 0,
        'items': []
    },
    1: {
        'name': 'КПРФ',
        'count': 0,
        'items': []
    },
    7: {
        'name': 'ЛДПР',
        'count': 0,
        'items': []
    },
    2: {
        'name': '«Справедливая Россия»​',
        'count': 0,
        'items': []
    },
    12: {
        'name': '«Родина»',
        'count': 0,
        'items': []
    },
    4: {
        'name': '«Гражданская платформа»',
        'count': 0,
        'items': []
    },
    16: {
        'name': 'Самовыдвижение',
        'count': 0,
        'items': []
    }
};

var work = $("#work")[0];
var biz = $("#biznes")[0];
    ctx     = '';

var global_type = 'biznes';

var works = [];
var biznes = [];
var the_matrix = [];
for(var i = -50; i < 200; i ++)
    the_matrix[i] = [];

var the_matrix2 = [];
for(var i = -50; i < 200; i ++)
    the_matrix2[i] = [];

var width = 20;
var sharik = 16;
if(parseInt($('html').css('width')) > 500) {
    var max_in_row = Math.floor((parseInt($('body').width()) - 25) / (2 * width));
    work.width = max_in_row * width * 2 + 25;
    biz.width = max_in_row * width * 2 + 25;
} else {
    var max_in_row = Math.floor(parseInt($('body').width()) / width);
    work.width = max_in_row * width;
    biz.width = max_in_row * width;
}

var texts = [];
var lines = [];
var circles = [];

$(window).resize(function() {
    if($(window).width() > 640) {
        the_matrix = [];
        for (var i = -50; i < 200; i++)
            the_matrix[i] = [];

        the_matrix2 = [];
        for (var i = -50; i < 200; i++)
            the_matrix2[i] = [];

        if (parseInt($('html').css('width')) > 500) {
            max_in_row = Math.floor((parseInt($('body').width()) - 25) / (2 * width));
            work.width = max_in_row * width * 2 + 25;
            biz.width = max_in_row * width * 2 + 25;
        } else {
            max_in_row = Math.floor(parseInt($('body').width()) / width);
            work.width = max_in_row * width;
            biz.width = max_in_row * width;
        }

        texts = [];
        lines = [];
        circles = [];

        if (global_type == 'work') {
            var work_init = 1;
            var biz_init = 0;
        } else {
            var work_init = 0;
            var biz_init = 1;
        }

        initWork(work_init);
        initBiznes(biz_init);
    }
});

$('.choosers__item').click(function() {
    if(!$(this).hasClass('choosers__item_active')) {
        if($(this).data('sort') == 'work') {
            global_type = 'work';
            initWork(1);
        } else {
            global_type = 'biznes';
            initBiznes(1);
        }
        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
    }
    return false;
});

$(document).ready(function() {
    $.ajax({
        url: 'data.json',
        dataType: 'json',
        cache: false,
        complete: function(deputates) {
            data = JSON.parse(deputates.responseText);

            for(var i = 0; i < data.length; i++) {
                data[i].inbox_money_summ = parseFloat(String(data[i].inbox_money_summ).replace(/ /ig, ''));
                data[i].bank_total_sum = parseFloat(String(data[i].bank_total_sum).replace(/ /ig, ''));
                parts[data[i].party_id].count += 1;
                parts[data[i].party_id].items[parts[data[i].party_id].items.length] = data[i];
            }

            data = data.sort( function(first, second) {
                if(parts[first.party_id].count > parts[second.party_id].count)
                    return -1;
                else if(parts[second.party_id].count > parts[first.party_id].count)
                    return 1;
                else {
                    if(first.party_name > second.party_name) {
                        return -1;
                    }
                    else {
                        return 1;
                    }
                }
            });

            if(global_type == 'work') {
                var work_init = 1;
                var biz_init = 0;
            } else {
                var work_init = 0;
                var biz_init = 1;
            }

            initWork(work_init);
            initBiznes(biz_init);
        }

    });
});

function initBiznes(u) {
    if(u) {
        $('#work').animate({'opacity': 0}, 600).css({'z-index': 4, 'display': 'none'});
        $('#biznes').animate({'opacity': 1}, 600).css({'z-index': 5, 'position': 'relative', 'display': 'block'});
    }
    ctx = biz.getContext('2d');

    biznes = [];
    for(var i = 0; i < data.length; i ++ ) {
        var found = 0;
        if(data[i].company_group_type_of_activity.length != 0) {
            for(var key = 0; key < data[i].company_group_type_of_activity.length; key ++) {
                if (biznes.length > 0) {
                    for (var j = 0; j < biznes.length; j++) {
                        if (biznes[j].name == data[i].company_group_type_of_activity[key]) {
                            biznes[j].count += 1;
                            biznes[j].items[biznes[j].items.length] = data[i];
                            found = 1;
                        }
                    }
                    if (found == 0) {
                        biznes[biznes.length] = {
                            'name': data[i].company_group_type_of_activity[key],
                            'count': 1,
                            'items': [data[i]]
                        };
                    }
                } else {
                    biznes[0] = {
                        'name': data[i].company_group_type_of_activity[key],
                        'count': 1,
                        'items': [data[i]]
                    };
                }
            }
        }
    }

    biznes = biznes.sort(function(first, second) {
        if(first.count > second.count)
            return -1;
        else if(first.count < second.count)
            return 1;
        else
            return 0;
    });

    var counter = 0;

    if($('body').innerWidth() > 500) {
        counter = 0;

        var drob = 0;
        var rows_max = 0;
        var left_in_row = 0;

        row1_x = 0;
        row1_y = 0;
        this_row1_y = 0;

        row2_x = 0;
        row2_y = 0;
        this_row2_y = 0;

        circles = [];
        texts = [];
        lines = [];

        the_matrix2 = [];
        for (var i = 0; i < 100; i++)
            the_matrix2[i] = [];

        this_id = 0;

        for (var i = 0; i < biznes.length; i++) {
            drob = biznes[i].items.length / max_in_row;
            rows_max = Math.floor(drob);
            left_in_row = Math.round((drob - rows_max) * max_in_row);
            ctx.font = "normal 16px ALSStory";

            if (counter % 2 == 0) {
                row1_x = 0;
                if (ctx.measureText(biznes[i].name).width > (max_in_row - 3) * width) {
                    this_text_id = texts.length;
                    texts[this_text_id] = new wrapText(biznes[i].name, 0, row1_y * width, (max_in_row - 3) * width, width);
                    row1_y += texts[this_text_id].cnt;

                } else {
                    texts[texts.length] = new textLeft(biznes[i].name, 0, row1_y * width, '#000000');
                    row1_y += 1;
                }
                texts[texts.length] = new counterRight(max_in_row * width, row1_y * width, biznes[i].count);
                lines[lines.length] = new line(0, (row1_y + 0.5) * width, max_in_row * width, (row1_y + 0.5) * width);
                row1_y += 1;
                this_row1_y = -1;
                for (var j = 0; j < biznes[i].items.length; j++) {
                    this_row1_y += 1;
                    this_id = circles.length;
                    circles[this_id] = new circle((row1_x + 0.5) * width, (this_row1_y + row1_y + 0.5) * width, sharik / 2, biznes[i].items[j].party_id);
                    circles[this_id].id = data.indexOf(biznes[i].items[j]);
                    the_matrix2[row1_x][this_row1_y + row1_y] = this_id;
                    if ((this_row1_y == rows_max && left_in_row > 0) || (this_row1_y == rows_max - 1 && left_in_row == 0)) {
                        if (this_row1_y == rows_max)
                            left_in_row -= 1;
                        row1_x += 1;
                        this_row1_y = -1;
                    }
                }
                row1_y += rows_max + 2;
            } else {
                row2_x = 0;
                if (ctx.measureText(biznes[i].name).width > (max_in_row - 2) * width) {
                    /*row2_y += Math.ceil(ctx.measureText(biznes[i].name).width / ((max_in_row - 2) * width)) - 1;*/
                    texts[texts.length] = new wrapText(biznes[i].name, (max_in_row + row2_x) * width + 25, row2_y * width, (max_in_row - 2) * width, width);
                    row2_y += Math.ceil(ctx.measureText(biznes[i].name).width / ((max_in_row - 2) * width)) + 1;
                } else {
                    texts[texts.length] = new textLeft(biznes[i].name, (max_in_row + row2_x) * width + 25, (row2_y) * width, '#000000');
                    row2_y += 1;
                }
                texts[texts.length] = new counterRight(max_in_row * width * 2 + 25, row2_y * width, biznes[i].count);
                lines[lines.length] = new line(max_in_row * width + 25, ( row2_y + 0.5) * width, max_in_row * width * 2 + 25, (row2_y + 0.5) * width);
                row2_y += 1;
                this_row2_y = -1;
                for (var j = 0; j < biznes[i].items.length; j++) {
                    this_row2_y += 1;
                    this_id = circles.length;
                    circles[this_id] = new circle((max_in_row + row2_x + 0.5) * width + 25, (row2_y + this_row2_y + 0.5) * width, sharik / 2, biznes[i].items[j].party_id);
                    circles[this_id].id = data.indexOf(biznes[i].items[j]);
                    /*data[data.indexOf(works[i].items[j])].circle = circles[circles.length-1];*/
                    the_matrix2[max_in_row + row2_x + 1][row2_y + this_row2_y] = this_id;
                    if ((this_row2_y == rows_max && left_in_row > 0) || (this_row2_y == rows_max - 1 && left_in_row == 0)) {
                        if (this_row2_y == rows_max)
                            left_in_row -= 1;
                        row2_x += 1;
                        this_row2_y = -1;
                    }
                }
                row2_y += rows_max + 2;
            }
            counter += 1;
        }
    } else {
        counter = 0;

        var drob = 0;
        var rows_max = 0;
        var left_in_row = 0;

        row1_x = 0;
        row1_y = 0;
        this_row1_y = 0;

        row2_x = 0;
        row2_y = -1;
        this_row2_y = 0;

        circles = [];
        texts = [];
        lines = [];

        the_matrix2 = [];
        for (var i = 0; i < 100; i++)
            the_matrix2[i] = [];

        this_id = 0;

        for (var i = 0; i < biznes.length; i++) {
            drob = biznes[i].items.length / max_in_row;
            rows_max = Math.floor(drob);
            left_in_row = Math.round((drob - rows_max) * max_in_row);
            ctx.font = "normal 16px ALSStory";

            row1_x = 0;
            if (ctx.measureText(biznes[i].name).width >= (max_in_row - 2) * width) {
                var this_text_id = texts.length;
                texts[this_text_id] = new wrapText(biznes[i].name, 0, row1_y * width, (max_in_row - 2) * width, width);
                row1_y += texts[this_text_id].cnt;
            } else {
                texts[texts.length] = new textLeft(biznes[i].name, 0, row1_y * width, '#000000');
                row1_y += 1;
            }
            texts[texts.length] = new counterRight(max_in_row * width, row1_y * width, biznes[i].count);
            lines[lines.length] = new line(0, (row1_y + 0.5) * width, max_in_row * width, (row1_y + 0.5) * width);
            row1_y += 1;
            this_row1_y = -1;
            for (var j = 0; j < biznes[i].items.length; j++) {
                this_row1_y += 1;
                this_id = circles.length;
                circles[this_id] = new circle((row1_x + 0.5) * width, (this_row1_y + row1_y + 0.5) * width, sharik / 2, biznes[i].items[j].party_id);
                circles[this_id].id = data.indexOf(biznes[i].items[j]);
                /*data[data.indexOf(works[i].items[j])].circle = circles[circles.length-1];*/
                /*the_matrix[row1_x][this_row1_y + row1_y] = data.indexOf(works[i].items[j]);*/
                the_matrix2[row1_x][this_row1_y + row1_y] = this_id;
                if ((this_row1_y == rows_max && left_in_row > 0) || (this_row1_y == rows_max - 1 && left_in_row == 0)) {
                    if (this_row1_y == rows_max)
                        left_in_row -= 1;
                    row1_x += 1;
                    this_row1_y = -1;
                }
            }
            row1_y += rows_max + 2;
        }
    }


    if (row1_y > row2_y)
        biz.height = row1_y * width;
    else
        biz.height = row2_y * width;

    if (window.devicePixelRatio) {
        var biznesCanvasWidth = $(biz).attr('width');
        var biznesCanvasHeight = $(biz).attr('height');
        var biznesCanvasCssWidth = biznesCanvasWidth;
        var biznesCanvasCssHeight = biznesCanvasHeight;

        $(biz).attr('width', biznesCanvasWidth * window.devicePixelRatio);
        $(biz).attr('height', biznesCanvasHeight * window.devicePixelRatio);
        $(biz).css('width', biznesCanvasCssWidth);
        $(biz).css('height', biznesCanvasCssHeight);
        ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
    }

    for(var i = 0; i < texts.length; i ++ ) {
        texts[i].draw();
    }
    for(var i = 0; i < lines.length; i ++ ) {
        lines[i].draw();
    }
    for(var i = 0; i < circles.length; i ++ ) {
        circles[i].draw();
    }
}

function initWork(u) {
    if(u) {
        $('#work').animate({'opacity': 1}, 600).css({'z-index': 5, 'position': 'relative', 'display': 'block'});
        $('#biznes').animate({'opacity': 0}, 600).css({'z-index': 4, 'position': 'absolute', 'display': 'none'});
    }
    ctx = work.getContext('2d');
    works = [];
    for(var i = 0; i < data.length; i ++ ) {
        var found = 0;
        if(data[i].cikrf_work_position_group != null && data[i].cikrf_work_position_group.length > 0) {
            if (works.length > 0) {
                for (var j = 0; j < works.length; j++) {
                    if (works[j].name == data[i].cikrf_work_position_group) {
                        works[j].count += 1;
                        works[j].items[works[j].items.length] = data[i];
                        found = 1;
                    }
                }
                if (found == 0) {
                    works[works.length] = {
                        'name': data[i].cikrf_work_position_group,
                        'count': 1,
                        'items': [data[i]]
                    };
                }
            } else {
                works[0] = {
                    'name': data[i].cikrf_work_position_group,
                    'count': 1,
                    'items': [data[i]]
                };
            }
        }
    }

    works = works.sort(function(first, second) {
        if(first.count > second.count)
            return -1;
        else if(first.count < second.count)
            return 1;
        else
            return 0;
    });

    var counter = 0;

    if($('body').innerWidth() > 500) {
        counter = 0;

        var drob = 0;
        var rows_max = 0;
        var left_in_row = 0;

        row1_x = 0;
        row1_y = 0;
        this_row1_y = 0;

        row2_x = 0;
        row2_y = 0;
        this_row2_y = 0;

        circles = [];
        texts = [];
        lines = [];

        for (var i = 0; i < works.length; i++) {
            drob = works[i].items.length / max_in_row;
            rows_max = Math.floor(drob);
            left_in_row = Math.round((drob - rows_max) * max_in_row);
            ctx.font = "normal 16px ALSStory";

            if (counter % 2 == 1 || counter == 2) {
                row2_x = 0;
                if (ctx.measureText(works[i].name).width > (max_in_row - 2) * width) {
                    this_text_id = texts.length;
                    texts[this_text_id] = new wrapText(works[i].name, (max_in_row + row2_x) * width + 25, row2_y * width, (max_in_row - 2) * width, width);
                    row2_y += texts[this_text_id].cnt;
                } else {
                    texts[texts.length] = new textLeft(works[i].name, (max_in_row + row2_x) * width + 25, row2_y * width, '#000000');
                    row2_y += 1;
                }
                texts[texts.length] = new counterRight(max_in_row * width * 2 + 25, row2_y * width, works[i].count);
                lines[lines.length] = new line(max_in_row * width + 25, ( row2_y + 0.5) * width, max_in_row * width * 2 + 25, (row2_y + 0.5) * width);
                row2_y += 1;
                this_row2_y = -1;
                for (var j = 0; j < works[i].items.length; j++) {
                    this_row2_y += 1;
                    circles[circles.length] = new circle((max_in_row + row2_x + 0.5) * width + 25, (row2_y + this_row2_y + 0.5) * width, sharik / 2, works[i].items[j].party_id);
                    data[data.indexOf(works[i].items[j])].circle = circles[circles.length - 1];
                    the_matrix[max_in_row + row2_x + 1][row2_y + this_row2_y] = data.indexOf(works[i].items[j]);
                    if ((this_row2_y == rows_max && left_in_row > 0) || (this_row2_y == rows_max - 1 && left_in_row == 0)) {
                        if (this_row2_y == rows_max)
                            left_in_row -= 1;
                        row2_x += 1;
                        this_row2_y = -1;
                    }
                }
                row2_y += rows_max + 2;
            } else {
                row1_x = 0;
                if (ctx.measureText(works[i].name).width > (max_in_row - 2) * width) {
                    this_text_id = texts.length;
                    texts[this_text_id] = new wrapText(works[i].name, 0, row1_y * width, (max_in_row - 2) * width, width);
                    row1_y += texts[this_text_id].cnt;
                } else {
                    texts[texts.length] = new textLeft(works[i].name, 0, row1_y * width, '#000000');
                    row1_y += 1;
                }
                texts[texts.length] = new counterRight(max_in_row * width, row1_y * width, works[i].count);
                lines[lines.length] = new line(0, (row1_y + 0.5) * width, max_in_row * width, (row1_y + 0.5) * width);
                row1_y += 1;
                this_row1_y = -1;
                for (var j = 0; j < works[i].items.length; j++) {
                    this_row1_y += 1;
                    circles[circles.length] = new circle((row1_x + 0.5) * width, (this_row1_y + row1_y + 0.5) * width, sharik / 2, works[i].items[j].party_id);
                    data[data.indexOf(works[i].items[j])].circle = circles[circles.length - 1];
                    the_matrix[row1_x][this_row1_y + row1_y] = data.indexOf(works[i].items[j]);
                    if ((this_row1_y == rows_max && left_in_row > 0) || (this_row1_y == rows_max - 1 && left_in_row == 0)) {
                        if (this_row1_y == rows_max)
                            left_in_row -= 1;
                        row1_x += 1;
                        this_row1_y = -1;
                    }
                }
                row1_y += rows_max + 2;
            }
            counter += 1;
        }
    } else {
        counter = 0;

        var drob = 0;
        var rows_max = 0;
        var left_in_row = 0;

        row1_x = 0;
        row1_y = 0;
        this_row1_y = 0;

        row2_x = 0;
        row2_y = 0;
        this_row2_y = 0;

        circles = [];
        texts = [];
        lines = [];

        for (var i = 0; i < works.length; i++) {
            drob = works[i].items.length / max_in_row;
            rows_max = Math.floor(drob);
            left_in_row = Math.round((drob - rows_max) * max_in_row);
            ctx.font = "normal 16px ALSStory";

            row1_x = 0;
            if (ctx.measureText(works[i].name).width >= (max_in_row - 2) * width) {
                var this_text_id = texts.length;
                texts[this_text_id] = new wrapText(works[i].name, 0, row1_y * width, (max_in_row - 2) * width, width);
                row1_y += texts[this_text_id].cnt;
            } else {
                texts[texts.length] = new textLeft(works[i].name, 0, row1_y * width, '#000000');
                row1_y += 1;
            }
            texts[texts.length] = new counterRight(max_in_row * width, row1_y * width, works[i].count);
            lines[lines.length] = new line(0, (row1_y + 0.5) * width, max_in_row * width, (row1_y + 0.5) * width);
            row1_y += 1;
            this_row1_y = -1;
            for (var j = 0; j < works[i].items.length; j++) {
                this_row1_y += 1;
                circles[circles.length] = new circle((row1_x + 0.5) * width, (this_row1_y + row1_y + 0.5) * width, sharik / 2, works[i].items[j].party_id);
                data[data.indexOf(works[i].items[j])].circle = circles[circles.length - 1];
                the_matrix[row1_x][this_row1_y + row1_y] = data.indexOf(works[i].items[j]);
                if ((this_row1_y == rows_max && left_in_row > 0) || (this_row1_y == rows_max - 1 && left_in_row == 0)) {
                    if (this_row1_y == rows_max)
                        left_in_row -= 1;
                    row1_x += 1;
                    this_row1_y = -1;
                }
            }
            row1_y += rows_max + 2;
        }
    }

    if (row1_y > row2_y)
        work.height = row1_y * width;
    else
        work.height = row2_y * width;

    if (window.devicePixelRatio) {
        var workCanvasWidth = $(work).attr('width');
        var workCanvasHeight = $(work).attr('height');
        var workCanvasCssWidth = workCanvasWidth;
        var workCanvasCssHeight = workCanvasHeight;

        $(work).attr('width', workCanvasWidth * window.devicePixelRatio);
        $(work).attr('height', workCanvasHeight * window.devicePixelRatio);
        $(work).css('width', workCanvasCssWidth);
        $(work).css('height', workCanvasCssHeight);
        ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
    }

    for(var i = 0; i < texts.length; i ++ ) {
        texts[i].draw();
    }
    for(var i = 0; i < lines.length; i ++ ) {
        lines[i].draw();
    }
    for(var i = 0; i < circles.length; i ++ ) {
        circles[i].draw();
    }
}

var last_circle = {};
var xxx = -1;
var yyy = -1;

work.addEventListener('mousemove', function(evt) {
    var mousePos = getMousePos(work, evt);
    var xx = Math.floor(mousePos.x / width);
    if(xx > 19)
        xx = Math.floor((mousePos.x - 5) / width);
    var yy = Math.floor(mousePos.y / width);
    if(the_matrix[xx][yy] != undefined) {
        if(data[the_matrix[xx][yy]] != last_circle && (xxx != xx || yyy != yy)) {
            if(last_circle.hasOwnProperty('x'))
                last_circle.reDraw();
            if(data[the_matrix[xx][yy]].img == 1)
                $('.duma-modal__image').html('<img onerror="imgError(this);" src="images/deputats/'+data[the_matrix[xx][yy]].img_file+'" />').css('background', color[data[the_matrix[xx][yy]].party_id]);
            else
                $('.duma-modal__image').html('').css('background', color[data[the_matrix[xx][yy]].party_id]);
            $('.duma-modal__party-name').html(parts[data[the_matrix[xx][yy]].party_id].name).css('color', color[data[the_matrix[xx][yy]].party_id]);
            $('.duma-modal__name').html(data[the_matrix[xx][yy]].name);
            $('.duma-modal__birthdate').html(formatDate(data[the_matrix[xx][yy]].date_birthday)+ ' ('+getAge(data[the_matrix[xx][yy]].age)+')');
            if(data[the_matrix[xx][yy]].hasOwnProperty('cikrf_work_position_group') && data[the_matrix[xx][yy]].cikrf_work_position_group.length > 0)
                $('.duma-modal__more-work-value').html(data[the_matrix[xx][yy]].cikrf_work_position_group);
            else
                $('.duma-modal__more-work-value').html('не указан');
            $('.duma-modal__more-commerce-value').html(data[the_matrix[xx][yy]].bissness_count);
            $('.duma-modal__more-election-value').html(data[the_matrix[xx][yy]].election_event_list_type);
            $('.duma-modal__more-income-value').html(parseFloat(data[the_matrix[xx][yy]].inbox_money_summ).formatMoney(0, ',', ' ')+' &#8381;');
            $('.duma-modal__more-bank-value').html(parseFloat(data[the_matrix[xx][yy]].bank_total_sum).formatMoney(0, ',', ' ')+' &#8381;');
            var rect = work.getBoundingClientRect();
            var from_top = evt.offsetY - ($('.duma-modal').height() / 2 - 50);
            if (from_top < 0 )
                from_top = 0;
            else if (from_top+$('.duma-modal').height() > $('body').height())
                from_top = $('body').height() - $('.duma-modal').height();
            if(xx > 18) {
                $('.duma-modal').css({
                    'left': 0,
                    'top': from_top,
                    'display': 'block'
                });
            } else {
                $('.duma-modal').css({
                    'left': 'auto',
                    'right': 0,
                    'top': from_top,
                    'display': 'block'
                });
            }
        }
        data[the_matrix[xx][yy]].circle.draw(1);
        last_circle = data[the_matrix[xx][yy]].circle;
        xxx = xx; yyy = yy;
    } else if(last_circle.hasOwnProperty('x')) {
        last_circle.reDraw();
        last_circle = {};
        xxx = -1; yyy = -1;
        $('.duma-modal').css('display', 'none');
    }
});

work.addEventListener('mouseout', function(evt) {
    var coodrss =$('.duma-modal').offset();
    var maxXcoord = coodrss.left + parseInt($('.duma-modal').width());
    var maxYcoord = coodrss.top + parseInt($('.duma-modal').width());

    if ((evt.clientX < coodrss.left || evt.clientX > maxXcoord) || (evt.clientY < coodrss.top && evt.clientY > maxYcoord)) {
        $('.duma-modal').css('display', 'none');
        if(last_circle.hasOwnProperty('x')) {
            last_circle.reDraw();
            last_circle = {};
        }
    }
});

biz.addEventListener('mousemove', function(evt) {
    var mousePos = getMousePos(biz, evt);
    var xx = Math.floor(mousePos.x / width);
    if(xx > 19)
        xx = Math.floor((mousePos.x - 5) / width);
    var yy = Math.floor(mousePos.y / width);
    if(the_matrix2[xx][yy] != undefined) {
        if(circles[the_matrix2[xx][yy]] != last_circle && (xxx != xx || yyy != yy)) {
            if(last_circle.hasOwnProperty('x'))
                last_circle.reDraw();
            if(data[circles[the_matrix2[xx][yy]].id].img == 1)
                $('.duma-modal__image').html('<img onerror="imgError(this);" src="images/deputats/'+data[circles[the_matrix2[xx][yy]].id].img_file+'" />').css('background', color[data[circles[the_matrix2[xx][yy]].id].party_id]);
            else
                $('.duma-modal__image').html('').css('background', color[data[circles[the_matrix2[xx][yy]].id].party_id]);
            $('.duma-modal__party-name').html(parts[data[circles[the_matrix2[xx][yy]].id].party_id].name).css('color', color[data[circles[the_matrix2[xx][yy]].id].party_id]);
            $('.duma-modal__name').html(data[circles[the_matrix2[xx][yy]].id].name);
            $('.duma-modal__birthdate').html(formatDate(data[circles[the_matrix2[xx][yy]].id].date_birthday)+ ' ('+getAge(data[circles[the_matrix2[xx][yy]].id].age)+')');
            if(data[circles[the_matrix2[xx][yy]].id].hasOwnProperty('cikrf_work_position_group') && data[circles[the_matrix2[xx][yy]].id].cikrf_work_position_group.length > 0)
                $('.duma-modal__more-work-value').html(data[circles[the_matrix2[xx][yy]].id].cikrf_work_position_group);
            else
                $('.duma-modal__more-work-value').html('не указан');
            $('.duma-modal__more-commerce-value').html(data[circles[the_matrix2[xx][yy]].id].bissness_count);
            $('.duma-modal__more-election-value').html(data[circles[the_matrix2[xx][yy]].id].election_event_list_type);
            $('.duma-modal__more-income-value').html(parseFloat(data[circles[the_matrix2[xx][yy]].id].inbox_money_summ).formatMoney(0, ',', ' ')+' &#8381;');
            $('.duma-modal__more-bank-value').html(parseFloat(data[circles[the_matrix2[xx][yy]].id].bank_total_sum).formatMoney(0, ',', ' ')+' &#8381;');
            var from_top = evt.offsetY - ($('.duma-modal').height() / 2);
            if (from_top < 0 )
                from_top = 0;
            else if (from_top+$('.duma-modal').height() > $('body').height())
                from_top = $('body').height() - $('.duma-modal').height();
            if(xx > 18) {
                $('.duma-modal').css({
                    'left': 0,
                    'top': from_top,
                    'display': 'block'
                });
            } else {
                $('.duma-modal').css({
                    'left': 'auto',
                    'right': 0,
                    'top': from_top,
                    'display': 'block'
                });
            }
        }
        circles[the_matrix2[xx][yy]].draw(1);
        last_circle = circles[the_matrix2[xx][yy]];
        xxx = xx; yyy = yy;
    } else if(last_circle.hasOwnProperty('x')) {
        last_circle.reDraw();
        last_circle = {};
        $('.duma-modal').css('display', 'none');
        xxx = -1; yyy = -1;
    }
});

biz.addEventListener('mouseout', function(evt) {
    var coodrss =$('.duma-modal').offset();
    var maxXcoord = coodrss.left + parseInt($('.duma-modal').width());
    var maxYcoord = coodrss.top + parseInt($('.duma-modal').width());

    if ((evt.clientX < coodrss.left || evt.clientX > maxXcoord) || (evt.clientY < coodrss.top && evt.clientY > maxYcoord)) {
        $('.duma-modal').css('display', 'none');
        if(last_circle.hasOwnProperty('x')) {
            last_circle.reDraw();
            last_circle = {};
            xxx = -1; yyy = -1;
        }
    }
});
