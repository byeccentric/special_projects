"use scrict";

var data = {};
var data_big = {};
var all_count = 0;
var max = 0;

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.value == this.axis.min) ? "left" : ((this.value == this.axis.max) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
    url: '2.json',
    dataType: 'json',
    cache: false,
    complete: function (items) {
        "use strict";

        var data_i = JSON.parse(items.responseText);

        all_count = 0;
        for(var i=0, item; item=data_i[i++];) {
            if(data_big.hasOwnProperty(item.group_in)) {
                data_big[item.group_in]['data'].push({
                    y: item.percent,
                    year: item.year,
                    data_2016: item.data_2016
                });
            } else {
                all_count++;
                data_big[item.group_in] = {
                    name: item.group_in,
                    fillColor: item.group_in=='Другое' ? '#d8d8d8' : '#56cc90',
                    lineWidth: 0,
                    data: []
                };
                data_big[item.group_in]['data'].push({
                    y: item.percent,
                    year: item.year,
                    data_2016: item.data_2016
                });
            }
            if(item.percent > max)
                max = item.percent;
        }

        max = max + 10;

        init();
    }
})
});

function init() {
    var cnt = 1;
    for(var key in data_big) {
        $('.graphics').append('<div class="graphic" id="container'+cnt+'"></div>');
        Highcharts.chart('container'+cnt, {
            chart: {
                type: 'area',
                spacingBottom: 0,
                spacingTop: 0,
                spacingLeft: 0,
                spacingRight: 0,
                height: 200,
                style: {
                    'fontFamily' : "ALSStory, 'Helvetica CY', Arial, sans-serif"
                }
            },
            title: {
                text: key,
                align: 'left',
                style: {'fontSize': '18px', 'fontWeight': 'bold'},
                x: 5
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                visible: false,
                padding: 0,
                'max': max
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                area: {
                    className: 'some',
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        fillColor: '#000000',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: false,
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.9,
                    dataLabels : {
                        enabled : true,
                        formatter: function() {
                            if(this.key == 0 || this.key == 14)
                                return '<div style="margin-left: 7px;"><b>'+this.point.year+'</b><br/>'+formatSumm(this.point.data_2016)+'</div>';
                            return null;
                        },
                        useHTML: true,
                        style: {"fontSize": '14px', "fontWeight": 'normal', "text-align": 'right'},
                        align: 'right',
                        y: -7,
                        x: -5
                    },
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                }
            },
            xAxis: {
                categories: [
                    2005,
                    2006,
                    2007,
                    2008,
                    2009,
                    2010,
                    2011,
                    2012,
                    2013,
                    2014,
                    2015,
                    2016,
                    2017,
                    2018,
                    2019
                ],
                visible: true,
                endOnTick: false,
                startOnTick: false,
                plotLines: [
                    {
                        color: "rgba(0,0,0,0.15)",
                        dashStyle: "Solid",
                        value: 0,
                        width: 1,
                        zIndex: 5
                    },
                    {
                        color: "rgba(0,0,0,0.15)",
                        dashStyle: "Solid",
                        value: 14,
                        width: 1,
                        zIndex: 5
                    }
                ],
                labels: {
                    enabled: false
                },
                showFirstLabel: false,
                showLastLabel: false,
                lineWidth: 0,
                tickLength: 0
            },
            series: [data_big[key]]
        });
        cnt++;
    }
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 > 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 > 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 > 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 > 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}