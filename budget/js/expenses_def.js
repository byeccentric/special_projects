"use scrict";

function get_GET () {
    var result = [];
    var search = decodeURIComponent(location.search.substr(1)).split('&');
    for (var i in search) {
        search[i] = search[i].split('=');
        result[ search[i][0] ] = search[i][1];
    }
    return result;
}

var GET = get_GET();

var data = {};
var max = 0;
var colors = ['#56cc90', '#d7ca22', '#22c7fd', '#df5a3c', '#1c36a5', '#ed8908', 'grey', 'violet', 'brown'];

var charts = [];

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.isFirst) ? "left" : ((this.isLast) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
        url: 'data/def_detail.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            "use strict";

            var data_i = JSON.parse(items.responseText);

            for(var i=0, item; item=data_i[i++];) {
                if(!data.hasOwnProperty(item.subsection_out))
                    data[item.subsection_out] = [];

                data[item.subsection_out].push({
                    y: item.data_2016,
                    year: item.year,
                    short_name: item.subsection_out_short
                });
            }
            initDetail();
        }
    });
});

function initDetail() {
    $('.underGraphic').show();
    $('.legend').show();
    $('.graphics').hide();

    var value = {
        series: []
    };

    var cnt = 0;
    var max = 0;
    $('.legend').html('');
    for(var key in data) {
        value.series.push({
            name: key,
            color: colors[cnt],
            data: data[key]
        });

        for(var i=0, item; item = data[key][i++];){
            if(item.y > max)
                max = item.y;
        }

        var html = $("#legend_item").html();
        html = _.template(html);
        html = html({"item": { class_add: cnt, name: key, short_name: data[key][0].short_name}});
        $('.legend').append(html);
        cnt++;
    }
    Highcharts.chart('maked', {
            chart: {
                type: 'line',
                spacingBottom: 0,
                spacingTop: 0,
                spacingLeft: 20,
                spacingRight: 10,
                height: 500,
                style: {
                    'fontFamily' : "ALSStory, 'Helvetica CY', Arial, sans-serif"
                }
            },
            title: {
                text: '',
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            yAxis: {
                visible: true,
                padding: 0,
                lineWidth: 0,
                tickLength: 0,
                lineColor: "#FFF",
                minorGridLineWidth: 0,
                tickWidth: 0,
                offset: 0,
                title: {
                    enabled: false,
                    margin: 0
                },
                labels: {
                    formatter: function() {
                        return formatSumm(this.value);
                    },
                    distance: 0,
                    padding: 0,
                    y: 3,
                    x: -15,
                    zIndex: 10,
                    align: 'right',
                    style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                },
                showFirstLabel: false,
                showLastLabel: false,
                gridLineColor: "rgba(0,0,0,0.15)",
                gridLineDashStyle: "Solid",
                gridLineWidth: 1,
                gridZIndex: 5,
                tickAmount:5,
                'max': max,
                startOnTick: false,
                endOnTick: false,
            },
            tooltip: {
                useHTML: true,
                formatter: _makeToolptipDetail,
                borderRadius: 0,
                borderWidth: 0,
                padding: 0
            },
            plotOptions: {
                line: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 2
                        }
                    },
                    marker: {
                        symbol: 'circle',
                        enabled: false
                    },
                },
            },
            series: value.series,
            xAxis: [
                {
                    categories: [
                        2011,
                        2012,
                        2013,
                        2014,
                        2015,
                        2016,
                        2017,
                        2018,
                        2019
                    ],
                    gridLineColor: "rgba(0,0,0,0.15)",
                    gridLineDashStyle: "Solid",
                    gridLineWidth: 1,
                    gridZIndex: 5,
                    visible: true,
                    opposite: false,
                    labels: {
                        align: "center",
                        margin: 0,
                        x: 10,
                        style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                    },
                    lineWidth: 0,
                    tickLength: 0,
                    startOnTick: false,
                    endOnTick: false,
                    minPadding: 0,
                    maxPadding: 0,
                }, {
                    categories: [
                        2011,
                        2012,
                        2013,
                        2014,
                        2015,
                        2016,
                        2017,
                        2018,
                        2019
                    ],
                    linkedTo: 0,
                    visible: true,
                    opposite: true,
                    labels: {
                        align: "center",
                        margin: 0,
                        x: 10,
                        style: {'fontSize': "14px", 'fontWeight': 'bold', "color": "#000000"}
                    },
                    lineWidth: 0,
                    tickLength: 0,
                    startOnTick: false,
                    endOnTick: false,
                    minPadding: 0,
                    maxPadding: 0
                }
            ]
        }
    );
}

function _makeToolptipDetail() {
    var style = '';
    var style2 = '';
    if(parseInt(this.point.year) > 2014) {
        if(GET['type'] == 'l') {
            style = 'style="right: 0; left: auto; width: 150px;"';
            style2 = 'style="font-size: 14px"';
        }
        else
            style = 'style="right: 0; left: auto;"';
    } else {
        if(GET['type'] == 'l') {
            style = 'style="width: 150px"';
            style2 = 'style="font-size: 14px"';
        }
    }
    var item = {
        year: this.point.year,
        val: formatSumm(this.point.y),
        name: this.series.name,
        styles: style,
        styles2: style2,
        attr: GET['type']
    };

    var html = $("#modal-tooltip-detail").html();
    html = _.template(html);
    html = html({"item": item});

    return html;
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 >= 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 >= 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 >= 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 >= 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}