"use scrict";

var colors = ['#56cc90', '#d7ca22', '#22c7fd', '#df5a3c', '#1c36a5', '#ed8908', 'grey', 'violet', 'brown'];

var data_big_2 = {
    color: colors[0],
    name: 'Расходы из консолидированного бюджета',
    data: []
};
var data_big_1 = {
    color: colors[1],
    name: 'Расходы из федерального бюджета',
    data: []
};

var chart;
var max1 = 0, max2 = 0;

Highcharts.Axis.prototype.init = (function (func) {
    return function (chart, userOptions) {
        func.apply(this, arguments);
        if (this.categories) {
            this.userCategories = this.categories;
            this.categories = undefined;
            this.labelFormatter = function() {
                this.axis.options.labels.align = (this.value == this.axis.min) ? "left" : ((this.value == this.axis.max) ? "right" : "center");
                return this.axis.userCategories[this.value];
            }
        }
    };
} (Highcharts.Axis.prototype.init));

$(document).ready(function() {
    $.ajax({
        url: 'data/safe_kb.json',
        dataType: 'json',
        cache: false,
        complete: function (items) {
            "use strict";

            var data_i = JSON.parse(items.responseText);

            for(var i=0,item; item=data_i[i++];) {
                data_big_2['data'].push({
                    y: item.date_kb_2016,
                    year: item.year
                });

                if(item.date_kb_2016 > max2) {
                    max2 = item.date_kb_2016;
                }

                data_big_1['data'].push({
                    y: item.date_fb_2016,
                    year: item.year
                });

                if(item.date_fb_2016 > max1) {
                    max1 = item.date_fb_2016;
                }
            }

            init(1);
            init(2);
        }
    })
});

function init(num) {
    var series, max;

    if(num == 1) {
        series = data_big_1;
        max = max1;
    } else {
        series = data_big_2;
        max = max2;
    }

    chart = Highcharts.chart('container'+num, {
        chart: {
            type: 'area',
            spacingBottom: 0,
            spacingTop: 0,
            spacingLeft: 0,
            spacingRight: 0,
            height: 200,
            style: {
                'fontFamily' : "ALSStory, 'Helvetica CY', Arial, sans-serif"
            }
        },
        title: {
            text: series['name'],
            align: 'left',
            style: {'fontSize': '18px', 'fontWeight': 'bold'},
            x: 5
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        yAxis: {
            visible: false,
            padding: 0,
            'max': max,
            tickAmount:5
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            area: {
                className: 'some',
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    fillColor: '#000000',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: false,
                            radius: 2
                        }
                    }
                },
                fillOpacity: 0.9,
                dataLabels : {
                    enabled : true,
                    formatter: function() {
                        if(this.key == 0 || this.key == 8)
                            return '<b>' + this.point.year + '</b><br/>' + formatSumm(this.point.y);
                        return null;
                    },
                    useHTML: true,
                    className: 'toop'+num,
                    style: {"fontSize": '14px', "fontWeight": 'normal', "text-align": 'right', 'marginTop': '-10px'},
                    align: 'right',
                    y: -15,
                    x: 2
                },
                states: {
                    hover: {
                        enabled: false
                    }
                }
            }
        },
        xAxis: {
            categories: [
                2011,
                2012,
                2013,
                2014,
                2015,
                2016,
                2017,
                2018,
                2019
            ],
            visible: true,
            endOnTick: false,
            startOnTick: false,
            plotLines: [
                {
                    color: "rgba(0,0,0,0.15)",
                    dashStyle: "Solid",
                    value: 0,
                    width: 1,
                    zIndex: 5
                },
                {
                    color: "rgba(0,0,0,0.15)",
                    dashStyle: "Solid",
                    value: 8,
                    width: 1,
                    zIndex: 5
                }
            ],
            labels: {
                enabled: false
            },
            showFirstLabel: false,
            showLastLabel: false,
            lineWidth: 0,
            tickLength: 0
        },
        series: [series]
    });
}

function formatSumm(value) {
    var num = 0;
    if(value / 100000000000 >= 10) {
        num = Math.round(value/100000000000) / 10;
        return String(num).replace('.', ',')+' трлн';
    }
    if(value / 100000000 >= 10) {
        num = Math.round(value/100000000) / 10;
        return String(num).replace('.', ',')+' млрд';
    }
    if(value / 100000 >= 10) {
        num = Math.round(value/100000) / 10;
        return String(num).replace('.', ',')+' млн';
    } else if(value / 100 >= 10) {
        num = Math.round(value/100) / 10;
        return String(num).replace('.', ',')+' тыс.';
    } else {
        return String(Math.round(value*10) / 10).replace('.', ',');
    }
}