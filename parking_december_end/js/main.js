'use strict';

function get_GET () {
    var result = [];
    var search = decodeURIComponent(location.search.substr(1)).split('&');
    for (var i in search) {
        search[i] = search[i].split('=');
        result[ search[i][0] ] = search[i][1];
    }
    return result;
}

var GET = get_GET();
var BG_MAP = 'light';
var BG_MAP_URL = 'http://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.{ext}';
var BG_MAP_COPYRIGHT = 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
var COLOR = '#18ca95';
var LINE_WEIGHT = 7;

if (GET['bg']) {
    BG_MAP = GET['bg'];
    if (BG_MAP == 'dark') {
        BG_MAP_URL = '//{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.{ext}';
        BG_MAP_COPYRIGHT = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>';
    } if (BG_MAP == 'osm') {
        BG_MAP_URL = '//korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}';
        BG_MAP_COPYRIGHT = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>';
    } if (BG_MAP == 'bw') {
        BG_MAP_URL = '//{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png';
        BG_MAP_COPYRIGHT = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>';

    } if (BG_MAP == 'hyd') {
        BG_MAP_URL = '//{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png';
        BG_MAP_COPYRIGHT = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>';
    }
}

if (GET['c']) {
    COLOR = '#' + GET['c'];
}

if (GET['w']) {
    LINE_WEIGHT = GET['w'];
}

// --------

function makePopupHtml(options) {
    var little = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var html = '';
    var html2 = '';
    for (var key in options){
        if (key == 'street' && !little || key == 'address' && little) {
            html += '<div class="popup__header">' + options[key] + '</div>';
        } else {
            if (key != 'line' && key != 'price_id' && key != 'weight' && key != 'price_first_hour' && key != 'street') {
                html2 += '<div class="popup__item">' + options[key];
                html2 += '<div class="popup__item-name">' + translate[key] + '</div></div>';
            }
        }
    }
    return html + html2;
}

function makeFilterElement(first_time, name, layer) {
    if (layer != undefined) var id = layer['_leaflet_id'];else var id = '';

    if (first_time) {
        allLayers[id] = layer;
    }

    var filter_elem = document.createElement('div');
    filter_elem.classList.add('filter__item');
    filter_elem.classList.add('filter_title__item');
    filter_elem.classList.add('js-filter-item');
    filter_elem.setAttribute('data-id', id);
    filter_elem.style.display = 'block';

    var filter_elem_points = document.createElement('div');
    filter_elem_points.classList.add('filter__points');

    var filter_elem_name = document.createElement('span');
    filter_elem_name.classList.add('filter__text');
    filter_elem_name.classList.add('js-filter-text');
    filter_elem_name.innerHTML = name;

    filter_elem_points.appendChild(filter_elem_name);
    filter_elem.appendChild(filter_elem_points);
    document.querySelector('.js-filter-item-container').appendChild(filter_elem);
    filter_elem.addEventListener('click', function(e) {
        filterClickAction(filter_elem)
    });
}

function filterClickAction(self) {
    setTimeout(function () {
        filter_opener.removeClass('active');
    }, 10);
    if ($('.filter__item.active')) $('.filter__item.active').removeClass('active');
    $(self).addClass('active');
    $('.filter__point.js-filter-point').html($(self).html());

    map.setView($(self).data('id').split(','), 16);
}

var translate = {
    price: 'Стоимость',
    car_capacity: 'Вместимость (кол-во машин)',
    address: 'Точный адрес парковки'
};

var prices = [{}, {
    color: '#2F8C3C',
    shortname: '40 <span class="g-rub">i</span>',
    fullname: '40 руб./час'
}, {
    color: '#ed0012',
    shortname: '50 <span class="g-rub">i</span>',
    fullname: '50 руб./час'
}, {
    color: '#4b00ed',
    shortname: '50 <span class="g-rub">i</span>',
    fullname: 'Первые 30 минут &mdash; 50 руб. <br>Больше 30 минут с 8.00 до 20.00 &mdash; 150 руб./час  <br> Больше 30 минут с 20.00 до 8.00 &mdash; 80 руб./час'
}, {
    color: '#',
    shortname: '60 <span class="g-rub">i</span>',
    fullname: '60 руб./час'
}, {
    color: '#0f99b2',
    shortname: '60 <span class="g-rub">i</span>',
    fullname: 'Первый час &mdash; 60 руб./час <br>Больше часа с 8.00 до 20.00 &mdash; 100 руб./час<br>Больше часа с 20.00 до 8.00 &mdash; 60 руб./час '
}, {
    color: '#EB2FA6',
    shortname: '80 <span class="g-rub">i</span>',
    fullname: '80 руб./час'
}, {
    color: '#ed0012',
    shortname: '100 <span class="g-rub">i</span>',
    fullname: '100 руб./час'
}, {
    color: '#ed0012',
    shortname: '200 <span class="g-rub">i</span>',
    fullname: '200 руб./час'
}];

var prices_per_hour = {
    40: {
        color: COLOR,
        name: '40 <span class="g-rub">i</span>',
    },
    50: {
        color: '#4b00ed',
        name: '50 <span class="g-rub">i</span>',
    },
    60: {
        color: '#0f99b2',
        name: '60 <span class="g-rub">i</span>',
    },
    80: {
        color: '#EB2FA6',
        name: '80 <span class="g-rub">i</span>',
    },
    100: {
        color: '#FF8D0D',
        name: '100 <span class="g-rub">i</span>',
    },
    200: {
        color: '#ed0012',
        name: '200 <span class="g-rub">i</span>',
    },
};

var default_price = 200;
var chosen_price = default_price;

var maximum_height = 300;

var zoom = 10;

var data_all = []; // Типа выбранные парковки
var allLayers = [];
var allLines = [];

var dif_streets = [];

var groupLayer;

var customOptions = {
    maxWidth: 500,
    className: 'map-popup',
    closeButton: true,
    closeOnClick: false
};

var map = L.map('map').setView([55.7482200, 37.6155600], zoom);
map.scrollWheelZoom.disable();
var data = {}; // Типа улицы
L.tileLayer(BG_MAP_URL, {
    attribution: BG_MAP_COPYRIGHT,
    subdomains: 'abcd',
    minZoom: 0,
    maxZoom: 17,
    ext: 'png'
}).addTo(map);

var xhr1 = new XMLHttpRequest();
xhr1.open('GET', 'new_streets.json', true);

xhr1.onload = function (e) {
    data = {
        type: 'FeatureCollection',
        features: []
    };
    JSON.parse(xhr1.responseText).forEach(function(item) {
        var geometry = {};
        if(item.geometry != null)
            geometry = item.geometry.pop();
        if(geometry != {} && geometry.type != "Point") {
            var temp = {
                type: 'Feature',
                geometry: geometry,
                properties: {
                    street: item.street
                }
            };
            data.features.push(temp);
        }
    });

    //data.features.sort(function(a,b){ return a.properties.street[0].toLowerCase().charCodeAt() - b.properties.street[0].toLowerCase().charCodeAt()});

    console.log(data);

    var geojson = L.geoJSON(
        data,
        {
            style: function(feature) {
                return {
                    color: COLOR,
                    weight: LINE_WEIGHT
                }
            },
            onEachFeature: function(feature, layer) {
                if (feature.properties) {
                    layer.bindPopup(makePopupHtml(feature.properties), customOptions);
                }
            }
        }
    ).addTo(map);

    //map.fitBounds(geojson.getBounds());
    //zoom = map.getZoom();
};

xhr1.onerror = function (e) {
    console.error(xhr1.status + ': ' + xhr1.statusText);
};
xhr1.send(null);


/*    var xhr2 = new XMLHttpRequest();
xhr2.open('GET', 'parks.json', true);

xhr2.onload = function (e) {
    JSON.parse(xhr2.responseText).forEach(function (item) {
        if (item.coordinates.length > 0) {
            var uslovie = true;
            item.coordinates.forEach(function (item) {
                return item == null ? uslovie = false : '';
            });
            if (uslovie) data_all.push({
                address: item.address,
                street: item.street,
                price_first_hour: item.price_first_hour,
                price_id: item.price_id,
                price: prices[item.price_id].fullname,
                car_capacity: item.car_capacity,
                line: item.coordinates,
                weight: LINE_WEIGHT
            });
        }
    });

    data_all.forEach(function (item) {
        if (item.price_first_hour == chosen_price) allLines[allLines.length] = L.polyline(item.line, {
            color: prices_per_hour[item.price_first_hour].color || COLOR,
            address: item.address,
            price: prices[item.price_id].fullname,
            weight: LINE_WEIGHT,
            opacity: 1
        }).bindPopup(makePopupHtml(item, true), customOptions);
    });
    if (allLines.length > 0) {
        groupLayer = L.featureGroup(allLines).addTo(map);
        //map.fitBounds(groupLayer.getBounds());
        map.setView([55.7482200, 37.6155600], 12);
    }

    for (var key in prices_per_hour) {
        var item = prices_per_hour[key];
        if (key != 0 && item != {} && key != 2 && key != 4 && key != 7) {
            var chooser_elem = document.createElement('a');
            chooser_elem.classList.add('choosers__item');
            chooser_elem.setAttribute('href', '#');
            chooser_elem.setAttribute('data-price-id', key);
            chooser_elem.innerHTML = item.name;
            chooser_elem.style.color = prices_per_hour[key].color;
            if (key == chosen_price)
                chooser_elem.classList.add('choosers__item_active');
            document.querySelector('.choosers').appendChild(chooser_elem);
            chooser_elem.addEventListener('click', function (e) {
                if (!this.classList.contains('choosers__item_active')) {
                    filter_opener.removeClass('active');
                    $('.filter__item').removeClass('active').css('display', "block");
                    $('.filter__search__input').val('');
                    $('.filter__point.js-filter-point').html('Все улицы');
                    $('.js-filter-clear').addClass('active');

                    $('.choosers__item.choosers__item_active').removeClass('choosers__item_active');
                    $(this).addClass('choosers__item_active');
                    chosen_price = $(this).attr('data-price-id');

                    map.eachLayer(function (layer) {
                        return layer instanceof L.Polyline ? map.removeLayer(layer) : '';
                    });

                    dif_streets = [];
                    data_all.forEach(function (item) {
                        return item.price_first_hour == chosen_price ? !~dif_streets.indexOf(item.street) ? dif_streets.push(item.street) : '' : '';
                    });

                    dif_streets.sort();
                    // Подгружаем улицы из прекрасного далека
                    $('.js-filter-item-container').html('');
                    dif_streets.forEach(function (item) {
                        makeFilterElement(false, item);
                    });

                    allLines = [];
                    data_all.forEach(function (item) {
                        if (item.price_first_hour == chosen_price) allLines[allLines.length] = L.polyline(item.line, {
                            color: prices_per_hour[item.price_first_hour].color || COLOR,
                            address: item.address,
                            price: prices[item.price_id].fullname,
                            weight: LINE_WEIGHT,
                            opacity: 1
                        }).bindPopup(makePopupHtml(item, true), customOptions);
                    });
                    if (allLines.length > 0) {
                        groupLayer = L.featureGroup(allLines).addTo(map);
                        //map.fitBounds(groupLayer.getBounds());
                        map.setView([55.7482200, 37.6155600], 12);
                    }
                }
                e.preventDefault();
            });
        }
    }
    var chooser_elem = document.createElement('a');
    chooser_elem.classList.add('choosers__item');
    chooser_elem.setAttribute('href', '#');
    chooser_elem.setAttribute('data-price-id', 'all');
    chooser_elem.innerHTML = 'Все';
    document.querySelector('.choosers').appendChild(chooser_elem);
    chooser_elem.addEventListener('click', function (e) {
        if (!this.classList.contains('choosers__item_active')) {
            filter_opener.removeClass('active');
            $('.filter__item').removeClass('active').css('display', "block");
            $('.filter__search__input').val('');
            $('.filter__point.js-filter-point').html('Все улицы');
            $('.js-filter-clear').addClass('active');

            $('.choosers__item.choosers__item_active').removeClass('choosers__item_active');
            $(this).addClass('choosers__item_active');
            chosen_price = $(this).attr('data-price-id');

            map.eachLayer(function (layer) {
                return layer instanceof L.Polyline ? map.removeLayer(layer) : '';
            });

            dif_streets = [];
            data_all.forEach(function (item) {
                return !~dif_streets.indexOf(item.street) ? dif_streets.push(item.street) : '';
            });

            dif_streets.sort();
            // Подгружаем улицы из прекрасного далека
            $('.js-filter-item-container').html('');
            dif_streets.forEach(function (item) {
                makeFilterElement(false, item);
            });

            allLines = [];
            data_all.forEach(function (item) {
                 allLines[allLines.length] = L.polyline(item.line, {
                    color: prices_per_hour[item.price_first_hour].color || COLOR,
                    address: item.address,
                    price: prices[item.price_id].fullname,
                    weight: LINE_WEIGHT,
                    opacity: 1
                }).bindPopup(makePopupHtml(item, true), customOptions);
            });
            if (allLines.length > 0) {
                groupLayer = L.featureGroup(allLines).addTo(map);
                //map.fitBounds(groupLayer.getBounds());
                map.setView([55.7482200, 37.6155600], 12);
            }
        }
        e.preventDefault();
    });
};
xhr2.onerror = function (e) {
    console.error(xhr2.status + ': ' + xhr2.statusText);
};
xhr2.send(null);*/


var timeOut;
var search = $('.filter__search__input');
search.keyup(function (e) {
    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        var count = 0;
        /*$.each($('.filter__item'), function (index, item) {
            if (!~$(item).children().children().html().toLowerCase().indexOf(search.val().toLowerCase()) && !$(item).hasClass('js-filter-clear') && !$(item).hasClass('js-filter-item-empty')) {
                $(item).css('display', 'none');
            } else {
                if (!$(item).hasClass('js-filter-item-empty') && !$(item).hasClass('js-filter-clear')) {
                    count++;
                    $(item).css('display', 'block');
                }
            }
        });
        if (count == 0)
            $('.js-filter-item-empty').css('display', "block");
        else
            $('.js-filter-item-empty').css('display', "none");

        var all_height = $('.js-scrollable-content').clientHeight;
        if (all_height > maximum_height) {
            $('.custom-scroll').css('display', "block");
            $('.custom-scroll__trackbar').css('height', Math.round(maximum_height / all_height * maximum_height) + 'px');
        } else {
            $('.custom-scroll').css('display', "none");
        }*/
        $('.js-filter-item-container').html('');

        var myGeocoder = ymaps.geocode(search.val(),{kind: 'street'});
        myGeocoder.then(
            function (res) {
                //var gg = res.geoObjects.get(0).geometry.getCoordinates().split(',');
                var count = 0;
                for(var i=0; i<50; i++) {
                    if(res.geoObjects.get(i) == undefined) continue;
                    if (res.geoObjects.get(i).properties['_data'].description == 'Россия, Москва') {
                        count++;
                        $('.custom-scroll').css('display', "none");
                        var filter_elem = document.createElement('div');
                        filter_elem.classList.add('filter__item');
                        filter_elem.classList.add('filter_title__item');
                        filter_elem.classList.add('js-filter-item');
                        filter_elem.setAttribute('data-id', res.geoObjects.get(i).geometry['_coordinates']);
                        filter_elem.style.display = 'block';

                        var filter_elem_points = document.createElement('div');
                        filter_elem_points.classList.add('filter__points');

                        var filter_elem_name = document.createElement('span');
                        filter_elem_name.classList.add('filter__text');
                        filter_elem_name.classList.add('js-filter-text');
                        filter_elem_name.innerHTML = res.geoObjects.get(i).properties['_data'].text;

                        filter_elem_points.appendChild(filter_elem_name);
                        filter_elem.appendChild(filter_elem_points);
                        document.querySelector('.js-filter-item-container').appendChild(filter_elem);
                        filter_elem.addEventListener('click', function(e) {
                            filterClickAction(filter_elem);
                        });
                    }
                }
                if(count > 0) {
                    $('.js-filter-item-empty').css('display', "none");
                } else {
                    $('.js-filter-item-empty').css('display', "block");
                }

            },
            function (err) {
                $('.js-filter-item-empty').css('display', "block");
            }
        );
    }, 500);

});

var filter_opener = $('.filter');
var filter_clear = $('.js-filter-clear');
filter_opener.click(function(e) {
    if (e.target != filter_clear[0] && !filter_clear[0].contains(e.target)) {
        $(this).addClass('active');
        var all_height = $('.js-scrollable-content').clientHeight;
        if (all_height > maximum_height) {
            $('.custom-scroll').css('display', "block");
            $('.custom-scroll__trackbar').css('height', Math.round(maximum_height / all_height * maximum_height) + 'px');
        } else {
            $('.custom-scroll').css('display', "none");
        }
        $('.filter__search__input').focus();
    }
});

$('.js-scrollable').on('scroll', function (e) {
    if (!tooked) {
        var from_top = this.scrollTop;
        var all_height = $('.js-scrollable-content').clientHeight;
        $('.custom-scroll__trackbar').css('top', Math.round(from_top / all_height * maximum_height) + 'px');
    }
});

var tooked = false;
var downY;
document.addEventListener('mousedown', function (e) {
    if (e.target != filter_opener && !filter_opener.has(e.target)) {
        filter_opener.removeClass('active');
    }
    if (e.which != 1) {
        return;
    }
    if (e.target == $('.custom-scroll__trackbar')) {
        tooked = true;
        downY = e.pageY;
    }
});

var timeCloseOut;
$('.filters').mouseout(function() {
    timeCloseOut = setTimeout(function() {
        filter_opener.removeClass('active');
    }, 1000);
});

$('.filters').mouseover(function() {
    clearTimeout(timeCloseOut);
});

document.addEventListener("mousemove", function (e) {
    if (!tooked) return;

    var from_top;
    var scroller_height = parseInt($('.custom-scroll__trackbar').css('height'));

    if (parseInt($('.custom-scroll__trackbar').css('top')) + e.pageY - downY > 0) {
        if (parseInt($('.custom-scroll__trackbar').css('top')) + e.pageY - downY <= maximum_height - scroller_height) {
            from_top = parseInt($('.custom-scroll__trackbar').css('top')) + (e.pageY - downY);
            downY = e.pageY;
        } else {
            from_top = maximum_height - scroller_height;
        }
    } else {
        from_top = 0;
    }
    $('.custom-scroll__trackbar').css('top', from_top + 'px');

    var all_height = $('.js-scrollable-content').clientHeight;

    $('.js-scrollable').scrollTop = Math.round((all_height - maximum_height) * (from_top / (maximum_height - scroller_height)));
    e.preventDefault();
});

document.addEventListener("mouseup", function (e) {
    tooked = false;
    downY = 0;
});

filter_clear.click(function (e) {
    filter_opener.removeClass('active');
    $('.js-filter-item-container').html('');
    $('.filter__search__input').val('');
    $('.filter__point.js-filter-point').html('Все улицы');
    $(this).addClass('active');
    allLines = [];
    data_all.forEach(function (item) {
        if (item.price_first_hour == chosen_price || chosen_price == "all")
            allLines[allLines.length] = L.polyline(item.line, {
                color: prices_per_hour[item.price_first_hour].color || COLOR,
                address: item.address,
                price: item.price,
                weight: LINE_WEIGHT
            }).bindPopup(makePopupHtml(item, true), customOptions);
    });
    groupLayer = L.featureGroup(allLines).addTo(map);
    //map.fitBounds(groupLayer.getBounds());
    map.setView([55.7482200, 37.6155600], 10);
});

document.onkeydown = function (evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = evt.key == "Escape" || evt.key == "Esc";
    } else {
        isEscape = evt.keyCode == 27;
    }
    if (isEscape) {
        //  js-filter-scroll
        // document.getElementsByClassName('filter__list__inner')[0].style.visibility = 'hidden';
        //document.getElementsByClassName('js-filter-select')[0].className = 'filter filter_title js-filter-select';
        $('.js-filter-select').removeClass('active');
    }
};