var data = {
    clinton: [
        {
            'name': 'Возраст',
            'value': '40-49'
        },
        {
            'name': 'Пол',
            'value': 'поровну М и Ж'
        },
        {
            'name': 'Раса',
            'value': 'Белые'
        },
        {
            'name': 'Образование',
            'value': 'окончили колледж и получили ученую степень'
        },
        {
            'name': 'Семейный доход',
            'value': 'от 30 тыс – 45 000 $ в год'
        },
        {
            'name': 'Семейное положение',
            'value': 'холостые'
        },
        {
            'value': 'Жители крупных городов'
        }
    ],
    trump: [
        {
            'name': 'Возраст',
            'value': '50-64'
        },
        {
            'name': 'Пол',
            'value': 'М'
        },
        {
            'name': 'Раса',
            'value': 'Белые'
        },
        {
            'name': 'Образование',
            'value': 'окончили колледж'
        },
        {
            'name': 'Семейный доход',
            'value': 'от 75 тыс $ в год'
        },
        {
            'name': 'Семейное положение',
            'value': 'женатые'
        },
        {
            'value': 'Жители маленьких городов'
        }
    ]
};


$('.portrait__images').mouseover(function() {
    if($(this).hasClass('portrait__images_trump')) {
        var head = 'Потрет за Трампа';
        var items = data.trump;
    } else {
        var head = 'Портрет за Клинтон';
        var items = data.clinton;
    }
    $('.modal__name').html(head);
    var coords = $(this).offset();
    $('.modal__more-wrapper').empty();
    for(var i=0; i<items.length; i++) {
        if(items[i].hasOwnProperty('name')) {
            $('.modal__more-wrapper').append('<div class="modal__more-item">' +
                items[i].name +
                '<div class="modal__more-item-value">' +
                items[i].value +
                '</div>' +
                '</div>'
            );
        } else {
            $('.modal__more-wrapper').append('<div class="modal__more-item-value">' +
                items[i].value +
                '</div>'
            );
        }
    }
    if (coords.left + $('.modal').width() > $('body').width()) {
        $('.modal').css({
            'top': coords.top + $(this).height(),
            'left': coords.left - $(this).width() / 2
        }).show();
    } else if (coords.left - $('.modal').width() < 0) {
        $('.modal').css({
            'top': coords.top + $(this).height(),
            'left': coords.left + $(this).width() / 2
        }).show();
    } else {
        $('.modal').css({
            'top': coords.top + $(this).height(),
            'left': coords.left - $(this).width() / 2
        }).show();
    }
});

$(document).on('mouseout', '.portrait__images', function() {
    $('.modal').hide();
});