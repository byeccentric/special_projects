var translate = {
    'Хиллари Клинтон': 'clinton',
    'Дональд Трамп': 'trump'
};

var data_i = [];
var data = {};
var width = $('.fact').width();
$(document).ready(function() {
    width = $('.fact').width();
    $.ajax({
        url: 'facts.json',
        dataType: 'json',
        cache: false,
        complete: function (facts) {
            data_i = JSON.parse(facts.responseText);

            for(var i=0; i<data_i.length; i++){
                if(data.hasOwnProperty(data_i[i].group)) {
                    data[data_i[i].group][translate[data_i[i].candidate]].push(
                        {
                            date: data_i[i].date,
                            event: data_i[i].event_name,
                            text: data_i[i].tesiz
                        }
                    );
                } else {
                    data[data_i[i].group] = {
                        'clinton': [],
                        'trump': []
                    }

                    data[data_i[i].group][translate[data_i[i].candidate]].push(
                        {
                            date: data_i[i].date,
                            event: data_i[i].event_name,
                            text: data_i[i].tesiz
                        }
                    );
                }
            }

            // Сортируем по длинне текста оба массива
            for(var key in data) {
                if(data[key].trump.length > 1) {
                    data[key].trump = data[key].trump.sort(function (a, b) {
                        if(a.text.length > b.text.length)
                            return -1;
                        else
                            return 1;

                    });
                }

                if(data[key].clinton.length > 1) {
                    data[key].clinton = data[key].clinton.sort(function (a, b) {
                        if(a.text.length > b.text.length)
                            return -1;
                        else
                            return 1;
                    });
                }
            }

            var cnt = 0;
            for(var key in data) {
                //choosers build
                choosers_class = 'choosers__item';
                if(cnt == 0)
                    add_class = ' '+choosers_class+'_active';
                else
                    add_class = '';
                $('.choosers').append('<a class="'+choosers_class+add_class+'" ' +
                    'data-value="'+key+'" ' +
                    'href="#">'+key+'</a>');


                //facts build
                var count = 0;
                var html = '';
                var rounds = '';
                var main_class = 'fact__item';
                var add_class = '';
                var height = 0;

                if(cnt == 0) {
                    $('.fact__item, .fact__slider-round').remove();
                    for(var i = 0; i < data[key].trump.length; i ++) {
                        main_class = 'fact__item';
                        if(count == 0)
                            add_class = ' '+main_class+'_active" style="left:'+(count*width)+'px';
                        else
                            add_class = '" style="left:'+(count*width)+'px';
                        html += '<div class="'+main_class+add_class+'">' +
                            '<div class="fact__item-text">' +
                            ''+data[key].trump[i].text +
                            '</div>' +
                            '<div class="fact__item-source">';
                        if(data[key].trump[i].date != '')
                            html += '' + data[key].trump[i].date +
                                '. ' + data[key].trump[i].event;
                        else
                            html += data[key].trump[i].event;
                        html+=
                            '</div>' +
                            '</div>';

                        if(data[key].trump.length > 1) {

                            main_class = 'fact__slider-round';
                            add_class = '';
                            if (count == 0)
                                add_class = ' ' + main_class + '_active';
                            rounds += '<div class="' + main_class + add_class + '"></div>';

                        }

                        count++;
                    }

                    $('.fact').eq(0).prepend(html);

                    if(data[key].trump.length > 1) {
                        $('.fact:nth-child(1) .fact__slider-rounds').prepend(rounds);
                        height += 25;
                    } else {
                        $('.fact:nth-child(1) .fact__slider').hide();
                    }

                    count = 0;
                    html = '';
                    rounds = '';
                    main_class = 'fact__item';

                    for(var i = 0; i < data[key].clinton.length; i ++) {
                        main_class = 'fact__item';
                        if(count == 0)
                            add_class = ' '+main_class+'_active" style="left: 0px';
                        else
                            add_class = '" style="left:'+(count*width)+'px';
                        html += '<div class="'+main_class+add_class+'">' +
                            '<div class="fact__item-text">' +
                            ''+data[key].clinton[i].text +
                            '</div>' +
                            '<div class="fact__item-source">';
                        if(data[key].clinton[i].date != '')
                            html += '' + data[key].clinton[i].date +
                                '. ' + data[key].clinton[i].event;
                        else
                            html += data[key].clinton[i].event;
                        html+=
                            '</div>' +
                            '</div>';

                        if(data[key].clinton.length > 1) {
                            main_class = 'fact__slider-round';
                            add_class = '';
                            if (count == 0)
                                add_class = ' ' + main_class + '_active';
                            else
                                add_class = '';
                            rounds += '<div class="' + main_class + add_class + '"></div>';
                        }

                        count++;
                    }

                    $('.fact:nth-child(2)').prepend(html);
                    if(data[key].clinton.length > 1) {
                        $('.fact:nth-child(2) .fact__slider-rounds').html(rounds);
                    } else {
                        $('.fact:nth-child(2) .fact__slider').hide();
                    }

                    var max_height = 0;
                    $('.fact__item').each(function() {
                        if($(this).height() > max_height) {
                            max_height = $(this).height();
                        }
                    });
                    max_height += 25;
                    $('.fact').css('height', max_height+'px');
                }

                cnt++;
            }
        }
    })
});

$('body').on('click', '.choosers__item', function() {
    if(!$(this).hasClass('choosers__item_active')) {
        var count = 0;
        var html = '';
        var rounds = '';
        var main_class = 'fact__item';
        var add_class = '';
        $('.fact__item, .fact__slider-round').remove();
        for(var i = 0; i < data[$(this).data('value')].trump.length; i ++) {
            main_class = 'fact__item';
            if(count == 0)
                add_class = ' '+main_class+'_active" style="left: 0px';
            else
                add_class = '" style="left:'+(count*width)+'px';
            html += '<div class="'+main_class+add_class+'">' +
                '<div class="fact__item-text">';

            if($(this).data('value') == 'Контроль над оружием') {
                var last_symbol = data[$(this).data('value')].trump[i].text[data[$(this).data('value')].trump[i].text.length-1];
                data[$(this).data('value')].trump[i].text = data[$(this).data('value')].trump[i].text.split(last_symbol, 1);
                /*data[$(this).data('value')].trump[i].text[data[$(this).data('value')].trump[i].text.length-1] = '';*/
                data[$(this).data('value')].trump[i].text += '<span class="fact__item-text-star">*</span>'+last_symbol;
            }
            html += ''+ data[$(this).data('value')].trump[i].text +
                '</div>';
            html += '' +
                '<div class="fact__item-source">';
            if(data[$(this).data('value')].trump[i].date != '')
                html += '' + data[$(this).data('value')].trump[i].date +
                    '. ' + data[$(this).data('value')].trump[i].event;
            else
                html += data[$(this).data('value')].trump[i].event;
            html+=
                '</div>';
            if($(this).data('value') == 'Контроль над оружием') {
                html += '<div class="fact__item-text_help">' +
                    '*Вторая поправка к Конституции США гарантирует право граждан на хранение и ношение оружия' +
                    '</div>';
            }
            html += '' +
                '</div>';

            main_class = 'fact__slider-round';
            add_class = '';
            if(count == 0)
                add_class = ' '+main_class+'_active';
            rounds += '<div class="'+main_class+add_class+'"></div>';

            count++;
        }

        $('.fact:nth-child(1)').prepend(html);
        if(data[$(this).data('value')].trump.length > 1) {
            $('.fact:nth-child(1) .fact__slider-rounds').prepend(rounds);
            $('.fact:nth-child(1) .fact__slider').show();
        } else {
            $('.fact:nth-child(1) .fact__slider').hide();
        }

        count = 0;
        html = '';
        rounds = '';
        main_class = 'fact__item';
        for(var i = 0; i < data[$(this).data('value')].clinton.length; i ++) {
            main_class = 'fact__item';
            if(count == 0)
                add_class = ' '+main_class+'_active" style="left: 0px';
            else
                add_class = '" style="left:'+(count*width)+'px';
            html += '<div class="'+main_class+add_class+'">' +
                '<div class="fact__item-text">' +
                ''+data[$(this).data('value')].clinton[i].text +
                '</div>' +
                '<div class="fact__item-source">';
            if(data[$(this).data('value')].clinton[i].date != '')
                html += '' + data[$(this).data('value')].clinton[i].date +
                '. ' + data[$(this).data('value')].clinton[i].event;
            else
                html += data[$(this).data('value')].clinton[i].event;
            html+=
                '</div>' +
                '</div>';

            main_class = 'fact__slider-round';
            add_class = '';
            if(count == 0)
                add_class = ' '+main_class+'_active';
            else
                add_class = '';
            rounds += '<div class="'+main_class+add_class+'"></div>';

            count++;
        }

        $('.fact:nth-child(2)').prepend(html);
        if(data[$(this).data('value')].clinton.length > 1) {
            $('.fact:nth-child(2) .fact__slider-rounds').html(rounds);
            $('.fact:nth-child(2) .fact__slider').show();
        } else {
            $('.fact:nth-child(2) .fact__slider').hide();
        }

        var max_height = 0;
        $('.fact__item').each(function() {
            if($(this).height() > max_height) {
                max_height = $(this).height();
            }
        });
        max_height += 25;
        $('.fact').css('height', max_height+'px');

        $('.choosers__item').removeClass('choosers__item_active');
        $(this).addClass('choosers__item_active');
    }
    return false;
});

$('.facts').on('click', '.fact__slider-next', function () {
    if(!$(this).parent().hasClass('disabled')) {
        if ($(this).parent().parent().find('.fact__item_active').next().hasClass('fact__item')) {
            $(this).parent().parent().find('.fact__item').each(function () {
                $(this).removeClass('fact__item_active');
                $(this).css('opacity', 1);
                if (parseFloat($(this).css('left'), 10) - width == 0)
                    $(this).addClass('fact__item_active').animate({'left': parseFloat($(this).css('left'), 10) - width + 'px'}, 300);
                else
                    $(this).animate({'left': parseFloat($(this).css('left'), 10) - width + 'px', 'opacity': 0}, 300);

            });

            $(this).prev().find('.fact__slider-round_active')
                .removeClass('fact__slider-round_active')
                .next().addClass('fact__slider-round_active');

            var block = $(this).parent();
            block.addClass('disabled');
            setTimeout(function() {
                block.removeClass('disabled');
            }, 300);
        } else {
            $(this).parent().find('.fact__slider-round:first-child').trigger('click');
        }
    }
    return false;
});

$('.facts').on('click', '.fact__slider-prev', function () {
    if(!$(this).parent().hasClass('disabled')) {
        if ($(this).parent().parent().find('.fact__item_active').prev().hasClass('fact__item')) {
            $(this).parent().parent().find('.fact__item').each(function () {
                $(this).removeClass('fact__item_active');
                $(this).css('opacity', 1);
                if (parseFloat($(this).css('left'), 10) + width == 0)
                    $(this).addClass('fact__item_active').animate({'left': parseFloat($(this).css('left'), 10) + width + 'px'}, 300);
                else
                    $(this).animate({'left': parseFloat($(this).css('left'), 10) + width + 'px', 'opacity': 0}, 300);

            });

            $(this).next().find('.fact__slider-round_active')
                .removeClass('fact__slider-round_active')
                .prev().addClass('fact__slider-round_active');

            var block = $(this).parent();
            block.addClass('disabled');
            setTimeout(function() {
                block.removeClass('disabled');
            }, 300);
        } else {
            $(this).parent().find('.fact__slider-round:last-child').trigger('click');
        }
    }
    return false;
});

$('.facts').on('click', '.fact__slider-round', function() {
    if(!$(this).hasClass('fact__slider-round_active') && !$(this).parent().parent().hasClass('disabled')) {
        var new_number = $(this).parent()
            .find('.fact__slider-round').index($(this));
        var old_number = $(this).parent()
            .find('.fact__slider-round')
            .index($(this).parent().find('.fact__slider-round_active'));
        var to_left = (old_number - new_number) * width; // Количество пикселей отмотки
        $(this).parent().parent().parent().find('.fact__item').each(function() {
            $(this).removeClass('fact__item_active');
            $(this).css('opacity', 1);
            if(parseFloat($(this).css('left'), 10) + to_left == 0) {
                $(this).addClass('fact__item_active').animate({'left': parseFloat($(this).css('left'), 10) + to_left+'px'}, 300);
            } else {
                $(this).animate({'left': parseFloat($(this).css('left'), 10) + to_left + 'px', 'opacity': 0}, 300);
            }

        });

        $(this).parent().find('.fact__slider-round').removeClass('fact__slider-round_active');
        $(this).addClass('fact__slider-round_active');

        var block = $(this).parent().parent();
        block.addClass('disabled');
        setTimeout(function() {
            block.removeClass('disabled');
        }, 300);
    }
    return false;
});